<?php session_start();
/**
* Karoo App - Catalogue Handler
*
* @category   Framework
* @package    Karoo
* @author     Meetico LTD <hello@meetico.ltd>
* @author     Simone Landi <s.landi@meetico.ltd>
* @copyright  2021 Meetico LTD
* @license    http://www.php.net/license/3_01.txt  PHP License 3.01
* @link       https://meetico.ltd
*/

/* Loading required libraries */
include_once __DIR__ . '/vendor/autoload.php';


spl_autoload_register(function($className) {
    $className = str_replace("\\", DIRECTORY_SEPARATOR, $className);
    include_once __DIR__ . '/src/' . $className . '.php';
});

/* Error and Exception handling */
error_reporting(E_ALL);
set_error_handler('Karoo\Core\Error::errorHandler');
set_exception_handler('Karoo\Core\Error::exceptionHandler');


/* Enabling Enviromental Variables */
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

/* Running the App */
$app = new \Karoo\App();
$app::run();