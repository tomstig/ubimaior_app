<?php

namespace Http\Message\Authentication;

use Http\Message\Authentication;
use Psr\Http\Message\RequestInterface;

/**
 * Authenticate a PSR-7 Request with a multiple authentication methods.
 *
 * @author Márk Sági-Kazár <mark.sagikazar@gmail.com>
 */
final class Chain implements Authentication
{
    /**
     * @var Authentication[]
     */
    private $AuthenticationChain = [];

    /**
     * @param Authentication[] $AuthenticationChain
     */
    public function __construct(array $AuthenticationChain = [])
    {
        foreach ($AuthenticationChain as $Authentication) {
            if (!$Authentication instanceof Authentication) {
                throw new \InvalidArgumentException(
                    'Members of the authentication chain must be of type Http\Message\Authentication'
                );
            }
        }

        $this->AuthenticationChain = $AuthenticationChain;
    }

    /**
     * {@inheritdoc}
     */
    public function authenticate(RequestInterface $request)
    {
        foreach ($this->AuthenticationChain as $Authentication) {
            $request = $Authentication->authenticate($request);
        }

        return $request;
    }
}
