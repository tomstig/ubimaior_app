<?php

namespace Http\Message\Authentication;

use Http\Message\Authentication;
use Http\Message\RequestMatcher;
use Psr\Http\Message\RequestInterface;

/**
 * Authenticate a PSR-7 Request if the request is matching the given request matcher.
 *
 * @author Márk Sági-Kazár <mark.sagikazar@gmail.com>
 */
final class RequestConditional implements Authentication
{
    /**
     * @var RequestMatcher
     */
    private $requestMatcher;

    /**
     * @var Authentication
     */
    private $Authentication;

    /**
     * @param RequestMatcher $requestMatcher
     * @param Authentication $Authentication
     */
    public function __construct(RequestMatcher $requestMatcher, Authentication $Authentication)
    {
        $this->requestMatcher = $requestMatcher;
        $this->Authentication = $Authentication;
    }

    /**
     * {@inheritdoc}
     */
    public function authenticate(RequestInterface $request)
    {
        if ($this->requestMatcher->matches($request)) {
            return $this->Authentication->authenticate($request);
        }

        return $request;
    }
}
