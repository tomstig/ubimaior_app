<?php //declare(strict_types=1);

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


// interface Renderable
// {
//     public function render(): string;
// }

// class Form implements Renderable
// {
//     /**
//      * @var Renderable[]
//      */
//     private $elements;

//     /**
//      * runs through all elements and calls render() on them, then returns the complete representation
//      * of the form.
//      *
//      * from the outside, one will not see this and the form will act like a single object instance
//      */
//     public function render(): string
//     {
//         $formCode = '<form>';

//         foreach ($this->elements as $element) {
//             $formCode .= $element->render();
//         }

//         $formCode .= '</form>';

//         return $formCode;
//     }

//     public function addElement(Renderable $element)
//     {
//         $this->elements[] = $element;
//     }
// }

// class InputElement implements Renderable
// {
//     public function render(): string
//     {
//         return '<input type="text" />';
//     }
// }

// class TextElement implements Renderable
// {
//     private $text;

//     public function __construct(string $text)
//     {
//         $this->text = $text;
//     }

//     public function render(): string
//     {
//         return $this->text;
//     }
// }

// $form =  new Form();
// $form->addElement(new TextElement('Email:'));
// $form->addElement(new InputElement());
// $embed = new Form();
// $embed->addElement(new TextElement('Password:'));
// $embed->addElement(new InputElement());
// $form->addElement($embed);
// echo $form->render();


// interface Table{
//     public function render(): string;
// }

// class ProductsTable implements Table{
//     private $columns = [];    
//     private $tbody_empty = '';
//     private $tbody_rows = [];

//     public function render(): string{
//         $table_html = '<table class="table border-left border-right categories-table shadow-sm rounded">';

//         $table_html .= '<thead>
//             <tr class="bg-white text-muted">';
//             foreach ($this->columns as $column) {
//                 $table_html .= $column->render();
//             }
//         $table_html .= '</tr>
//         </thead>
//         <tbody>';            
//             $table_html .= $this->renderBody();
//         $table_html .= '</tbody>
//         </table>';

//         return $table_html;
//     }

//     private function renderBody(){ 
        
//         if(count($this->tbody_rows) > 0){            
//             $table_row_html = '';
//             foreach ($this->tbody_rows as $row) {
//                 $table_row_html .= $row->render();
//             }
//             return $table_row_html;
//         }else{
//             return '
//             <tr>
//                 <td colspan="'.count($this->columns).'">'.$this->tbody_empty.'</td>
//             </tr>
//             ';
//         }
//     }

//     public function addColumn(Table $column){
//         $this->columns[] = $column;
//     }

//     public function addTBodyEmpty(string $message){
//         $this->tbody_empty = $message;
//     }

//     public function addRow(Table $row){
//         $this->tbody_rows[] = $row;
//     }
// }

// class TableHeadColumn implements Table{

//     private $column_name;

//     public function __construct(string $column_name){
//         $this->column_name = $column_name;
//     }

//     public function render(): string{
//         return '<th class="border-bottom-0">'.$this->column_name.'</th>';
//     }
// }

// class TableRows implements Table{

//     private $row;

//     public function __construct(array $row){
//         $this->row = $row;
//     }

//     public function render(): string{
//         $row_html = '<tr>';
//         foreach ($this->row as $column) {
//             $row_html .= '<td>'.$column.'</td>';            
//         }
//         $row_html .= '<tr>';
//         return $row_html;
//     }
// }


// $product_table = new ProductsTable();
// $product_table->addColumn(new TableHeadColumn('ID'));
// $product_table->addColumn(new TableHeadColumn('Category')); 
// $product_table->addColumn(new TableHeadColumn('Slug'));

// $product_table->addTBodyEmpty('No categories added yet');

// $product_table->addRow(new TableRows([1,'Test','test']));
// $product_table->addRow(new TableRows([2,'Category 2','category-2']));

// echo $product_table->render();