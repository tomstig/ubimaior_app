<?php 
/**
 * Karoo App WebService Class
 *
 * @author     Meetico LTD <hello@meetico.ltd>
 * @author     Simone Landi <s.landi@meetico.ltd>
 * @copyright  2021 Meetico LTD
 */

namespace Karoo\Http;

class WebService{

    public static function run(){
        
        if(!$_REQUEST['action']) throw new \Exception('Invalid Request: missing required request params');

        $Auth = new \Karoo\Authentication\Auth();

        $Router = new \Karoo\Http\Router();        

        $Db = new \Karoo\Database\Db();        
        $Db->selectDB();

        if($_ENV['KRO_STRIPE_SECRET']){
            $gateway = \Omnipay\Omnipay::create('Stripe');
            $gateway->setApiKey($_ENV['KRO_STRIPE_SECRET']);
        }
        
        if(!include('src/services/'.$_REQUEST['controller'].'.service.php')) throw new \Exception('Could not load WebService service: '.$_REQUEST['controller']);

    }
    
}