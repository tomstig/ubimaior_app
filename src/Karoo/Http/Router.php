<?php 
/**
 * Karoo App Routing Class
 *
 * @author     Meetico LTD <hello@meetico.ltd>
 * @author     Simone Landi <s.landi@meetico.ltd>
 * @copyright  2021 Meetico LTD
 */

namespace Karoo\Http;

use \Karoo\Authentication\Auth;

class Router {    

    private $serverBasePath;
    protected $WSUrl;

    public function __construct() {
        $this->WSUrl = $_ENV['KRO_APP_BASEURL'].'/webservice.php';
    }

    public function match($routes, $uri, $prefix = ''){ 

        $page = (object) ['file_path' => 404];             

        foreach ($routes as $pattern => $route) {
            $pattern = preg_replace('/\/{(.*?)}/', '/(.*?)', $pattern);

            if (preg_match_all('#^' . $prefix.$pattern . '$#', $uri, $matches, PREG_OFFSET_CAPTURE)) {                                

                $route = (object) $route;

                $is_authenticated = \Karoo\Authentication\Auth::isAuthenticated();
                $is_granted = \Karoo\Authentication\Auth::isGranted($route->grant_types);

                if($route->auth_required && !$is_authenticated){
                    $this->redirect($_ENV['KRO_APP_BASEURL'].'/login?redirect_to='.$uri);                    
                    break;
                }elseif($route->auth_required && $is_authenticated && !$is_granted){
                    $page = (object) ['file_path' => 403];
                    break;
                }else{
                    
                    if(property_exists($route,'routes')){
                        return $this->match($route->routes,$uri,$route->url_prefix);
                    }else{
                        
                        $matches = array_slice($matches, 1);
    
                        $params = array_map(function ($match, $index) use ($matches) {
    
                            // We have a following parameter: take the substring from the current param position until the next one's position (thank you PREG_OFFSET_CAPTURE)
                            if (isset($matches[$index + 1]) && isset($matches[$index + 1][0]) && is_array($matches[$index + 1][0])) {
                                return trim(substr($match[0][0], 0, $matches[$index + 1][0][1] - $match[0][1]), '/');
                            } // We have no following parameters: return the whole lot
    
                            return isset($match[0][0]) ? trim($match[0][0], '/') : null;
                        }, $matches, array_keys($matches));
                        
                        $page = $route;
                        if(!property_exists($route,'header_path')) $page->header_path = '__defaults/header.inc.php';
                        if(!property_exists($route,'menu_path')) $page->menu_path = '__defaults/menu.inc.php';
                        if(!property_exists($route,'footer_path')) $page->footer_path = '__defaults/footer.inc.php';
                        $page->params = $params;
                        $page = (object) $page;                        

                        return $page;
                    }
                }
            }
        }        
        return $page;
    }

    public function getCurrentUri(){

        $uri = substr(rawurldecode($_SERVER['REQUEST_URI']), strlen($this->getBasePath()));

        if (strstr($uri, '?')) {
            $uri = substr($uri, 0, strpos($uri, '?'));
        }
        
        if(substr($uri, -1) && substr($uri, -1) !=='/') return '/' . trim($uri,'/') . '/';
        else if(substr($uri, -1) && substr($uri, -1) ==='/') return '/' . $uri ;
        else return '/' . trim($uri,'/'); 
    }

    /**
     * Return server base Path, and define it if isn't defined.
     *
     * @return string
     */
    public function getBasePath(){
        // Check if server base path is defined, if not define it.
        if ($this->serverBasePath === null) {
            $this->serverBasePath = implode('/', array_slice(explode('/', $_SERVER['SCRIPT_NAME']), 0, -1)) . '/';            
        }

        return $this->serverBasePath;
    }

    public function redirect($url, $tempo = FALSE ){
        if(!headers_sent() && $tempo == FALSE ){ 
            header('Location:' . $url); 
        }elseif(!headers_sent() && $tempo != FALSE ){
            header('Refresh:' . $tempo . ';' . $url);
        }else{
            if($tempo == FALSE ) { $tempo = 0; }
            echo "<meta http-equiv=\"refresh\" content=\"" . $tempo . ";" . $url . "\">";
        }
    }

    public function getWSUrl(){
        return $this->WSUrl;
    }

}