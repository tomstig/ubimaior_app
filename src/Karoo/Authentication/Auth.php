<?php
/**
 * Karoo Authentication Class
 *
 * @author     Meetico LTD <hello@meetico.ltd>
 * @author     Simone Landi <s.landi@meetico.ltd>
 * @copyright  2021 Meetico LTD
 */

namespace Karoo\Authentication;

class Auth{    

    public static function getUser(){
        return $_SESSION[$_ENV['KRO_COOKIE_PREFIX'].'user'] ? $_SESSION[$_ENV['KRO_COOKIE_PREFIX'].'user'] : null;
    }

    public static function isAuthenticated(){             
        return isset($_SESSION[$_ENV['KRO_COOKIE_PREFIX'].'access_token']) ? true : false;
    }

    public static function isGranted($grant_required){     
        return in_array($_SESSION[$_ENV['KRO_COOKIE_PREFIX'].'user']->id_user_type, $grant_required) ? true : false;
    }

    public static function isAdmin(){
        return $_SESSION[$_ENV['KRO_COOKIE_PREFIX'].'user']->role==='admin' ? true : false;
    }

}