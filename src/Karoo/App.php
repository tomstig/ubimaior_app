<?php
/**
 * Karoo App Class
 *
 * @author     Meetico LTD <hello@meetico.ltd>
 * @author     Simone Landi <s.landi@meetico.ltd>
 * @copyright  2021 Meetico LTD
 */

namespace Karoo;

use Karoo\Database;

class App {    

    public static function run(){        
        
        $Router = new \Karoo\Http\Router();

        if($_ENV['KRO_CLIENT'] && $_ENV['KRO_LICENSE']){
            $Licensor = new \Karoo\Core\Licensor();
            if(!isset($_SESSION[$_ENV['KRO_COOKIE_PREFIX'].'ver'])){
                $status = $Licensor->verify();                                
                $message = $Licensor::handleLicenseStatus($status);
                if($message!=='') die($message);            
            }
            $Tracker = new \Karoo\Core\Tracker();        
        }

        $Db = new \Karoo\Database\Db();        
        $Db->run();

        $Auth = new \Karoo\Authentication\Auth();        

        if(isset($_REQUEST[$_ENV['KRO_COOKIE_PREFIX'].'lang'])){
            $_SESSION[$_ENV['KRO_COOKIE_PREFIX'].'lang'] = $_REQUEST[$_ENV['KRO_COOKIE_PREFIX'].'lang'];
        }
        $I18n = new \Karoo\Localization\I18n(null, 'src/i18n/cache/', 'en');                
        $I18n->init();

        if(!isset($_SESSION[$_ENV['KRO_COOKIE_PREFIX'].'currency'])) $_SESSION[$_ENV['KRO_COOKIE_PREFIX'].'currency'] = 'EUR';
        if(isset($_REQUEST[$_ENV['KRO_COOKIE_PREFIX'].'currency'])) $_SESSION[$_ENV['KRO_COOKIE_PREFIX'].'currency'] = $_REQUEST[$_ENV['KRO_COOKIE_PREFIX'].'currency'];

        $Template = new \League\Plates\Engine();
        $Template->addFolder('shared','src/components/templates/shared');
        $Template->addFolder('admin','src/components/templates/admin');
        $Template->addFolder('public','src/components/templates/public');
        $Template->addFolder('form','src/components/templates/form');
        $Template->addFolder('tables','src/components/templates/tables');

        $Builder = new \Karoo\Builder\Builder();        
        $Form = new \Karoo\Builder\Form();        
        
        $Product = new \Karoo\Builder\Product();        
        $Lines = new \Karoo\Builder\Lines();


        if(!include_once('src/routes/routes.php')) throw new \Exception('Cannot load app routes');

        if(!include_once('src/app.php')) throw new \Exception('Cannot load db class');        
    }    
}