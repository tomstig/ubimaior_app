<?php 
/**
 * Karoo App Licensor Class
 *
 * @author     Meetico LTD <hello@meetico.ltd>
 * @author     Simone Landi <s.landi@meetico.ltd>
 * @copyright  2021 Meetico LTD
 */

namespace Karoo\Core;

class Licensor{    

    /**
     * Encrypt a message
     * 
     * @param string $message - message to be encrypted with app password
     * @return string
     * @throws Exception
     */
    private static function encrypt($message){
        $encrypted=openssl_encrypt($message,"AES-128-ECB",$_ENV['KRO_KEY']);
        return $encrypted;
    }

    /**
     * Decrypt a message
     * 
     * @param string $encrypted - message encrypted to be decipher
     * @return string
     * @throws Exception
     */
    private static function decrypt($encrypted){           
        $plain=openssl_decrypt($encrypted,"AES-128-ECB",$_ENV['KRO_KEY']);
        return $plain;
    }

    public function verify(){

        $configuration = include('src/env/'.$_ENV['ENV'].'.env.php');

        $cURLConnection = curl_init($configuration->licensorWS);
        curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array("X-Karoo-license: {$_ENV['KRO_LICENSE']}", "X-Karoo-app: {$_ENV['KRO_APP']}", "X-Karoo-client: {$_ENV['KRO_CLIENT']}") ); 
        curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($cURLConnection, CURLOPT_VERBOSE, true); 
        curl_setopt($cURLConnection, CURLOPT_POST, true); 
        curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, array('action' => 'verify') );
        
        $apiResponse = curl_exec($cURLConnection);
        curl_close($cURLConnection);        

        $jsonArrayResponse  = json_decode($apiResponse);        

        return intval($jsonArrayResponse->status);
        
    }

    public static function handleLicenseStatus($status){
        $message = '';
        switch ($status) {
            case 400:                    
                $message = "App license could not be verified";                
                break;
            case 401:
                $message = "App license not valid";                
                break;
            case 404:
                $message = "App license not found";                
                break;
            case 412:
                $message = "App license not recognised";                
                break;
            case 426:
                $message = "App license exprired";                
                break;
            case 200:
                $_SESSION[$_ENV['KRO_COOKIE_PREFIX'].'ver'] = 1;                
                break;
        }
        return $message;
    }
    
}