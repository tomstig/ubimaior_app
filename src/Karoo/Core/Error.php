<?php

namespace Karoo\Core;

class Error
{

    /**
     * Error handler. Convert all errors to Exceptions by throwing an ErrorException.
     *
     * @param int $level  Error level
     * @param string $message  Error message
     * @param string $file  Filename the error was raised in
     * @param int $line  Line number in the file
     *
     * @return void
     */
    public static function errorHandler($level, $message, $file, $line)
    {        
        if ($level === 1) {  // to keep the @ operator working
            $error_html = '<h3 style="font-weight:bold;color:red !important;font-family:\'Courier New\', Courier, monospace !important;">Karoo Fatal Error</h3>';
            $error_html .= "<p>Uncaught error</p>";
            $error_html .= "<p>Message: '" . $message . "'</p>";            
            $error_html .= "<p>Thrown in '" . $file . "' on line " . $line . "</p>";

            $error = '
            <div style="position:fixed;text-align:left;top:0;left:0;z-index:9999;margin:0 auto;background:rgb(222, 232, 243); width:100%; height:100vh;overflow:hidden;">
                <div style="margin:150px auto 0;width:75%;padding:100px;display:flex;justify-content:center;align-items:center;border-radius: 50px;border-radius: 50px; background: #dee8f3;box-shadow:  20px 20px 60px #bdc5cf, -20px -20px 60px #ffffff;">
                    <div style="width:100%;">                    
                        '.$error_html.'    
                    </div>
                </div>
            </div>
            <script>document.querySelector(\'body\').style.overflow = \'hidden\';</script>';
            die($error);
        }
    }

    /**
     * Exception handler.
     *
     * @param Exception $exception  The exception
     *
     * @return void
     */
    public static function exceptionHandler($exception){                

        $code = $exception->getCode();
        if ($code != 404) {
            $code = 500;
        }
        http_response_code($code);

        $exception_html = '<h3 style="font-weight:bold;color:red !important;font-family:\'Courier New\', Courier, monospace !important;">Karoo Exception Error</h3>';
        $exception_html .= "<p>Uncaught exception: '" . get_class($exception) . "'</p>";
        $exception_html .= "<p>Message: '" . $exception->getMessage() . "'</p>";
        $exception_html .= "<p>Stack trace:<pre>" . $exception->getTraceAsString() . "</pre></p>";
        $exception_html .= "<p>Thrown in '" . $exception->getFile() . "' on line " . $exception->getLine() . "</p>";

        $exception = '
        <div style="position:fixed;text-align:left;top:0;left:0;z-index:9999;margin:0 auto;background:rgb(222, 232, 243); width:100%; height:100vh;overflow:hidden;">
            <div style="margin:150px auto 0;width:75%;padding:100px;display:flex;justify-content:center;align-items:center;border-radius: 50px;border-radius: 50px; background: #dee8f3;box-shadow:  20px 20px 60px #bdc5cf, -20px -20px 60px #ffffff;">
                <div style="width:100%;">                    
                    '.$exception_html.'    
                </div>
            </div>
        </div>
        <script>document.querySelector(\'body\').style.overflow = \'hidden\';</script>';            
        
        die($exception);

    }
}