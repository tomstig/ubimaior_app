<?php

/**
 * Karoo App Tracking Class
 *
 * @author     Meetico LTD <hello@meetico.ltd>
 * @author     Simone Landi <s.landi@meetico.ltd>
 * @copyright  2021 Meetico LTD
 */

namespace Karoo\Core;

class Tracker{

    private function is_bot() {
        $botlist = $this->getBotList() ;
        foreach($botlist as $bot) {
            if(strpos($_SERVER['HTTP_USER_AGENT'] , $bot) !== false)
            return true ;
        }
        return false ;
    }
        
    private function getBotList(){
        if (($handle = fopen("robotid.txt", "r")) !== FALSE) {            
            $bots = array() ;
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                if (strchr($data[0] , "robot-id:")) {                    
                    $botId = substr("$data[0]", 9) . "" ;
                    array_push($bots, "$botId") ;                    
                }
            }
            fclose($handle);
            return $bots ;
        }
    }

    private function extractTrackingData(){
        $ip = $_SERVER['REMOTE_ADDR'] ;
        $host = $_SERVER['HTTP_HOST'];
        $session_id = $_COOKIE['PHPSESSID'];
        $date = date("Y-m-d") ;
        $time = date("H:i:s") ;        
        $query_string = $_SERVER['QUERY_STRING'] ;
        $http_referer = isset( $_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "no referer" ;
        $http_user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : "no User-agent" ;
        $web_page = str_replace($_ENV['KRO_APP_BASEURL'],'',$_SERVER['REQUEST_URI']);
        $isbot = $this->is_Bot() ? '1' : '0' ;

        return [
            'host' => $host,
            'ip' => $ip,
            'session_id' => $session_id,
            'date' => $date,
            'time' => $time,
            'query_string' => $query_string,
            'http_referer' => $http_referer,
            'http_user_agent' => $http_user_agent,
            'web_page' => $web_page,
            'isbot' => $isbot,
            'action' => 'track-view'
        ];
    }

    public function run(){

        $configuration = include('src/env/'.$_ENV['ENV'].'.env.php');

        $payload = $this->extractTrackingData();            

        $cURLConnection = curl_init($configuration->licensorWS);
        curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array("X-Karoo-license: {$_ENV['KRO_LICENSE']}", "X-Karoo-app: {$_ENV['KRO_APP']}") ); 
        curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($cURLConnection, CURLOPT_VERBOSE, true); 
        curl_setopt($cURLConnection, CURLOPT_POST, true); 
        curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, $payload );
        
        $apiResponse = curl_exec($cURLConnection);
        curl_close($cURLConnection);
        
        $jsonArrayResponse  = json_decode($apiResponse);        
        return intval($jsonArrayResponse->status);    
    }
}