<?php
/**
 * Karoo App Configuration Class
 *
 * @author     Meetico LTD <hello@meetico.ltd>
 * @author     Simone Landi <s.landi@meetico.ltd>
 * @copyright  2021 Meetico LTD
 */

namespace Karoo\Core;

class Configuration{

    public static function getBaseUrl(){
        return "";
    }

    public static function getWsUrl(){
        return 'webservice.php';
    }

    public static function getDBConfiguration(){                   
        $configuration = include('src/env/'.$_ENV['ENV'].'.env.php');                
        return $configuration->database;
    }

    public static function getFrameworkCss($framework = 'bootstrap'){
        $configuration = include('src/env/'.$_ENV['ENV'].'.env.php');        
        return $configuration->frameworks->$framework->css;
    }

    public static function getFrameworkJs($framework = 'bootstrap'){
        $configuration = include('src/env/'.$_ENV['ENV'].'.env.php');                
        return $configuration->frameworks->$framework->js;
    }

    public static function licensor(){
        $postRequest = array( 'license' => $_ENV['KRO_KEY'] );
        
        $cURLConnection = curl_init('http://localhost/licensor/webservice.php');
        curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, $postRequest);
        curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
        
        $apiResponse = curl_exec($cURLConnection);
        curl_close($cURLConnection);
        
        $jsonArrayResponse  = json_decode($apiResponse);

        print_r($jsonArrayResponse);
        
    }
    
}