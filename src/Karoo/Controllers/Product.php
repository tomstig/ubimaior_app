<?php
/**
 * Karoo Product Controller
 *
 * @author     Meetico LTD <hello@meetico.ltd>
 * @author     Simone Landi <s.landi@meetico.ltd>
 * @copyright  2021 Meetico LTD
 */

namespace Karoo\Controllers;

use \Karoo\Http\Router;
use \Karoo\Authentication\Auth;
use \Karoo\Database\Db;

class Product{

    private $db;

    public function __construct($db){
        $this->db = $db;
    }

    public function productsNumber(){
        $product_list = $this->db->select('products');
        return $product_list->num_rows;
    }

    public function create($payoad = null){        
        if($payoad){
            extract($payload);
            $columns = 'name,description,tags,image,active';
            $values = "'{$name}','{$description}','{$tags}','{$image}','{$active}'";

            $res = $this->db->create('products',$columns,$values);
            if(!$res->completed) return (object) ['status' => 500, 'errno' => $res->errno, "error" => $res->error." - <br />".$res->query];
                    
            $product_id = $res->insert_id;

            return (object) ['status' => 200, 'product_id' => $product_id];
        }

        return (object) ['status' => 400, 'error' => 'BAD REQUEST: missing payload'];
        
    }

    public function setCategories($categories = [], $product_id){

        foreach ($categories as $category_id) {
            $res = $this->db->create('products_categories','id_cateogry,id_product',"{$category_id},{$product_id}");
            if(!$res->completed) return (object) ['status' => 500, 'errno' => $res->errno, "error" => $res->error." - <br />".$res->query];            
        }

        return (object) ['status' => 200];

    }

}