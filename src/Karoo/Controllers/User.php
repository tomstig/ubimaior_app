<?php
/**
 * Karoo User Controller
 *
 * @author     Meetico LTD <hello@meetico.ltd>
 * @author     Simone Landi <s.landi@meetico.ltd>
 * @copyright  2021 Meetico LTD
 */

namespace Karoo\Controllers;

use \Karoo\Http\Router;
use \Karoo\Authentication\Auth;
use \Karoo\Database\Db;
use \Karoo\Builder\Builder;

class User{

    private $db;

    public function __construct($db){
        $this->db = $db;
    }

    public function login($email, $password){        
        $users = $this->db->select('users','*',"email='{$email}' AND password='{$password}'");
        if($users->num_rows>0){
            $user = $users->fetch_object();
            $token = base64_encode($_ENV['KRO_COOKIE_CLIENT_ID'].":".$_ENV['KRO_COOKIE_CLIENT_SECRET']);             
            $_SESSION[$_ENV['KRO_COOKIE_PREFIX'].'access_token'] = $token;
            $_SESSION[$_ENV['KRO_COOKIE_PREFIX'].'user'] = $user;
            return (object) ["status" => 200, "user" => $user, "token" => $token];            
        }
        
        return (object) ["status" => 404, "user" => "NOT_FOUND"];        
        
    }

}