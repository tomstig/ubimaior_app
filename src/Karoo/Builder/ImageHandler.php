<?php
/**
 * Karoo Image Handler Class
 *
 * @author     Meetico LTD <hello@meetico.ltd>
 * @author     Simone Landi <s.landi@meetico.ltd>
 * @copyright  2021 Meetico LTD
 */

namespace Karoo\Builder;

use \Gumlet\ImageResize;

class ImageHandler{

    protected $allowed_extensions;

    public function __construct(){        
        $this->allowed_extensions = ['png','jpg','jpeg'];
    }

    private function setImageExtensions($allowed_extensions){
        $this->allowed_extensions = $allowed_extensions;
    }

    private function getImageExtension($filename){
        $image_info = pathinfo($filename);                
        return $image_info['extension'];
    }

    private function isImageExtensionsValid($filename){          
        return in_array($this->getImageExtension($filename),$this->allowed_extensions);
    }    

    private function getFileDimensions($filepath){
        list($width, $height, $type, $attr) = getimagesize($filepath);
        return (object)['width'=>$width,'height'=>$height];
    }
    
    private function resizeByWidth($filepath){
        try{
            $image = new ImageResize($filepath);
            $image->resizeToWidth(1200);
            $image->save($filepath);
            return true;
        } catch (ImageResizeException $e) {
            echo "Something went wrong" . $e->getMessage();
            die();
        }        
    }

    private function resizeByHeight($filepath){
        try{
            $image = new ImageResize($filepath);
            $image->resizeToHeight(800);
            $image->save($filepath);
            return true;
        } catch (ImageResizeException $e) {
            echo "Something went wrong" . $e->getMessage();
            die();
        }         
    }

    private function getRGBArray($rgb){
        $rgb = str_replace('rgb(','',$rgb);
        $rgb = str_replace(')','',$rgb);
        return explode(',',$rgb);
    }

    public function createImage($width = 100, $height = 100, $rgb = 'rgb(255, 0, 0)' ,$destination = null){
        
        if(!$destination) return (object) ['processed' => false,'error'=> 'Missing destination folder'];
        
        list($r,$g,$b) = $this->getRGBArray($rgb);

        $image = imagecreate($width,$height);
        if($image===false) return (object) ['processed' => false,'error'=> 'Error while creating the image'];

        $image_bg = imagecolorallocate($image,trim($r),trim($g),trim($b));
		if($image_bg===false) return (object) ['processed' => false,'error'=> 'Error while creating the image - set color'];			

        if(imagefill($image, 0, 0, $image_bg)===false) return (object) ['processed' => false,'error'=> "Error while creating the image - fill image with color"];
        
        if(imagejpeg($image, $destination)===false) return (object) ['processed' => false,'error'=> "Error while creating the image - saving failed"];
            
        @imagedestroy($image);
        
        return (object) ['processed' => true ];

    }

    public function uploadImage($file = null, $destination = null, $filename = null, $allowed_extensions = ['png']){                        
        
        if(!$file) return (object) ['processed' => false,'error'=> 'Missing file to upload'];
        if(!$destination) return (object) ['processed' => false,'error'=> 'Missing destination folder'];

        $this->setImageExtensions($allowed_extensions);
        
        if(!is_dir($destination)) mkdir($destination);

        if($file['error']) return (object) ['processed' => false,'error'=> $file['error']];        
        
        if(!$this->isImageExtensionsValid($file['name']))  return (object) ['processed' => false,'error'=> 'Invalid File Extension: '.$this->getImageExtension($file['name'])];
        
        if(!$filename) $filename = time().'.'.$this->getImageExtension($file['name']);
        
        $destination = $destination.'/'.$filename;

        if(!move_uploaded_file($file['tmp_name'], $destination)) return (object) ['processed' => false, 'error' => 'Error while moving picture to destination folder'];
        
        $file_dimensions = $this->getFileDimensions($destination);

        if($file_dimensions->width>1200){
            $this->resizeByWidth($destination);
            $file_dimensions = $this->getFileDimensions($destination);
        }

        if($file_dimensions->height>800){
            $this->resizeByHeight($destination);            
        }

        return $filename;

    }

    public function uploadImages($files,$destination){   
        
        global $__token_generator;
        
        if(!is_dir($destination)) mkdir($destination);

        $filenames = [];

        foreach($files['name'] as $key => $value){

            if($files['error'][$key]) return (object) ['processed' => false,'error'=> $files['error'][$key]]; 
            
            if(!$this->isImageExtensionsValid($files['name'][$key]))  return (object) ['processed' => false,'error'=> 'Invalid File Extension: .'.$this->getImageExtension($files['name'][$key])];
            
            $filename = time()."_".$__token_generator->generate(6).'.'.$this->getImageExtension($files['name'][$key]);
            
            $finale_destination = $destination.'/'.$filename;            

            if(!move_uploaded_file($files['tmp_name'][$key], $finale_destination)) return (object) ['processed' => false, 'error' => 'Error while moving picture to destination folder'];
            
            $file_dimensions = $this->getFileDimensions($finale_destination);

            if($file_dimensions->width>1200){
                $this->resizeByWidth($finale_destination);
                $file_dimensions = $this->getFileDimensions($finale_destination);
            }

            if($file_dimensions->height>800){
                $this->resizeByHeight($finale_destination);            
            }

            $filenames[] = $filename;
        }

        return $filenames;

    }

    // public function resizeImages(){ 
    //     $Directory = new RecursiveDirectoryIterator('img/collections/');
    //     $Iterator = new RecursiveIteratorIterator($Directory);
    //     $Regex = new RegexIterator($Iterator, '/^.+(.jpe?g|.png)$/i', RecursiveRegexIterator::GET_MATCH);
    //     foreach($Regex as $image => $Regex){
    //         $init_file_dimensions = $this->getFileDimensions($image);
    //         if($init_file_dimensions->width>1200){
    //             $this->resizeByWidth($image);                
    //         }
    //         $file_dimensions = $this->getFileDimensions($image);

    //         if($file_dimensions->height>800){
    //             $this->resizeByHeight($image);                            
    //         }
    //         $file_dimensions = $this->getFileDimensions($image);
    //         echo "$image (Initial: {$init_file_dimensions->width}x{$init_file_dimensions->height} - Now: {$file_dimensions->width}x{$file_dimensions->height})<br />";            
    //     }        

    // }
}