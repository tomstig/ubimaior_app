<?php
/**
 * Karoo Table Builder Class
 *
 * @author     Meetico LTD <hello@meetico.ltd>
 * @author     Simone Landi <s.landi@meetico.ltd>
 * @copyright  2021 Meetico LTD
 */

namespace Karoo\Builder;

use \Karoo\Builder\Builder;

class Table{

    private $options;
    private $editable = false;
    private $sortable = false;
    private $checkable = false;
    private $table_id;
    private $hide_ids = false;
    
    private $theme = '';
    private $table_class = '';
    private $thead_class = '';
    private $tbody_class = '';
    
    private $thead;
    private $columns;
    private $rows;
    private $actions;

    private $special_cells = ['__bool','__qty','__discount','__currency','__img','__icon'];
    private $__discount_options = ['%','€'];

    public function __construct($table_id) {
        $this->table_id = $table_id;        
    }    

    public function setTableId($table_id){
        $this->table_id = $table_id;
    }
    public function hideIds($hide_ids){
        $this->hide_ids = $hide_ids;
    }

    public function setTheme($theme){        
        $this->theme = $theme;
    }
    public function setTableClass($table_class){        
        $this->table_class = $table_class;
    }
    public function setTHeadClass($thead_class){        
        $this->thead_class = $thead_class;
    }
    public function setTBodyClass($tbody_class){        
        $this->tbody_class = $tbody_class;
    }

    public function setSortable($value){
        $this->sortable = $value;
    }
    public function setCheckable($value){
        $this->checkable = $value;
    }
    public function setEditable($value){
        $this->editable = $value;
    }

    public function setColumns($columns){
        $this->columns = $columns;
    }
    public function setRows($rows){
        $this->rows = $rows;
    }
    public function setActions($actions){
        $this->actions = $actions;
    }

    /** TABLE SECTION */

    public function renderTable(){        
        ?>
        <div class="table-responsive">
            <table 
                id="<?=$this->table_id?>" 
                class="table table-borderless table-hover <?=$this->theme?> <?=$this->sortable ? "sortable" : ""?> <?=$this->table_class?>">
                <?=$this->renderThead() ?>
                <?=$this->renderTbody() ?>
            </table>
        </div>
        <?php
    }


    /** THEAD SECTION */

    private function renderThead(){
        if($this->columns){
            ?>
            <thead class="<?=$this->thead_class?>">
                <tr>                                                
                    <?=$this->renderTHeadCheckbox()?>
                    <?=$this->renderTHeadSortableCell()?>
                    <?php                 
                    foreach($this->columns as $column): ?>
                        <th scope="col"><?=$column?></th>
                    <?php endforeach ?>      
                </tr>
            </thead>
            <?php
        }
    }
    private function renderTHeadCheckbox(){
        if($this->checkable){
            $Builder = new \Karoo\Builder\Builder();
            $attributes = $Builder->generateAttributes(["id" => $this->table_id, "class" => "table-select-all"]);                    
            ?><th><?=$this->TCheckbox($attributes)?></th><?php
        }
    }
    private function renderTHeadSortableCell(){
        if($this->sortable){
            ?><th>Order <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top" title="Drag rows to set new order"></i></th><?php
        }
    }


    /** TBODY SECTION */

    /** ROW SECTION */
    private function renderTbody(){        
        ?>
        <tbody data-table="<?=$this->table_id?>">
            <?php 
            foreach($this->rows as $row): ?>
                <tr                     
                    class="<?=$this->sortable ? 'ui-state-default' : ''?>"
                    data-table=<?=$this->table_id?>
                    id="<?=$row["id"]?>"
                >
                    <?=$this->renderTBodyCheckbox($row["id"])?>
                    <?=$this->renderTBodySortableHandle()?>
                    <?php
                    foreach($row as $key => $value):
                        if(!$this->hide_ids || ($this->hide_ids && $key!=="id")):
                            ?><td class="align-middle"><?=$this->renderTCell($key, $value, $row["id"])?></td><?php
                        endif;
                    endforeach;
                    ?>
                    <?php if($this->editable || $this->actions): ?>
                        <td class="align-middle"><?=$this->generateActions($row["id"]);?></td>
                    <?php endif; ?>
                </tr>
            <?php endforeach; ?>
        </tbody>
        <?php
    }    
    private function renderTBodyCheckbox($id){
        if($this->checkable){
            $Builder = new \Karoo\Builder\Builder();
            $attributes = $Builder->generateAttributes(["data-table" => $this->table_id, "id" => $id, "class" => "row-item"]);
            ?><td class="align-middle"><?=$this->TCheckbox($attributes);?></td><?php
        }
    }
    private function renderTBodySortableHandle(){
        if($this->sortable){
            ?><td class="align-middle"><span class="handle"><i class="fas fa-grip-lines"></i></span></td><?php
        }
    }

    /** CELL SECTION */
    private function renderTCell($key, $cell, $item_id){        
        if($this->editable){
            echo $this->renderEditableCell($key,$cell,$item_id);        
        }else{
            if(\is_array($cell) && \in_array($cell["type"],$this->special_cells)){
                $this->renderSpecialCell($key,$cell,$item_id);
            }else{
                echo $cell;
            }
        }        
    }    
    private function renderEditableCell($key, $cell, $item_id){        
        if(\is_bool($cell)){
            echo $this->renderEditableCellCheckbox($key, $cell, $item_id);
        }else{
            if($key!=='cover' && $key!=='id'){
                echo $this->renderEditableCellInput($key, $cell, $item_id);
            }else{
                if($key==="cover"): 
                    ?><img src="<?=$cell['value']?>" alt="Product Cover <?=$item_id?>" width="50" /><?php 
                else: 
                    echo $cell; 
                endif;
            }
        }
    }
    private function renderEditableCellCheckbox($key, $cell, $item_id){
        $Builder = new \Karoo\Builder\Builder();
        $checkbox_config = ["class" => "row-item-checkbox","data-id" => $item_id,"data-field" => $key];
        if($cell) $checkbox_config["checked"] = "checked";
        $attributes = $Builder->generateAttributes($checkbox_config);
        echo $this->TCheckbox($attributes);
    }
    private function renderEditableCellInput($key, $cell, $item_id){
        $Builder = new \Karoo\Builder\Builder();
        $attributes = $Builder->generateAttributes(["class" => "form-control row-item-input","data-id" => $item_id,"data-field" => $key,"value" => $cell]);
        echo $this->TInput($attributes);
    }
    private function renderSpecialCell($key, $cell, $item_id){
        switch ($cell["type"]) {
            case '__bool':
                echo $this->TSwitch($item_id,$cell['value']);
                break;
            case '__img':
                ?><img src="<?=$cell['value']?>" alt="Product Cover <?=$item_id?>" width="50" /><?php 
                break;
            case '__icon':
                ?><i class="<?=$cell['value']?>"></i><?php 
                break;
            case '__currency':                
                ?>
                <span class="currency-formatter" data-name="<?=$key?>" data-id="<?=$item_id?>" data-amount="<?=$cell['value']?>" data-currency="<?=$cell['currency']?>"></span>
                <?php
                break;
            case '__qty':
                ?>
                <div class="d-flex justify-content-around align-items-center">
                    <button class="btn btn-sm btn-outline-dark qty-trigger" data-type="sub" data-id="<?=$item_id?>">-</button>
                    <span class="qty" data-id="<?=$item_id?>"><?=$cell['value']?></span>
                    <button class="btn btn-sm btn-outline-dark qty-trigger" data-type="add" data-id="<?=$item_id?>">+</button>
                </div>
                <?php
                break;
            case '__discount':                
                ?>
                <div class="d-flex justify-content-center align-items-center">
                    <input type="number" style="width:80px;margin-right: 5px;" min="0" class="form-control discount-value" data-id="<?=$item_id?>" value="<?=$cell['value']?>">
                    <select class="custom-select discount-type" data-id="<?=$item_id?>" style="width:80px">
                        <?php foreach($this->__discount_options as $option): ?>
                            <option value="<?=$option?>" <?=$option===$cell['discount_type'] ? 'selected' : ''?>>
                                <?=$option?>
                            </option>                            
                        <?php endforeach;?>
                    </select>
                </div>
                <?php
                break;
            
            default:
                # code...
                break;
        }
    }


    /** ACTIONS SECTION */
    private function generateActions($item_id){
        if(!$this->editable){
            $actions = (object) $this->actions;
            foreach ($actions as $key => $action) {
                $action = (object) $action;            
                switch ($key) {
                    case 'view':
                        ?>
                        <a href="<?=$action->url?>" class="mx-2 text-body" data-toggle="tooltip" data-placement="top" title="View">
                            <i class="fas fa-eye"></i>
                        </a>
                        <?php
                        break;
                    case 'open':
                        ?>
                        <a href="<?=$action->url?>" class="mx-2 btn btn-sm <?=$this->table_class==='table-dark' ? 'btn-light' : 'btn-dark'?>" data-toggle="tooltip" data-placement="top" title="Open">
                            Open
                        </a>
                        <?php
                        break;
                    case 'edit':
                        ?>
                        <a href="<?=$action->url?>" class="mx-2 text-body" data-toggle="tooltip" data-placement="top" title="Edit">
                            <i class="fas fa-edit"></i>
                        </a>
                        <?php
                        break;
                    case 'info':
                        ?>
                        <span class="mx-2" data-toggle="tooltip" data-placement="top" title="<?=$action->custom_text?>">
                            <i class="fas fa-info-circle"></i>
                        </span>
                        <?php
                        break;
                    case 'activity':
                        ?>
                        <a href="<?=$action->url?>" class="mx-2 text-body" data-toggle="tooltip" data-placement="top" title="View Activity">
                            <i class="fas fa-list-ul"></i>
                        </a>
                        <?php
                        break;
                    case 'users':
                        ?>
                        <a href="<?=$action->url?>" class="mx-2 text-body" data-toggle="tooltip" data-placement="top" title="View Users">
                            <i class="fas fa-user-circle"></i>
                        </a>
                        <?php
                        break;
                    case 'export':
                        ?>
                        <a 
                            href="javascript:void(0)" 
                            class="mx-2 text-body modal-trigger" 
                            data-type="confirm" 
                            data-text="<?=$action->custom_text?>"
                            data-url="<?=\Karoo\Core\Configuration::getWsUrl()?>"
                            data-action="<?=$action->action?>"
                            data-controller="<?=$action->controller?>"
                            data-id="<?=$item_id?>"
                            data-toggle="tooltip" 
                            data-placement="top" 
                            title="Export"
                        >
                            <i class="fas fa-file-alt"></i>
                        </a>
                        <?php
                        break;
                    case 'send':
                        ?>
                        <a 
                            href="javascript:void(0)"
                            class="mx-2 text-body modal-trigger"
                            data-type="confirm"
                            data-text="<?=$action->custom_text?>"
                            data-url="<?=\Karoo\Core\Configuration::getWsUrl()?>"
                            data-action="<?=$action->action?>"
                            data-controller="<?=$action->controller?>"
                            data-id="<?=$item_id?>"
                            data-toggle="tooltip" 
                            data-placement="top" 
                            title="Resend"
                        >
                            <i class="fas fa-paper-plane"></i>
                        </a>
                        <?php
                        break;
                    case 'duplicate':
                        ?>
                        <a 
                            href="javascript:void(0)" 
                            class="mx-2 text-body modal-trigger" 
                            data-type="confirm" 
                            data-text="<?=$action->custom_text?>"
                            data-url="<?=\Karoo\Core\Configuration::getWsUrl()?>"
                            data-action="<?=$action->action?>"
                            data-controller="<?=$action->controller?>"
                            data-id="<?=$item_id?>"
                            data-toggle="tooltip" 
                            data-placement="top" 
                            title="Duplicate"
                        >
                            <i class="fas fa-clone"></i>
                        </a>
                        <?php
                        break;
                    case 'delete':
                        ?>
                        <a 
                            href="javascript:void(0)" 
                            class="mx-2 text-body modal-trigger" 
                            data-type="confirm" 
                            data-text="<?=$action->custom_text?>"
                            data-url="<?=\Karoo\Core\Configuration::getWsUrl()?>"
                            data-action="<?=$action->action?>"
                            data-controller="<?=$action->controller?>"
                            data-id="<?=$item_id?>"
                            data-toggle="tooltip" 
                            data-placement="top" 
                            title="Delete"
                        >
                            <i class="fas fa-trash"></i>
                        </a>
                        <?php
                        break;
                    case 'accept':
                        ?>
                        <a 
                            href="javascript:void(0)" 
                            class="mx-2 text-success modal-trigger" 
                            data-type="confirm" 
                            data-text="<?=$action->custom_text?>"
                            data-url="<?=\Karoo\Core\Configuration::getWsUrl()?>"
                            data-action="<?=$action->action?>"
                            data-controller="<?=$action->controller?>"
                            data-id="<?=$item_id?>"
                            data-toggle="tooltip" 
                            data-placement="top" 
                            title="Accept"
                        >
                            <i class="fas fa-check-circle"></i>
                        </a>
                        <?php
                        break;
                    case 'reject':
                        ?>
                        <a 
                            href="javascript:void(0)" 
                            class="mx-2 text-body modal-trigger" 
                            data-type="confirm" 
                            data-text="<?=$action->custom_text?>"
                            data-url="<?=\Karoo\Core\Configuration::getWsUrl()?>"
                            data-action="<?=$action->action?>"
                            data-controller="<?=$action->controller?>"
                            data-id="<?=$item_id?>"
                            data-toggle="tooltip" 
                            data-placement="top" 
                            title="Reject"
                        >
                            <i class="fas fa-times-circle"></i>
                        </a>
                        <?php
                        break;
                    default:
                        break;
                }
            }
        }else{
            ?>
            <button class="btn btn-sm btn-block btn-success edit-table-item" data-id="<?=$item_id?>">Save</button>
            <button class="btn btn-sm btn-block btn-outline-dark edit-table-item" data-id="<?=$item_id?>">Details</button>            
            <?php
        }        
    }

    public function TInput($attributes){        
        return "<input type=\"text\" {$attributes} />";
    }

    public function TCheckbox($attributes){        
        return "<input type=\"checkbox\" {$attributes} />";
    }

    private function TSwitch($id, $cell){
        ?>                    
        <div class="table-switch text-left mx-auto">
            <input id="table-switch-<?=$id?>" type="checkbox" <?=$cell ? 'checked' : ''?>/>
            <label for="table-switch-<?=$id?>" data-table="<?=$this->table_id?>" class="table-switch-trigger" id="<?=$id?>"></label>
        </div>                    
        <?php
    }
    
}