<?php
/**
 * Karoo Table Builder Class
 *
 * @author     Meetico LTD <hello@meetico.ltd>
 * @author     Simone Landi <s.landi@meetico.ltd>
 * @copyright  2021 Meetico LTD
 */

namespace Karoo\Builder;

use \Karoo\Builder\Builder;
use \Karoo\Builder\Table;

class TransferList{
    private $items = [];
    private $selected = [];

    private $transfer_list_id;
    private $items_id;
    private $selected_id;

    private $items_search = false;
    private $items_grouped = false;
    private $items_title;
    private $items_subtitle;

    private $selected_search = false;
    private $selected_grouped = false;
    private $selected_title;
    private $selected_subtitle;

    private $all_selectors = true;

    public function __construct($tl_id) {
        $this->transfer_list_id = $tl_id;
        $this->items_id = uniqid('left_');
        $this->selected_id = uniqid('right_');
    }

    public function setItems($items = [], $title = '', $grouped = false, $searchable = false){
        $this->items = $items;
        if($title) $this->items_title = $title;
        $this->items_grouped = $grouped;
        $this->items_search = $searchable;
    }
    public function setItemsTitle($title){
        $this->items_title = $title;
    }
    public function setItemsSubtitle($subtitle){
        $this->items_subtitle = $subtitle;
    }
    public function isItemsGrouped($grouped){
        $this->items_grouped = $grouped;
    }
    public function isItemsSearchable($searchable){
        $this->items_search = $searchable;
    }

    public function setSelected($selected = [], $title = '', $grouped = false, $searchable = false){
        $this->selected = $selected;
        if($title) $this->selected_title = $title;
        $this->selected_grouped = $grouped;
        $this->selected_search = $searchable;
    }
    public function setSelectedTitle($title){
        $this->selected_title = $title;
    }
    public function setSelectedSubtitle($subtitle){
        $this->selected_subtitle = $subtitle;
    }
    public function isSelectedGrouped($grouped){
        $this->selected_grouped = $grouped;
    }
    public function isSelectedSearchable($searchable){
        $this->selected_search = $searchable;
    }

    public function render(){
        ?>
        <div class="d-flex justify-content-around align-items-center transfer-list" id="<?=$this->transfer_list_id?>">
            <?=$this->renderItems()?>
            <?=$this->renderButtons()?>
            <?=$this->renderSelected()?>
        </div>
        <?php
    }

    private function renderItems(){  
        $accordion_id = uniqid('tl-items-accordion-');      
        ?>
        <div class="d-flex flex-column justify-content-start align-items-start items-container" id="<?=$this->items_id?>">
            <?php if($this->items_title): ?>
                <p class="w-100 mb-0 text-center"><?=$this->items_title?></p>                
            <?php endif; ?>
            <?php if($this->items_subtitle): ?>
                <small class="w-100 text-muted text-center"><?=$this->items_subtitle?></small>
            <?php endif; ?>
            <?php if($this->items_search):?>
                <input class="form-control search-input mt-4 shadow-sm" placeholder="Search by keyword" />
            <?php endif; ?>
            <div class="accordion w-100 mt-4 pb-2" data-items-id="<?=$this->items_id?>" id="<?=$accordion_id?>">
                <?php                
                foreach ($this->items as $key => $item) {
                    echo $this->renderSection('items', $key, $item,$accordion_id);
                }                
                ?>
            </div>
        </div>
        <?php
    }

    private function renderButtons(){
        ?>
        <div class="h-100">
            <div class="d-flex flex-column">
                <button class="btn btn-sm btn-dark mb-2 add-selected"  data-tl-id="<?=$this->transfer_list_id?>">Add &raquo;</button>
                <button class="btn btn-sm btn-outline-dark remove-selected"  data-tl-id="<?=$this->transfer_list_id?>">&laquo; Remove</button>
            </div>
            <?php if($this->all_selectors): ?>
            <div class="mt-4 d-flex flex-column">
                <button class="btn btn-sm btn-outline-dark mb-2 add-all"  data-tl-id="<?=$this->transfer_list_id?>">Add All &raquo;</button>
                <button class="btn btn-sm btn-outline-dark remove-all"  data-tl-id="<?=$this->transfer_list_id?>">&laquo; Remove All</button>
            </div>
            <?php endif; ?>
        </div>
        <?php
    }


    private function renderSelected(){
        $accordion_id = uniqid('tl-selected-accordion-');
        ?>
        <div class="d-flex flex-column justify-content-start align-items-start selected-container" id="<?=$this->selected_id?>">
            <?php if($this->selected_title): ?>
                <p class="w-100 mb-0 text-center"><?=$this->selected_title?></p>
            <?php endif; ?>
            <?php if($this->selected_subtitle): ?>
                <small class="w-100 text-muted text-center"><?=$this->selected_subtitle?></small>
            <?php endif; ?>
            <?php if($this->selected_search):?>
                <input class="form-control search-input shadow-sm" placeholder="Search by keyword" />
            <?php endif; ?>
            <?php if(!$this->selected): ?>
                <p class="mt-4 w-100 text-center">No items selected yet</p>
            <?php endif; ?>
            <div class="accordion w-100 mt-4 pb-2" data-items-id="<?=$this->selected_id?>" id="<?=$accordion_id?>">
                <?php                
                foreach ($this->selected as $key => $item) {
                    echo $this->renderSection('selected', $key, $item, $accordion_id);
                }                
                ?>
            </div>
        </div>
        <?php
    }

    private function renderSection($type, $key, $item,$accordion_id){        
        ?>
        <div class="card bg-transparent border-0 p-2">
            <?=$this->renderSectionHeader($type, $item['id'], $item['code'], $item['name'], $item['products'])?>
            <?=$this->renderSectionContent($type, $key, $item['id'], $item['code'], $item['products'],$accordion_id)?>            
        </div>
        <?php
    }
    private function renderSectionHeader($type, $id, $code,$name,$products){
        ?>
        <div class="card-header bg-white border rounded" id="header-<?=$type?>-<?=$code.'-'.$id?>">
            <div class="d-flex justify-content-between align-items-center" data-toggle="collapse" data-target="#content-<?=$type?>-<?=$code.'-'.$id?>" aria-expanded="true" aria-controls="content-<?=$type?>-<?=$code.'-'.$id?>">
                <div class="d-flex align-items-center">
                    <h3 class="mb-0 mr-4"><?=$code?></h3>
                    <p class="mb-0"><?=$name?></p>
                </div>
                <p class="text-muted mb-0"><span class="text-dark"><?=count($products)?></span> items</p>                
            </div>
        </div>
        <?php
    }
    private function renderSectionContent($type, $key, $id, $code, $products,$accordion_id){
        $table = new \Karoo\Builder\Table(uniqid('tl-'.$type.'-'));
        $table->setTheme("light-theme");
        $table->setCheckable(true);
        $table->hideIds(true);
        $table->setColumns(["Img","Code","RRP","Price"]);
        $table->setTHeadClass("border");
        $table->setRows($products);        
        ?>
        <div id="content-<?=$type?>-<?=$code.'-'.$id?>" class="bg-transparent  collapse <?=$key===0 ? 'show' : ''?>" aria-labelledby="header-<?=$type?>-<?=$code.'-'.$id?>" data-parent="#<?=$accordion_id?>">
            <div class="card-body bg-transparent p-0 pt-2">
                <?=$table->renderTable();?>
            </div>
        </div>
        <?php
    }


}