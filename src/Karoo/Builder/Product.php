<?php
/**
 * Karoo Product Builder Class
 *
 * @author     Meetico LTD <hello@meetico.ltd>
 * @author     Simone Landi <s.landi@meetico.ltd>
 * @copyright  2021 Meetico LTD
 */

namespace Karoo\Builder;

use \Karoo\Http\Router;
use \Karoo\Authentication\Auth;
use \Karoo\Database\Db;
use \Karoo\Builder\Builder;

class Product{    
    

    public static function generateStatusLabel($item){        
        if($item){ 
            ?>
            <label>
                <?php if($item->active): ?>
                    <small>Actual: <i class="fas fa-circle text-success"></i> Active</small>
                <?php else: ?>
                    <small>Actual: <i class="fas fa-circle text-danger"></i> Inactive</small>
                <?php endif; ?>
            </label>
            <?php
        }else{
            ?>
            <label>Set Status</label>
            <?php
        }
    }
}