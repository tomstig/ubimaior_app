<?php
/**
 * Karoo Lines Builder Class
 *
 * @author     Meetico LTD <hello@meetico.ltd>
 * @author     Simone Landi <s.landi@meetico.ltd>
 * @copyright  2021 Meetico LTD
 */

namespace Karoo\Builder;

use \Karoo\Http\Router;
use \Karoo\Authentication\Auth;
use \Karoo\Database\Db;
use \Karoo\Builder\Builder;

class Lines{    
    

    public static function generateLineHomeCards($lines,$read_more_label,$products_label){        
        foreach($lines as $line){
            ?>
                <div class="<?=$line['container_css']?>">
                    <div class="<?=$line['outer_css']?>">
                        <div class="<?=$line['feature_css']?>"></div>
                        <img src="<?=$line['image']?>" class="cat-image-size" alt="<?=$line['image_alt']?>">
                        <h3 class="text-white"><?=$line['line_name']?></h3>
                        <p class="m-b-20">
                            <?php if($line['description']): ?>
                                <?=$line['description']?> ...
                            <?php endif; ?>
                            <?php if($line['read_more_link']): ?>
                                <a href="<?=$line['read_more_link']?>" class="<?=$line['read_more_class']?> readMore"><i><?=$read_more_label?> &raquo;</i></a>
                            <?php endif; ?>
                        </p>               
                        <?php if($line['products_link']): ?>         
                            <a href="<?=$line['products_link']?>" class="<?=$line['products_css']?>">
                                <i class=" m-r-10 fa fa-cog"></i> <?=$products_label?>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
            <?php
        }        
    }
}