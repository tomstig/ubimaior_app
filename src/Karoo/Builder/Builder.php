<?php
/**
 * Karoo Elements Builder Class
 *
 * @author     Meetico LTD <hello@meetico.ltd>
 * @author     Simone Landi <s.landi@meetico.ltd>
 * @copyright  2021 Meetico LTD
 */

namespace Karoo\Builder;

use \Karoo\Http\Router;
use \Karoo\Authentication\Auth;
use \Karoo\Database\Db;

class Builder{

    public static function loadFrameworkCss(){
        $css_cdns = \Karoo\Core\Configuration::getFrameworkCss();
        foreach ($css_cdns as $css) {
            ?><link href="<?=$css?>" rel="stylesheet"/><?php
        }
    }

    public static function loadFrameworkJs(){
        $js_cdns = \Karoo\Core\Configuration::getFrameworkJs();
        foreach ($js_cdns as $js) {
            ?><script src="<?=$js?>"></script><?php
        }
    }

    public static function loadDefaultCss($pattern = '*.default.css'){        
        if(is_dir('src/assets/css/__defaults')){
            foreach (glob("src/assets/css/__defaults/".$pattern) as $css_file) {
                ?><link href="<?=__FILE_ROOT__?>/src/assets/css/__defaults/<?=basename($css_file)?>" rel="stylesheet" /><?php
            }
        }
    }

    public static function loadDefaultJs($pattern = '*.default.js'){        
        if(is_dir('src/assets/js/__defaults')){
            foreach (glob("src/assets/js/__defaults/".$pattern) as $js_file) {
                ?><script src="<?=__FILE_ROOT__?>/src/assets/js/__defaults/<?=basename($js_file)?>"></script><?php
            }
        }
    }

    public static function loadPageCss($page_css = []){                
        foreach ($page_css as $css_file) {
            if(is_file("src/assets/css/$css_file"))
                ?><link href="<?=__FILE_ROOT__?>/src/assets/css/<?=$css_file?>" rel="stylesheet" /><?php
        }        
    }

    public static function loadPageJs($page_js = []){
        foreach ($page_js as $js_file) {
            if(is_file("src/assets/js/$js_file"))
                ?><script src="<?=__FILE_ROOT__?>/src/assets/js/<?=$js_file?>"></script><?php
        }        
    }
    
    public static function prefixer($array) {
        return array_combine(
            array_map(function($k){ return '{{'.$k.'}}'; }, array_keys($array)),
            $array
        );        
    }

    public function includeSidebar($current_page = 'dashboard',$sidebar = null){ 
        $Builder = $this;        
        if(!$sidebar) $sidebar = "__defaults/admin/sidebar.inc.php";
        if(is_file("src/components/menus/{$sidebar}"))
            include("src/components/menus/".$sidebar);             
    }

    public function includeTopbar($topbar = null){
        $Router = $this->Router;
        $Auth = $this->Auth;
        $Builder = $this;
        if(!$topbar) $topbar = "__defaults/admin/topbar.inc.php";
        if(is_file("src/components/menus/{$topbar}"))
            include("src/components/menus/".$topbar);            
    }

    public static function getAppColophon(){
        if(is_file('src/components/ui/colophon/colophon.inc.php')){
            include('src/components/ui/colophon/colophon.inc.php');
        }
    }

    public static function generateDropdown($dropdown_id,$current,$options,$container_class=null,$toggle_class=null,$dropdown_menu_class=null,$dropdown_item_class=null){        
        if(!$dropdown_id) $dropdown_id = 'dropdown-'.time();
        ?>
        <div class="dropdown mx-2 align-items-center <?=$container_class?>">
            <a class="dropdown-toggle text-secondary <?=$toggle_class?>" href="#" id="<?=$dropdown_id?>" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?=$current?></a>    
            <div class="dropdown-menu dropdown-menu-right <?=$dropdown_menu_class?>" style="max-height:300px;overflow:auto;" aria-labelledby="<?=$dropdown_id?>">
                <?php foreach ($options as $option) {                    
                    ?><a class="dropdown-item <?=$dropdown_item_class?>" href="<?=$option['link']?>"><?=$option['label']?></a><?php
                }
                ?>        
            </div>
        </div>
        <?php
    }

    public static function generateListMenu($voices = [], $menu_class = '', $direction = 'vertical'){
        $direction_class = ($direction === 'vertical') ? 'flex-column' : 'flex-row';
        ?>
        <ul class="nav <?=$menu_class?> <?=$direction_class?> m-0 p-0">
            <?php foreach ($voices as $voice) {
                $voice = (object) $voice;
                ?>
                <li class="nav-item">                    
                    <div class="d-flex justify-content-start align-items-start">                            
                        <?php if($voice->icon): ?>
                            <a class="nav-link <?=$voice->active ? 'text-danger' : ''?> text-secondary" href="<?=$voice->link?>">                                
                                <i class="fas <?=$voice->icon?> mr-3"></i>                                
                            </a>
                        <?php endif; ?>
                        <div>
                            <a class="nav-link <?=$voice->active ? 'text-danger' : ''?> text-secondary" href="<?=$voice->link?>">
                                <?=$voice->label?>
                            </a>
                            <?php
                            if($voice->active && $voice->actions){
                                ?>
                                <div class="sidemenu-sub-actions mb-2">
                                <?php
                                foreach ($voice->actions as $link => $action) {
                                    ?>                            
                                    <a class="nav-link p-0 m-0 pb-2 text-secondary" href="<?=$voice->link.$link?>">
                                        <?=$action?>
                                    </a>                            
                                    <?php
                                }
                                ?>
                                </div>
                                <?php
                            }
                            ?>
                        </div>                            
                    </div>
                </li>
                <?php
            }
            ?>
        </ul>
        <?php
    }


    public static function generateTabsNav($tabs){
        ?>
        <ul class="nav nav-tabs" id="myTab" role="tablist">
        <?php
        foreach ($tabs as $tab) {
            $tab = (object) $tab;
            $active = $tab->active ? 'active grey-t' : 'grey-t';            
            ?>
            <li class="nav-item">
                <a class="tabnav-link nav-link <?=$active?>" id="<?=$tab->id?>-tab" data-tab="<?=$tab->id?>" data-toggle="tab" href="#<?=$tab->id?>" role="tab" aria-controls="<?=$tab->id?>" aria-selected="<?=$tab->active?>">
                    <?=$tab->label?>
                    <?php if($tab->icon):?> 
                        <i class="ml-2 <?=$tab->icon?>"></i>
                    <?php endif; ?>                
                </a>
            </li>
            <?php
        }
        ?>
        </ul>
        <?php
    }

    public static function generateAutocompleteBadge($items, $id){        
        foreach ($items as $item) {
            $item = (object) $item;
            ?>
            <span class="item mr-2 text-uppercase">
                <?=$item->label?>
                <i class="fas fa-times close ml-1" id="<?=$id?>" data-label="<?=$item->label?>" data-value="<?=$item->value?>"></i>
            </span>
            <?php
        }
    }

    public static function generateChips($items){
        foreach ($items as $item) {
            $item = $item;
            ?>
            <span class="chip mr-1 text-uppercase">
                <?=$item?>                
            </span>
            <?php
        }
    }

    /**
     * generateAutocomplete()
     * Generate Autocomplete Input with custom dropdown
     * 
     * @param options
     * Made of:
     * 1. [string] label
     * 2. [object] help_tooltip [placement, text]
     * 3. [string] id
     * 4. [string] selected_container
     * 5. [string] value_container
     * 6. [string] entity
     * 7. [array] fields
     * 8. [object] input [name,placeholder,invalid_feedback_message]
     * 9. [string] default_fields_value
     * 10. [bool] image
     * 11. [bool] icon
     */
    public function generateAutocomplete($options){
        foreach ($options->fields as $name => $field) {            
            if($field->default===1) $default_fields_value = $name;
            $fields[] = $name;
        }        
        $initial_values = '';
        foreach ($options->values as $value) {
            $initial_values .= $initial_values=='' ? $value['value'] : ",{$value[value]}";
        }        
        ?>
        <div class="form-group">
            <label class="w-100">
                <?=$options->label?> 
                <?php if($options->help_tooltip){ ?>
                    <a href="#" class="text-muted float-right float-right" data-toggle="tooltip" data-placement="<?=$options->help_tooltip->placement?>" title="<?=$options->help_tooltip->text?>">
                        <i class="fas fa-question-circle text-body"></i>
                    </a>
                <?php } ?>
            </label>        
            <div style="position:relative;">
                <input 
                    id="<?=$options->id?>"
                    type="text" 
                    autocomplete="nope" 
                    class="form-control"
                    data-toggle="autocomplete"                    
                    data-entity="<?=$options->entity?>"
                    data-fields='<?=implode(',',$fields)?>'
                    data-search-by="<?=$options->search_by?>"
                    data-order-by="<?=$options->order_by?>"
                    placeholder="<?=$options->input->placeholder?>"                    
                />
                <div class="autocomplete-container d-none" id="<?=$options->id?>">
                    <div class="autocomplete-template" id="<?=$options->id?>">
                        <div class="autocomplete-item" id="<?=$options->id?>" data-value="{{id}}" data-label="{{<?=$default_fields_value?>}}">
                            <?php if($options->layout->image): ?>
                                <img class="shadow-sm mr-3" src="<?=$_ENV['KRO_APP_BASEURL']?>/{{image}}" height="35" alt="autocomplete left picture" />
                            <?php elseif($options->layout->icon): ?>
                                <i class="{{icon}} mr-3"></i>
                            <?php endif; ?>
                            {{<?=$default_fields_value?>}}
                        </div>
                    </div>
                </div>
                <div class="invalid-feedback"><?=$options->input->invalid_feedback_message?></div>
            </div>        
            <input type="hidden" name="<?=$options->input->name?>" class="autocomplete-values" id="<?=$options->id?>" value="<?=$initial_values?>" />
            <div class="spacer10"></div>
            <div class="autocomplete-selected" id="<?=$options->id?>">
                <?php $this->generateAutocompleteBadge($options->values, $options->id); ?>
            </div>
        </div>
        <?php
    }

    public function generateAttributes($attributes){
        $formAttrs = "";
        foreach ($attributes as $key => $value) {
            $formAttrs .= \is_bool($value) ? " {$key} " :" {$key}=\"{$value}\" ";
        }
        return $formAttrs;
    }

    public static function renderModalTemplate($type){
        switch ($type) {
            case 'confirm':
                ?>
                <div class="modal fade" id="confirmModal" tabindex="-1" aria-labelledby="confirmModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="confirmModalLabel">Confirm Action</h5>                                
                                <a href="javascript:void(0)" class="text-white" data-dismiss="modal">
                                    <i class="fas fa-times-circle"></i>
                                </a>
                            </div>
                            <div class="modal-body">
                                <p class="modal-body-text"></p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Cancel</button>
                                <a href="" class="btn btn-dark modal-submit">Confirm</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                break;
            
            default:
                # code...
                break;
        }
    }

}   