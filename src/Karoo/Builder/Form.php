<?php
/**
 * Karoo Form Builder Class
 *
 * @author     Meetico LTD <hello@meetico.ltd>
 * @author     Simone Landi <s.landi@meetico.ltd>
 * @copyright  2021 Meetico LTD
 */

namespace Karoo\Builder;

use \Karoo\Http\Router;
use \Karoo\Authentication\Auth;
use \Karoo\Database\Db;
use \League\Plates\Engine;


class Form{

    /**
     * generateForm()
     * Generate Form
     * 
     * @param options
     * Made of:     
     * 1. [object] config [action, method, attributes, inputs]     
     */
    public function generateForm($config){
        $config = (object) $config;
        ?>
        <form action="<?=$config->action?>" method="<?=$config->method?>" <?=$this->generateAttributes($config->attributes)?>>
            <?=$this->renderInputs($config->inputs)?>
            <button class="btn btn-primary <?=$config->submit_classes?>" type="submit"><?=$config->submit_text ? $config->submit_text : "Submit"?></button>
        </form>
        <?php
    }

    public function renderInputs($inputs){
        $Template = new \League\Plates\Engine();
        $Template->addFolder('form','src/components/templates/form');

        foreach ($inputs as $input) {
            $input = (object) $input;
            $input_config = [
                "attributes" => $input->attributes,
                "custom_classes" => $input->custom_classes,
                "helper_text" => $input->helper_text,
                "id" => $input->id,
                "initial_value" => $input->initial_value,
                "invalid_feedback" => $input->invalid_feedback,
                "label" => $input->label,            
                "options" => $input->options,            
                "select_placeholder" => $input->select_placeholder,
                "type" => $input->type,
                "valid_feedback" => $input->valid_feedback
            ];

            switch ($input->type) {                
                case 'text':
                case 'email':
                case 'password':
                    echo $Template->render('form::input',$input_config);
                    break;                
                case 'hidden':                    
                    echo $Template->render('form::hidden',$input_config);
                    break;                
                case 'select':
                    echo $Template->render('form::select',$input_config);
                    break;
                case 'switch':
                    echo $Template->render('form::switch',$input_config);
                    break;
                case 'range':
                    echo $Template->render('form::range',$input_config);
                    break;
                case 'range':
                    echo $Template->render('form::range',$input_config);
                    break;
                case 'textarea':
                    echo $Template->render('form::textarea',$input_config);
                    break;
                case 'file':
                    echo $Template->render('form::file',$input_config);
                    break;                
                case 'radio':
                    echo $Template->render('form::radio',$input_config);
                    break;
                case 'checkbox':
                    echo $Template->render('form::checkbox',$input_config);
                    break;

                default:
                    ?>
                    <p class="text-danger">Invalid form input provided</p>
                    <?php
                    break;
            }
        }
    }

    public static function generateAttributes($attributes){
        $formAttrs = "";
        foreach ($attributes as $key => $value) {
            $formAttrs .= \is_bool($value) ? " {$key} " :" {$key}=\"{$value}\" ";
        }
        return $formAttrs;
    }
}