<?php
/**
 * Karoo Db Class
 *
 * @author     Meetico LTD <hello@meetico.ltd>
 * @author     Simone Landi <s.landi@meetico.ltd>
 * @copyright  2021 Meetico LTD
 */

namespace Karoo\Database;

use \Karoo\Core\Configuration;

class Db{

    protected $orm;	
    protected $configuration;
    protected $table_prefix;

    /**
    * Db ORM.
    *
    * Public Methods for App
    * 
    */

	public function __construct() {               
        $this->configuration = \Karoo\Core\Configuration::getDBConfiguration();    
		$this->orm = new \mysqli($this->configuration->db_host, $this->configuration->db_user, $this->configuration->db_pass);
		if ($this->orm->connect_error) {
            throw new \Exception($this->orm->connect_errno.' - '.$this->orm->connect_error);		
		}
        $this->orm->set_charset($this->configuration->db_charset);
    }

    public function run($sync = false){        
        if(!$this->DbExists()){                 
            $this->setDb();
        }else{
            $this->orm->select_db($this->configuration->db_name);
            if($sync) $this->execModels();            
        }                
    }

    public function selectDB(){
        $this->orm->select_db($this->configuration->db_name);
    }

    public function escapeString($string){
        return $this->orm->real_escape_string($string);
    }

    public function cleanDataForDb($request){
        foreach ($request as $key => $value) {
            if(!is_array($value)) $request[$key] = $this->escapeString(trim($value));
            else $request[$key] = $this->cleanDataForDb($value);
        }
        return $request;
    }
    
    public function select($table, $fields = '*', $where = null, $order = null, $group = null, $join = null, $limit = null){        
                
        $query = "SELECT ".$fields." FROM ".$this->configuration->db_prefix.$table;
        if($join) $query .= " ".$join;
        if($where) $query .= " WHERE ".$where;
        if($group) $query .= ' GROUP BY '.$group;
        if($order) $query .= " ORDER BY ".$order;    
        if($limit) $query .= ' LIMIT '.$limit;            
        
        $result = $this->orm->query($query);
        if(!$result){            
            throw new \Exception($query."<br />".$this->orm->errno.' - '.$this->orm->error);
        }
        
        return $result;

    }

    public function create($table,$columns,$values){        
        
        $query = "INSERT INTO ".$this->configuration->db_prefix.$table."(".$columns.") VALUES(".$values.")";        
        if(!$this->orm->query($query)){
            return (object) [
                'completed' => false,
                'errno' => $this->orm->errno,
                'error' => $this->orm->error,
                'query' => $query
            ];
        }
        return (object) [ 'completed' => true, 'insert_id' => $this->orm->insert_id ];
    }

    public function update($table,$changes,$where){        

        $query = "UPDATE ".$this->configuration->db_prefix.$table." SET ".$changes." WHERE ".$where;    
        if(!$this->orm->query($query)){
            return (object) [
                'completed' => false,
                'errno' => $this->orm->errno,
                'error' => $this->orm->error,
                'query' => $query
            ];
        }
        return (object) [ 'completed' => true ];
                
    }

    public function delete($table,$where){        
        
        $query = "DELETE FROM ".$this->configuration->db_prefix.$table." WHERE ".$where;    
        if(!$this->orm->query($query)){
            return (object) [
                'completed' => false,
                'errno' => $this->orm->errno,
                'error' => $this->orm->error,
                'query' => $query
            ];
        }
        return (object) [ 'completed' => true ];   
    }     

    public function getTablePrefix(){
        return $this->configuration->db_prefix;
    }
    
    /**
    * Db ORM.
    *
    * Private Methods for Db Class
    * 
    */

    private function setDb(){
                   
        $database_sql = "CREATE DATABASE IF NOT EXISTS ".$this->configuration->db_name;
        if(!$this->orm->query($database_sql)){
            throw new \Exception($database_sql."<br />".$this->orm->errno.' - '.$this->orm->error);
        }
        $this->orm->select_db($this->configuration->db_name);
        $this->execModels($this->configuration->db_prefix);
        $this->execDefaultModels($this->configuration->db_prefix);
              
    }

    private function execDefaultModels(){
        
        foreach (glob("src/database/models/__defaults/*.model.php") as $model_file){
            $model = include 'src/database/models/__defaults/'.basename($model_file);                
            if($this->tableExists($this->configuration->db_name.$model->name)){
                $this->alterTableColumns($model);
            }else{
                $this->createTable($model);                
            }
            
            if($model->hooks || is_file('src/database/hooks/__defaults/'.$model->name.'.hook.php')){
                $this->execDefaultHooksQueries($model);
            }
        }        
    }

    private function execModels(){
        
        foreach (glob("src/database/models/*.model.php") as $model_file){
            $model = include 'src/database/models/'.basename($model_file);                
            if($this->tableExists($this->configuration->db_name.$model->name)){
                $this->alterTableColumns($model);
            }else{
                $this->createTable($model);                
            }
            
            if($model->hooks || is_file('src/database/hooks/'.$model->name.'.hook.php')){
                $this->execHooksQueries($model);
            }
        }        
    }

    private function execHooksQueries($model){
        
        $queries = null;
        if($model->hook) $queries = $model->queries;
        else if(is_file('src/database/hooks/'.$model->name.'.hook.php')){
            $hook = include('src/database/hooks/'.$model->name.'.hook.php');            
            $queries = $hook->queries;
        }

        foreach ($queries as $key => $attributes) {
            $columns = $values = $where = '';
            foreach ($attributes as $column => $value) {
                $columns .= $columns==='' ? $column : ','.$column;
                $values .= $values==='' ? "'".$value."'" : ",'".$value."'";
                $where .= $where==='' ? "$column='".$value."'" : " AND $column='".$value."'";                
            }
            if($this->select($model->name,'*',$where)->num_rows===0){
                if(!$this->create($model->name,$columns,$values))
                    throw new \Exception($database_sql."<br />".$this->orm->errno.' - '.$this->orm->error); 
            }            
        }
    }

    private function execDefaultHooksQueries($model){
        
        $queries = null;
        if($model->hook) $queries = $model->queries;
        else if(is_file('src/database/hooks/__defaults/'.$model->name.'.hook.php')){
            $hook = include('src/database/hooks/__defaults/'.$model->name.'.hook.php');            
            $queries = $hook->queries;
        }

        foreach ($queries as $key => $attributes) {
            $columns = $values = $where = '';
            foreach ($attributes as $column => $value) {
                $columns .= $columns==='' ? $column : ','.$column;
                $values .= $values==='' ? "'".$value."'" : ",'".$value."'";
                $where .= $where==='' ? "$column='".$value."'" : " AND $column='".$value."'";                
            }
            if($this->select($model->name,'*',$where)->num_rows===0){
                if(!$this->create($model->name,$columns,$values))
                    throw new \Exception($database_sql."<br />".$this->orm->errno.' - '.$this->orm->error); 
            }            
        }
    }

    private function createTable($model){
        
        $model_sql = "CREATE TABLE IF NOT EXISTS ".$this->configuration->db_prefix.$model->name."(";
        $columns_sql = 'ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT';
        foreach ($model->columns as $column => $attributes) {
            $columns_sql .= ", ".$column." ".$attributes['type']." ".$attributes['nullable'];            
            if(isset($attributes['default'])) $columns_sql .= " DEFAULT ".$attributes['default'];                    
            if(isset($attributes['unique'])) $columns_sql .= " UNIQUE";                                    
        }        
        $columns_sql .= ', created_at DATETIME DEFAULT CURRENT_TIMESTAMP';
        $columns_sql .= ', last_update DATETIME ON UPDATE CURRENT_TIMESTAMP';
        $model_sql .= $columns_sql.") DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;";
        $result = $this->orm->query($model_sql);
        if(!$result){
            throw new \Exception($model_sql."<br />".$this->orm->errno.' - '.$this->orm->error);
        }       
    }

    private function alterTableColumns($model){
       
        $prev_model_name = null;
        foreach ($model->columns as $column => $attributes) {
            if(!$this->tableColumnExists($this->configuration->db_prefix.$model->name,$column)){                    
                $columns_sql = "ALTER TABLE ".$this->configuration->db_prefix.$model->name." ADD ";
                
                $columns_sql .= $column." ".$attributes['type']." ".$attributes['nullable'];                                        
                if($attributes['default']!==null) $columns_sql .= " DEFAULT ".$default;
                if($attributes['unique']) $columns_sql .= " UNIQUE";

                if($prev_model_name) $columns_sql .= " AFTER $prev_model_name";                    
                $result = $this->orm->query($columns_sql);
                if(!$result){
                    throw new \Exception($columns_sql."<br />".$this->orm->errno.' - '.$this->orm->error);
                }
            }                                    
            $prev_model_name = $column;                
        }        
    }

    private function dbExists(){
        
        $sql = 'SELECT COUNT(*) AS `exists` FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMATA.SCHEMA_NAME="'.$this->configuration->db_name.'"';            
        $result = $this->orm->query($sql);
        if (!$result) {
            throw new \Exception($mysqli->error, $mysqli->errno);
        }
        $row = $result->fetch_object();
        return (bool) $row->exists;
        
    }

    private function tableExists($table) {

        $sql = "SHOW TABLES LIKE '$table'";            
        $res = $this->orm->query($sql);
        return $res->num_rows > 0;        
    }

    private function tableColumnExists($table,$column) {        
        $res = $this->orm->query("SHOW COLUMNS FROM $table LIKE '$column'");
        return $res->num_rows > 0;        
    }
}