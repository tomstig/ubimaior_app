<div class="container">
    <div class="row p-t-100 xs-p-t-0 xs-m-t-30 xs-m-b-0 xs-p-b-50 p-b-100 m-t-100 m-b-100">
        <div class="col-sm-7">
            <h1 class="text-white m-b-40 m-t-60">REGATA</h1>
            <h5 class="text-white hint-text">
                <?=L::regata_intro?>
            </h5>
            <p class="closer m-t-20"><i class="fa fa-plus-circle m-r-10"></i><?=L::shared_look_closer?> </p>
            <div class="table-container">
                <table id="table-closer">
                    <tr>
                        <td width="110" class="all-caps text-white"><?=L::shared_cheeks?></td>
                        <td><?=L::regata_cheeks?></td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white"><?=L::shared_sheave?></td>
                        <td><?=L::regata_sheave?></td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white"><?=L::shared_rollers?></td>
                        <td><?=L::regata_rollers?></td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white"><?=L::shared_balls?></td>
                        <td><?=L::regata_balls?></td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white"><?=L::shared_shackle?></td>
                        <td><?=L::regata_shackle?></td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white"><?=L::shared_other?></td>
                        <td><?=L::regata_other?></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-sm-4 col-sm-offset-1 product-page-main-image">
            <img src="<?=__FILE_ROOT__?>/src/assets/images/category-rt.png" alt="Jiber" class="image-responsive-height">
        </div>
    </div>
</div>

<div class="category-intro-expl expl-regata">
    <div class="category-intro-text hidden-xs">
        <div class="cat-open-close">
            &laquo;
        </div>
        <!--<div class="cat-open-open">
            &raquo;
        </div>-->
        <p class="m-b-50">
            <?=L::regata_img_text?>
        </p>            
        <a class="btn btn-lrg btn-bordered rt-but" href="index.php?p=catalog&idl=3">
            <i class=" m-r-10 fa fa-cog"></i> <?=L::shared_products?>
        </a>            

    </div><!-- / category-intro-text-->
</div>

<div class="category-intro-text visible-xs-inline-block"><!-- mobile category-intro-text-->
    <p class="m-b-50">
        <?=L::regata_img_text?>
    </p>            
    <a class="btn btn-lrg btn-bordered rt-but" href="index.php?p=catalog&idl=3">
        <i class=" m-r-10 fa fa-cog"></i> <?=L::shared_products?>
    </a>            
</div><!-- / mobile category-intro-text-->
<!---------------------- category options ---------------------->
<div class="container-fluid p-b-50"> 
    <div class="row">
        <div class="col-sm-4 p-l-40 m-t-40 bordered-right text-center">
            <h2 class="text-white m-t-40">RT REGATA</h2>
            <p class="fs-15 text-white m-t-20 lines-tags">
                <?=L::regata_tech1?>
            </p>
            <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-regata/rt-regata.png" class="image-responsive m-t-20" width="60%" alt="">
            <table class="table-tech m-t-40 hint-text">
                <tr>
                    <td><img src="<?=__FILE_ROOT__?>/src/assets/images/icon-length.png" /></td>
                    <td>
                        <?=L::shared_sheave?> <span class="bold">60-150 mm</span> <br>
                        <?=L::shared_line?> <span class="bold"> 12-25 mm</span> <br>
                    </td>
                </tr>
                <tr>
                    <td><img src="<?=__FILE_ROOT__?>/src/assets/images/icon-weight.png" /></td>
                    <td>
                        <?=L::shared_mwl?> <span class="bold">1400-6800 Kg</span> <br>
                        <?=L::shared_breaking_load?>  <span class="bold">2800-13600 Kg</span> <br>
                    </td>
                </tr>
            </table>
            <div style="width:100%;text-align:center;margin-top:20px;">
                <a href="index.php?p=catalog&idl=3" class="btn btn-bordered rt-but"><?=L::shared_see_products?></a>
            </div>
        </div>

        <div class="col-sm-4 p-l-40 m-t-40 bordered-right text-center">
            <h2 class="text-white m-t-40">RT ULTRA</h2>
            <p class="fs-15 text-white m-t-20 lines-tags">
                <?=L::regata_tech2?>
            </p>
            <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-regata/rt-ultra.png" class="image-responsive m-t-20" width="60%" alt="">
            <table class="table-tech m-t-40 hint-text">
                <tr>
                    <td><img src="<?=__FILE_ROOT__?>/src/assets/images/icon-length.png" /></td>
                    <td>
                        <?=L::shared_sheave?> <span class="bold">55-120 mm</span> <br>
                        <?=L::shared_line?> <span class="bold"> 14-28 mm</span> <br>
                    </td>
                </tr>
                <tr>
                    <td><img src="<?=__FILE_ROOT__?>/src/assets/images/icon-weight.png" /></td>
                    <td>
                        <?=L::shared_mwl?> <span class="bold">1800-5000 Kg</span> <br>
                        <?=L::shared_breaking_load?>  <span class="bold">3600-10000 Kg</span> <br>
                    </td>
                </tr>
            </table>
            <div style="width:100%;text-align:center;margin-top:20px;">
                <a href="index.php?p=catalog&idl=12" class="btn btn-bordered rt-but"><?=L::shared_see_products?></a>
            </div>
        </div>

        <div class="col-sm-4 p-l-40 m-t-40 text-center">
            <h2 class="text-white m-t-40">RT EVE</h2>
            <p class="fs-15 text-white m-t-20 lines-tags">
                <?=$lang[$_COOKIE['ubi_lang']]['_RT_TECH3']?>
            </p>
            <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-regata/rt-eve.png" class="image-responsive m-t-20" width="60%" alt="">
            <table class="table-tech m-t-40 hint-text">
                <tr>
                    <td><img src="<?=__FILE_ROOT__?>/src/assets/images/icon-length.png" /></td>
                    <td>
                        <?=L::shared_sheave?> <span class="bold">55-150 mm</span> <br>
                        <?=L::shared_line?> <span class="bold"> 12-25 mm</span> <br>
                    </td>
                </tr>
                <tr>
                    <td><img src="<?=__FILE_ROOT__?>/src/assets/images/icon-weight.png" /></td>
                    <td>
                        <?=L::shared_mwl?> <span class="bold">3000-13000 Kg</span> <br>
                        <?=L::shared_breaking_load?>  <span class="bold">6000-26000 Kg</span> <br>
                    </td>
                </tr>
            </table>
            <div style="width:100%;text-align:center;margin-top:20px;">
                <a href="index.php?p=catalog&idl=13" class="btn btn-bordered rt-but"><?=L::shared_see_products?></a>
            </div>
        </div>
    </div>
</div><!---------------------- / category options ---------------------->

<div class="clearfix product-drawings-box features-img-right">
    <div class="container-fluid clearfix">
        <div class="row equal"><!-- regata drawing -->
            <div class="col-12 col-sm-6 text-center cat-tech-cont cat-tech-bg">
                <a class="fancybox" rel="group" href="images/cat-regata/regata-photo-drawing.jpg">
                    <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-regata/regata-photo-drawing.jpg" alt="Regata" class="img-responsive">
                </a>
            </div>
            <div class="col-12 col-sm-6 text-center cat-tech-cont">
                <a class="fancybox" rel="group" href="images/cat-regata/regata-drawing.jpg">
                    <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-regata/regata-drawing.png" alt="Regata" class="img-responsive"> 
                </a>
            </div>
        </div>

        <div class="spacer10"></div>

        <div class="row equal"><!-- ultra drawing -->
            <div class="col-12 col-sm-6 text-center cat-tech-cont">
                <a class="fancybox" rel="group" href="images/cat-regata/ultra-drawing.jpg">
                    <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-regata/ultra-drawing.png" alt="Regata" class="img-responsive"> 
                </a>
            </div>
            <div class="col-12 col-sm-6 text-center cat-tech-cont cat-tech-bg">
                <a class="fancybox" rel="group" href="images/cat-regata/ultra-photo-drawing.jpg">
                    <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-regata/ultra-photo-drawing.jpg" alt="Regata" class="img-responsive">
                </a>
            </div>
        </div>

        <div class="spacer10"></div>

        <div class="row equal pt-5"><!-- eve drawing -->
            <div class="col-12 col-sm-6 text-center cat-tech-cont cat-tech-bg">
                <a class="fancybox" rel="group" href="images/cat-regata/eve-photo-drawing.jpg">
                    <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-regata/eve-photo-drawing.jpg" alt="Jiber-TX" class="img-responsive">
                </a>
            </div>
            <div class="col-12 col-sm-6 text-center cat-tech-cont">
                <a class="fancybox" rel="group" href="images/cat-regata/eve-drawing.jpg">
                    <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-regata/eve-drawing.png" alt="Jiber-TX" class="img-responsive"> 
                </a>
            </div>
        </div>

        <div class="spacer10"></div>

        <div class="row equal pt-5"><!-- xbearing drawing -->
            <div class="col-12 col-sm-6 text-center cat-tech-cont">
                <a class="fancybox" rel="group" href="images/cat-regata/xbearing-drawing.jpg">
                    <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-regata/xbearing-drawing.png" alt="Jiber-TX" class="img-responsive"> 
                </a>
            </div>
            <div class="col-12 col-sm-6 text-center cat-tech-cont cat-tech-bg">
                <a class="fancybox" rel="group" href="images/cat-regata/xbearing-photo-drawing.jpg">
                    <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-regata/xbearing-photo-drawing.jpg" alt="Jiber-TX" class="img-responsive">
                </a>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="rt-esploso"></div>
</div>

<div class="image-carousel">
    <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-regata/rt-slider1.jpg" alt="Regata - slideshow">
    <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-regata/rt-slider2.jpg" alt="Regata - slideshow">
    <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-regata/rt-slider3.jpg" alt="Regata - slideshow">
</div>

<div class="container pt-5 pb-5 text-center">
    <div class="row">
        <div class="col-sm-4 col-sm-push-4 col-12">
            <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-regata/regata-footer.png" alt="Ubi Maior Italia - Regata Line" class="img-responsive">
            <a href="pdf/UbiMaiorItalia_Catalog_eng.pdf#page=20" target="_blank" class="btn btn-bordered rt-but m-t-50"><i class=" m-r-10 fa fa-cog"></i> <?=L::regata_catalog?></a>
        </div>
    </div>
</div>


