<div class="container">
    <div class="row p-t-100 xs-p-t-0 xs-m-t-30 xs-m-b-0 xs-p-b-50 p-b-100 m-t-100 m-b-100">
        <div class="col-sm-7">
            <h1 class="text-white m-b-40 m-t-60"><?=L::other_brands_title?></h1>
            <h5 class="text-white hint-text">
                <?=L::other_brands_intro?>
            </h5>
            <ul class="anchor-brands">
                <li><a href="#cariboni"><img src="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/brands-cariboni.jpg" alt="UbiMaior Italia - Dealer Cariboni" class="image-responsive-height"></a></li>
                <li><a href="#carbonautica"><img src="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/brands-carbonautica.jpg" alt="UbiMaior Italia - Dealer Carbonautica" class="image-responsive-height"></a></li>
                <li><a href="#nubeway"><img src="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/brands-nubeway.jpg" alt="UbiMaior Italia - Dealer Nubeway" class="image-responsive-height"></a></li>
            </ul>
        </div>
        <div class="col-sm-4 col-sm-offset-1 product-page-main-image">
            <img src="<?=__FILE_ROOT__?>/src/assets/images/category-otherbrands.png" alt="UbiMaior Italia - Sailing Brands" class="image-responsive-height">
        </div>
    </div>
</div>

<!-- CARIBONI -->
<div class="container carbonautica-container">
    <a id="cariboni"></a>
    <div class="row p-t-70 xs-p-t-0 xs-m-t-0 xs-m-b-0 xs-p-b-50 m-t-70">
        <div class="col-sm-12">
            <h1 class="text-white m-b-30 m-t-50">CARIBONI CARITEC</h1>
            <h5 class="text-white hint-text">
                <?=L::other_brands_cariboni_intro?>
            </h5>
        </div>
    </div>
    <div class="container-fluid p-t-50 p-b-70"> 
        <div class="brands-txt"><?=L::other_brands_cariboni_text?></div>
        <div class="col-sm-4 text-center brands-imgs">
            <a class="fancybox" rel="group" href="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/cariboni1.jpg">
                <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/cariboni1.jpg" width="75%" alt="Ubi Maior Italia - Cariboni Dealer">
            </a>    
        </div>
        <div class="col-sm-4 text-center brands-imgs">
            <a class="fancybox" rel="group" href="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/cariboni2.jpg">
                <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/cariboni2.jpg" width="75%" alt="Ubi Maior Italia - Cariboni Dealer">
            </a>    
        </div>
        <div class="col-sm-4 text-center brands-imgs">
            <a class="fancybox" rel="group" href="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/cariboni3.jpg">
                <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/cariboni3.jpg" width="75%" alt="Ubi Maior Italia - Cariboni Dealer">
            </a>    
        </div>

        <div class="spacer20"></div>

        <div class="col-sm-4 text-center brands-imgs">
            <a class="fancybox" rel="group" href="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/cariboni4.jpg">
                <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/cariboni4.jpg" width="75%" alt="Ubi Maior Italia - Cariboni Dealer">
            </a>    
        </div>
        <div class="col-sm-4 text-center brands-imgs">
            <a class="fancybox" rel="group" href="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/cariboni5.jpg">
                <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/cariboni5.jpg" width="75%" alt="Ubi Maior Italia - Cariboni Dealer">
            </a>    
        </div>
        <div class="col-sm-4 text-center brands-imgs">
            <a class="fancybox" rel="group" href="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/cariboni6.jpg">
                <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/cariboni6.jpg" width="75%" alt="Ubi Maior Italia - Cariboni Dealer">
            </a>    
        </div>
        <div class="spacer20"></div>
        <div class="col-sm-12 text-center m-t-60">
            <a href="https://www.cariboni-italy.it/" target="blank" class="btn btn-bordered brands-but"><span class="fa fa-cog"></span> &nbsp; <?=L::other_brands_button?></a>
        </div>
    </div>
</div>

<!-- CARBONAUTICA -->
<div class="container carbonautica-container">
    <a id="carbonautica"></a>
    <div class="row p-t-70 xs-p-t-0 xs-m-t-0 xs-m-b-0 xs-p-b-50 m-t-70">
        <div class="col-sm-12">
            <h1 class="text-white m-b-30 m-t-50"><?=L::other_brands_carbonautica_title?></h1>
            <h5 class="text-white hint-text">
                <?=L::other_brands_carbonautica_intro?>
            </h5>
        </div>
    </div>
    <div class="container-fluid p-t-50 p-b-70"> 
        <div class="brands-txt"><?=L::other_brands_carbonautica_text1?></div>
        <div class="col-sm-4 text-center brands-imgs">
            <a class="fancybox" rel="group" href="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/carbonautica-carbon1.jpg">
                <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/carbonautica-carbon1.jpg" width="75%" alt="Ubi Maior Italia - Carbonautica Dealer">
            </a>    
        </div>
        <div class="col-sm-4 text-center brands-imgs">
            <a class="fancybox" rel="group" href="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/carbonautica-carbon2.jpg">
                <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/carbonautica-carbon2.jpg" width="75%" alt="Ubi Maior Italia - Carbonautica Dealer">
            </a>    
        </div>
        <div class="col-sm-4 text-center brands-imgs">
            <a class="fancybox" rel="group" href="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/carbonautica-carbon3.jpg">
                <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/carbonautica-carbon3.jpg" width="75%" alt="Ubi Maior Italia - Carbonautica Dealer">
            </a>    
        </div>
        <div class="spacer20"></div>
        <div class="brands-txt"><?=L::other_brands_carbonautica_text2?></div>
        <div class="col-sm-4 text-center brands-imgs">
            <a class="fancybox" rel="group" href="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/carbonautica-fiber1.jpg">
                <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/carbonautica-fiber1.jpg" width="75%" alt="Ubi Maior Italia - Carbonautica Dealer">
            </a>     
        </div>
        <div class="col-sm-4 text-center brands-imgs">
            <a class="fancybox" rel="group" href="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/carbonautica-fiber2.jpg">
                <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/carbonautica-fiber2.jpg" width="75%" alt="Ubi Maior Italia - Carbonautica Dealer">
            </a> 
        </div>
        <div class="col-sm-4 text-center brands-imgs">
            <a class="fancybox" rel="group" href="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/carbonautica-fiber3.jpg">
                <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/carbonautica-fiber3.jpg" width="75%" alt="Ubi Maior Italia - Carbonautica Dealer">
            </a> 
        </div>
        <div class="spacer20"></div>
        <div class="brands-txt"><?=L::other_brands_carbonautica_text3?></div>
        <div class="col-sm-12 text-center m-t-60">
            <a href="http://www.carbonautica.com/" target="blank" class="btn btn-bordered brands-but"><span class="fa fa-cog"></span> &nbsp; <?=L::other_brands_button?></a>
        </div>
    </div>
</div>

<div class="darker-divider"></div>


<!-- NUBEWAY -->
<div class="container carbonautica-container">
    <a id="nubeway"></a>
    <div class="row p-t-70 xs-p-t-0 xs-m-t-0 xs-m-b-0 xs-p-b-50 m-t-70">
        <div class="col-sm-12">
            <h1 class="text-white m-b-40 m-t-60"><?=L::other_brands_nubeway_title?></h1>
            <h5 class="text-white hint-text">
                <?=L::other_brands_nubeway_intro?>
            </h5>
        </div>
    </div>
    <div class="container-fluid p-t-50 p-b-70"> 
        <div class="brands-txt"><?=L::other_brands_nubeway_text1?></div>
        <div class="col-sm-4 text-center brands-imgs">
            <a class="fancybox" rel="group1" href="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/nubeway1.jpg">
                <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/nubeway1.jpg" width="75%" alt="Ubi Maior Italia - Nubeway dealer">
            </a>     
        </div>
        <div class="col-sm-4 text-center brands-imgs">
            <a class="fancybox" rel="group1" href="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/nubeway2.jpg">
                <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/nubeway2.jpg" width="75%" alt="Ubi Maior Italia - Nubeway dealer">
            </a>
        </div>
        <div class="col-sm-4 text-center brands-imgs">
            <a class="fancybox" rel="group1" href="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/nubeway3.jpg">
                <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/nubeway3.jpg" width="75%" alt="Ubi Maior Italia - Nubeway dealer">
            </a>
        </div>
        <div class="spacer20"></div>
        <div class="brands-txt"><?=L::other_brands_nubeway_text2?></div>
        <div class="col-sm-4 text-center brands-imgs">
            <a class="fancybox" rel="group1" href="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/nubeway4.jpg">
                <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/nubeway4.jpg" width="75%" alt="Ubi Maior Italia - Nubeway dealer">
            </a>
        </div>
        <div class="col-sm-4 text-center brands-imgs">
            <a class="fancybox" rel="group1" href="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/nubeway5.jpg">
                <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/nubeway5.jpg" width="75%" alt="Ubi Maior Italia - Nubeway dealer">
            </a>
        </div>
        <div class="col-sm-4 text-center brands-imgs">
            <a class="fancybox" rel="group1" href="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/nubeway6.jpg">
                <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-brands/nubeway6.jpg" width="75%" alt="Ubi Maior Italia - Nubeway dealer">
            </a>
        </div>
        <div class="spacer20"></div>
        <div class="brands-txt"><?=L::other_brands_nubeway_text3?></div>
        <div class="col-sm-12 text-center m-t-60">
            <a href="http://www.nubeway.com/it/" target="blank" class="btn btn-bordered brands-but"><span class="fa fa-cog"></span> &nbsp; <?=L::other_brands_button?></a>
        </div>
    </div>
</div>

<div class="darker-divider"></div>

<div class="image-carousel">
    <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-accessories/accessories-slider1.jpg" alt="Accessories - slideshow">
    <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-accessories/accessories-slider2.jpg" alt="Accessories - slideshow">
    <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-accessories/accessories-slider3.jpg" alt="Accessories - slideshow">
</div>

