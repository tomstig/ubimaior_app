<div class="container">
    <div class="row p-t-100 xs-p-t-0 xs-m-t-30 xs-m-b-0 xs-p-b-50 p-b-100 m-t-100 m-b-100">
        <div class="col-sm-7">
            <h1 class="text-white m-b-40 m-t-60">ALPHALOCK</h1>
            <h5 class="text-white hint-text">
                <?=L::alphalock_intro?>
            </h5>
            <p class="closer m-t-20"><i class="fa fa-plus-circle m-r-10"></i><?=L::shared_look_closer?> </p>
            <div class="table-container">
                <table id="table-closer">
                    <tr>
                        <td width="130" class="all-caps text-white"><?=L::shared_titanium?></td>
                        <td><?=L::alphalock_titanium?></td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white"><?=L::shared_service?></td>
                        <td><?=L::alphalock_service?></td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white"><?=L::shared_universal?></td>
                        <td><?=L::alphalock_universal?></td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white"><?=L::shared_stocking?></td>
                        <td><?=L::alphalock_stocking?></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-sm-4 col-sm-offset-1 product-page-main-image">
            <img src="<?=__FILE_ROOT__?>/src/assets/images/category-alphalock.png" alt="Jiber" class="image-responsive-height">
        </div>
    </div>
</div>

<div class="category-intro-expl expl-alphalock">
    <div class="category-intro-text hidden-xs">
        <div class="cat-open-close">
            &laquo;
        </div>
        <!--<div class="cat-open-open">
            &raquo;
        </div>-->
        <p class="m-b-50">
            <?=L::alphalock_image_text?><br><br>
        </p>            
        <a class="various fancybox.iframe btn btn-lg btn-bordered lck-but m-r-20" href="https://www.youtube.com/embed/e4ivhGMbOrM">
            <?=L::shared_watch_video?>
        </a> 
        <a class="btn btn-lrg btn-bordered lck-but" href="index.php?p=catalog&idl=16">
            <i class=" m-r-10 fa fa-cog"></i> <?=L::shared_products?>
        </a>           

    </div><!-- / category-intro-text-->
</div>

<div class="category-intro-text visible-xs-inline-block"><!-- mobile category-intro-text-->
    <p class="m-b-50">
        <?=L::alphalock_image_text?>
    </p>            
    <a class="btn btn-lrg btn-bordered lck-but" href="index.php?p=catalog&idl=16">
        <i class=" m-r-10 fa fa-cog"></i> <?=L::shared_products?>
    </a>            
</div><!-- / mobile category-intro-text-->

<div class="gaff-lock-txt row">
    <p class="col-12 col-md-7 col-lg-8"> 
        <?=L::alphalock_image2_text?>
    </p>
    <div class="col-12 col-md-5 col-lg-4">
        <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-alphalock/alphalock-detail.jpg" alt="Alphalock">
    </div>
    
</div>


<!---------------------- category options ---------------------->
<div class="container-fluid p-b-50"> 
    <div class="col-sm-4 p-l-40 m-t-40 bordered-right text-center">
        <h2 class="text-white m-t-40">LCK BLOCK</h2>
        <p class="fs-15 text-white m-t-20 lines-tags">
            <?=$lang[$_COOKIE['ubi_lang']]['_LCK_TECH1']?>
        </p>
        <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-alphalock/lck-block.png" class="image-responsive m-t-20" width="60%" alt="">
        <table class="table-tech m-t-40 hint-text">
            <tr>
                <td><img src="<?=__FILE_ROOT__?>/src/assets/images/icon-weight.png" /></td>
                <td>
                    <?=L::shared_mwl?>  <span class="bold">1500-12000 Kg</span>
                </td>
            </tr>
        </table>
        <div style="width:100%;text-align:center;margin-top:20px;">
            <a href="index.php?p=catalog&idl=16&f_type=LCK-BLK" class="btn btn-bordered lck-but"><?=L::shared_see_catalog?></a>
        </div>
    </div>

    <div class="col-sm-4 p-l-40 m-t-40 bordered-right text-center">
        <h2 class="text-white m-t-40">LCK IN MAST</h2>
        <p class="fs-15 text-white m-t-20 lines-tags">
            <?=L::alphalock_tech2?>
        </p>
        <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-alphalock/lck-inmast.png" class="image-responsive m-t-20" width="60%" alt="">
        <table class="table-tech m-t-40 hint-text">
            <tr>
                <td><img src="<?=__FILE_ROOT__?>/src/assets/images/icon-weight.png" /></td>
                <td>
                    <?=L::shared_mwl?>  <span class="bold">1500-12000 Kg</span> <br>
                </td>
            </tr>
        </table>
        <div style="width:100%;text-align:center;margin-top:20px;">
            <a href="index.php?p=catalog&idl=16&f_type=LCK-INT" class="btn btn-bordered lck-but"><?=L::shared_see_catalog?></a>
        </div>
    </div>

    <div class="col-sm-4 p-l-40 m-t-40 text-center">
        <h2 class="text-white m-t-40">LCK GAFF LOCK</h2>
        <p class="fs-15 text-white m-t-20 lines-tags">
            <?=L::alphalock_tech3?>
        </p>
        <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-alphalock/lck-gaff.png" class="image-responsive m-t-20" width="60%" alt="">
        <table class="table-tech m-t-40 hint-text">
            <tr>
                <td><img src="<?=__FILE_ROOT__?>/src/assets/images/icon-weight.png" /></td>
                <td class="bold">
                    <?=L::shared_ondemand?>
                </td>
            </tr>
        </table>
        <div style="width:100%;text-align:center;margin-top:20px;">
            <a href="index.php?p=catalog&idl=16&f_type=LCK-GAFF" class="btn btn-bordered lck-but"><?=L::shared_see_catalog?></a>
        </div>
    </div>

</div><!---------------------- / category options ---------------------->

<div class="clearfix product-drawings-box features-img-right">
    <div class="container-fluid clearfix">
        <div class="row equal">
            <div class="col-12 col-sm-6 text-center cat-tech-cont cat-tech-bg">
                <a class="fancybox" rel="group" href="<?=__FILE_ROOT__?>/src/assets/images/cat-alphalock/alphalock-photo1.jpg">
                    <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-alphalock/alphalock-photo1.jpg" alt="Alphalock" class="img-responsive">
                </a>
            </div>
            <div class="col-12 col-sm-6 text-center cat-tech-cont">
                <a class="fancybox" rel="group" href="<?=__FILE_ROOT__?>/src/assets/images/cat-alphalock/alphalock-photo2.jpg">
                    <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-alphalock/alphalock-photo2.png" alt="Alphalock" class="img-responsive"> 
                </a>
            </div>
        </div>

        <div class="spacer10"></div>

        <div class="row equal">
            <div class="col-12 col-sm-6 text-center cat-tech-cont">
                <a class="fancybox" rel="group" href="<?=__FILE_ROOT__?>/src/assets/images/cat-alphalock/alphalock-photo3.jpg">
                    <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-alphalock/alphalock-photo3.png" alt="Alphalock" class="img-responsive"> 
                </a>
            </div>
            <div class="col-12 col-sm-6 text-center cat-tech-cont cat-tech-bg">
                <a class="fancybox" rel="group" href="<?=__FILE_ROOT__?>/src/assets/images/cat-alphalock/alphalock-photo4.jpg">
                    <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-alphalock/alphalock-photo4.jpg" alt="Alphalock" class="img-responsive">
                </a>
            </div>
        </div>

        <div class="spacer10"></div>

        <div class="row equal">
            <div class="col-12 col-sm-6 text-center cat-tech-cont">
                <a class="fancybox" rel="group" href="<?=__FILE_ROOT__?>/src/assets/images/cat-alphalock/alphalock-photo5.jpg">
                    <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-alphalock/alphalock-photo5.jpg" alt="Alphalock" class="img-responsive"> 
                </a>
            </div>
            <div class="col-12 col-sm-6 text-center cat-tech-cont cat-tech-bg">
                <a class="fancybox" rel="group" href="<?=__FILE_ROOT__?>/src/assets/images/cat-alphalock/alphalock-photo6.jpg">
                    <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-alphalock/alphalock-photo6.png" alt="Alphalock" class="img-responsive">
                </a>
            </div>
        </div>

    </div>
</div>

<div class="container-fluid">
    <div class="alphalock-esploso"></div>
</div>

<div class="image-carousel">
    <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-alphalock/lck-slider1.jpg" alt="Alphalock - slideshow">
    <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-alphalock/lck-slider2.jpg" alt="Alphalock - slideshow">
    <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-alphalock/lck-slider3.jpg" alt="Alphalock - slideshow">
</div>
<div class="container p-t-50 p-b-50 text-center">
    <div class="row">
        <div class="col-sm-4 col-sm-push-4 col-12">
            <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-alphalock/alphalock-footer.png" alt="Ubi Maior Italia - Alphalock Line" class="img-responsive">
            <a href="https://www.alphalocksystems.com/" target="_blank" class="btn btn-bordered lck-but m-t-50"><i class=" m-r-10 fa fa-cog"></i> <?=L::alphalock_catalog?></a>
        </div>
    </div>
</div>