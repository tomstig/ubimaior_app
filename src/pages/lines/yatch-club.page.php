<div class="container">
    <div class="row">
        <div class="col-sm-7">
            <h1 class="text-white mb-5 mt-5">YACHT CLUB</h1>
            <h5 class="text-white hint-text">
                <?=L::yc_intro?>
            </h5>
            <p class="closer mt-2"><i class="fa fa-plus-circle mr-2"></i><?=L::shared_look_closer?></p>
            <div class="table-container">
                <table id="table-closer">
                    <tr>
                        <td width="110" class="all-caps text-white"><?=L::shared_cheeks?></td>
                        <td><?=L::yc_cheeks?></td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white"><?=L::shared_sheave?></td>
                        <td><?=L::yc_sheave?></td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white"><?=L::shared_balls?></td>
                        <td><?=L::yc_balls?></td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white"><?=L::shared_other?></td>
                        <td><?=L::yc_other?></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-sm-4 offset-sm-1 product-page-main-image">
            <img src="<?=__FILE_ROOT__?>/src/assets/images/category-yc.png" alt="Jiber" class="image-responsive-height">
        </div>
    </div>
</div>

<div class="category-intro-expl expl-yc">
    <div class="category-intro-text hidden-xs">
        <div class="cat-open-close">
            &laquo;
        </div>
        <!--<div class="cat-open-open">
            &raquo;
        </div>-->
        <p class="m-b-50">
            <?=L::yc_descr1?>
        </p>            
        <a class="btn btn-lrg btn-bordered yc-but" href="index.php?p=catalog&idl=4">
            <i class="m-r-10 fa fa-cog"></i> <?=L::shared_products?>
        </a>            

    </div><!-- / category-intro-text-->
</div>
<div class="category-intro-text visible-xs-inline-block">
    <p class="m-b-50">
        <?=L::yc_descr1?>
    </p>            
    <a class="btn btn-lrg btn-bordered yc-but" href="index.php?p=catalog&idl=4">
        <i class="m-r-10 fa fa-cog"></i> <?=L::shared_see_products?>
    </a>            
</div><!-- / category-intro-text-->
<!---------------------- category options ---------------------->
<div class="container-fluid p-b-50"> 
    <div class="col-sm-4 col-sm-offset-1 text-center">
        <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-yc/yc60s.png" class="image-responsive m-t-50" width="100%" alt="">
    </div>
    <div class="col-sm-5 col-md-offset-1 col-lg-offset-1 p-l-40 m-t-40 text-center">
        <h2 class="text-white m-t-40">YC YACHT CLUB</h2>
        <p class="fs-15 text-white m-t-20 lines-tags">
            <?=L::yc_tech?>
        </p>
        <table class="table-tech m-t-40 hint-text">
            <tr>
                <td><img src="<?=__FILE_ROOT__?>/src/assets/images/icon-length.png" /></td>
                <td>
                    <?=L::shared_sheave?><span class="bold"> 40-80 mm</span> <br>
                    <?=L::shared_line?><span class="bold"> 8-16 mm</span> <br>
                </td>
            </tr>
            <tr>
                <td><img src="<?=__FILE_ROOT__?>/src/assets/images/icon-weight.png" /></td>
                <td>
                    <?=L::shared_mwl?> <span class="bold"> 500-1800 Kg</span> <br>
                    <?=L::shared_breaking_load?><span class="bold"> 1000-3600 Kg</span> <br>
                </td>
            </tr>
        </table>
        <div class="cat-option-button m-t-20">
            <a href="index.php?p=catalog&idl=4" class="btn btn-bordered yc-but"><?=L::shared_see_products?></a>
        </div>
    </div>

</div><!---------------------- / category options ---------------------->

<div class="clearfix product-drawings-box features-img-right">
    <div class="container-fluid clearfix">
        <div class="row equal">
            <div class="col-12 col-sm-6 text-center cat-tech-cont">
                <a class="fancybox" rel="group" href="<?=__FILE_ROOT__?>/src/assets/images/cat-yc/yc-photo-drawing.jpg">
                    <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-yc/yc-photo-drawing.jpg" alt="Ubimaior Italia - Yacht Club" class="img-responsive">
                </a>
            </div>
            <div class="col-12 col-sm-6 text-center cat-tech-cont cat-tech-bg">
                <a class="fancybox" rel="group" href="<?=__FILE_ROOT__?>/src/assets/images/cat-yc/yc-drawing.jpg">
                    <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-yc/yc-drawing.jpg" alt="Ubimaior Italia - Yacht Club" class="img-responsive"> 
                </a>
            </div>
        </div>
    </div>
    <div class="container-fluid clearfix">
        <div class="row equal">
            <div class="col-12 col-sm-6 text-center cat-tech-cont cat-tech-bg">
                <a class="fancybox" rel="group" href="<?=__FILE_ROOT__?>/src/assets/images/cat-yc/yc-drawing2.jpg">
                    <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-yc/yc-drawing2.jpg" alt="Ubimaior Italia - Yacht Club" class="img-responsive">
                </a>
            </div>
            <div class="col-12 col-sm-6 text-center cat-tech-cont">
                <a class="fancybox" rel="group" href="<?=__FILE_ROOT__?>/src/assets/images/cat-yc/yc-photo-drawing2.jpg">
                    <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-yc/yc-photo-drawing2.jpg" alt="Ubimaior Italia - Yacht Club" class="img-responsive"> 
                </a>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="yc-esploso"></div>
</div>

<div class="image-carousel">
    <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-yc/yc-slider1.jpg" alt="Yacht Club - slideshow">
    <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-yc/yc-slider2.jpg" alt="Yacht Club - slideshow">
    <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-yc/yc-slider3.jpg" alt="Yacht Club - slideshow">
</div>

<div class="container p-t-50 p-b-50 text-center">
    <div class="row">
        <div class="col-sm-4 col-sm-push-4 col-12">
            <img src="<?=__FILE_ROOT__?>/src/assets/images/cat-yc/yc-footer.png" alt="Ubi Maior Italia - Regata Line" class="img-responsive">
            <a href="pdf/UbiMaiorItalia_Catalog_eng.pdf#page=16" target="_blank" class="btn btn-bordered yc-but m-t-50"><i class=" m-r-10 fa fa-cog"></i> <?=L::yc_catalog?></a>
        </div>
    </div>
</div>


