<?php
$categories = $Db->select('categories','*',null,'category ASC');
?>
<div class="admin-page-container p-4">    
    <div class="d-flex justify-content-between align-items-center">
        <div class="d-flex w-25 justify-content-start align-items-center">
            <h5 class="p-0 m-0 mr-4">Manage Categories</h5>
            <small class="p-0 m-0 text-muted"><?=$categories->num_rows?> <?=$categories->num_rows===1 ? 'Category' : 'Categories'?></small>
        </div>        
        <div class="w-50 d-flex justify-content-between align-items-center">
            <a href="<?="{$Router->getBasePath()}admin/categories/add"?>" class="btn add-new-button text-decoration-none mr-3">
                <i class="fas fa-plus-circle"></i> Add New Category
            </a>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="search-category-addon">
                        <i class="fas fa-search"></i>
                    </span>                
                </div>
                <input type="text" class="form-control" placeholder="search category by keyword" aria-label="search category by keyword" aria-describedby="search-category-addon">            
            </div>
        </div>        
    </div>    
    <div class="spacer30"></div>
    <?php include("src/components/ui/tables/categories.table.inc.php"); ?>
</div>