<?php 
if($page->params) $product = $Db->select('products','*',"id={$page->params[0]}")->fetch_object();
?>
<div class="admin-page-container p-4">     
    <h6 class="text-muted">
        <a href="<?="{$Router->getBasePath()}admin/products"?>" class="text-muted text-decoration-none">
            <i class="fas fa-long-arrow-alt-left mr-1"></i> Products
        </a>        
    </h6>
    <div class="d-flex justify-content-start align-items-center">    
        <h4 class="p-0 m-0"><?=isset($product) ? 'Edit' : 'Add New'?> Product</h4>                              
    </div>    
    <div class="spacer30"></div>
    <div class="bg-white p-3 shadow-sm">    
        <?php
        $tabs = [
            ['id' => 'info', 'label' => 'General Info', 'active' => (!isset($_REQUEST['tab']) || $_REQUEST['tab']==='info') ? true : false, 'icon' => null],                     
        ];
        if(isset($product)):

            $product_id = $page->params[0];

            // if(!$Db->select('products_properties','id','id_product='.$product_id)->num_rows>0) $properties_icon = 'fas fa-exclamation-triangle text-warning';
            // if(!$Db->select('products_sizes','id','id_product='.$product_id)->num_rows>0) $sizes_icon = 'fas fa-exclamation-triangle text-warning';
            // if(!$Db->select('products_colors','id','id_product='.$product_id)->num_rows>0) $colors_icon = 'fas fa-exclamation-triangle text-warning';            
            // if(!$Db->select('products_prices','id','id_product='.$product_id)->num_rows>0) $prices_icon = 'fas fa-exclamation-triangle text-warning';            
            // if(!$Db->select('products_images','id','id_product='.$product_id)->num_rows>0) $images_icon = 'fas fa-exclamation-triangle text-warning';            
        
            $addon_tabs = [
                ['id' => 'properties', 'label' => 'Properties', 'active' => (isset($_REQUEST['tab']) && $_REQUEST['tab']==='properties') ? true : false, 'icon' => $properties_icon],           
                ['id' => 'sizes', 'label' => 'Sizes', 'active' => (isset($_REQUEST['tab']) && $_REQUEST['tab']==='sizes') ? true : false, 'icon' => $sizes_icon],
                ['id' => 'colors', 'label' => 'Colors', 'active' => (isset($_REQUEST['tab']) && $_REQUEST['tab']==='colors') ? true : false, 'icon' => $colors_icon],                                    
            ];
            if($Db->select('products_sizes','id','id_product='.$product_id)->num_rows>0 || $Db->select('products_colors','id','id_product='.$product_id)->num_rows>0){
                $addon_tabs[] = ['id' => 'prices', 'label' => 'Prices', 'active' => (isset($_REQUEST['tab']) && $_REQUEST['tab']==='prices') ? true : false, 'icon' => $prices_icon];
            }
            $addon_tabs[] = ['id' => 'images', 'label' => 'Images', 'active' => (isset($_REQUEST['tab']) && $_REQUEST['tab']==='images') ? true : false, 'icon' => $images_icon];

            $tabs = array_merge($tabs,$addon_tabs);
        endif;

        $Builder::generateTabsNav($tabs); 
        ?>            
        <div class="tab-content" id="myTabContent">
            <?php 
            foreach ($tabs as $tab) {
                $tab = (object) $tab;
                ?>
                <div class="tab-pane fade <?=$tab->active ? 'show active' : ''?>" id="<?=$tab->id?>" role="tabpanel" aria-labelledby="<?=$tab->id?>-tab">                    
                    <?php include('src/components/ui/tabs/product/'.$tab->id.'.tab.php'); ?> 
                </div>
                <?php
            } 
            ?>            
        </div>
    </div>    

</div>