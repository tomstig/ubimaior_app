<div class="spacer20"></div>
<div class="p-4 admin-page-container text-center">
    <h4>Welcome to Ubimaior Italia Admin Dashboard</h4>
    <div class="row">
        <div class="col-sm-12 mt-4">
            <?php 
            $items = [
                [
                    "id" => 1, 
                    "name" => "Yatch Club", 
                    "code" => "YC",
                    "products" => [
                        ["id" => 1, "img" => ["type" => "__img", "value" => __FILE_ROOT__."/src/assets/demo/images/product.jpg"], "code" => "JB1234", "rrp" => ["type" => "__currency", "value" => 1200, "currency" => "EUR"], "price" => ["type" => "__currency", "value" => 1000, "currency" => "EUR"]],
                        ["id" => 2, "img" => ["type" => "__img", "value" => __FILE_ROOT__."/src/assets/demo/images/product.jpg"], "code" => "JB5678", "rrp" => ["type" => "__currency", "value" => 1200, "currency" => "EUR"], "price" => ["type" => "__currency", "value" => 1000, "currency" => "EUR"]],
                        ["id" => 3, "img" => ["type" => "__img", "value" => __FILE_ROOT__."/src/assets/demo/images/product.jpg"], "code" => "JB9876", "rrp" => ["type" => "__currency", "value" => 1200, "currency" => "EUR"], "price" => ["type" => "__currency", "value" => 1000, "currency" => "EUR"]],
                        ["id" => 4, "img" => ["type" => "__img", "value" => __FILE_ROOT__."/src/assets/demo/images/product.jpg"], "code" => "JB5432", "rrp" => ["type" => "__currency", "value" => 1200, "currency" => "EUR"], "price" => ["type" => "__currency", "value" => 1000, "currency" => "EUR"]]
                    ]
                ],
                [
                    "id" => 2, 
                    "name" => "Regata", 
                    "code" => "RT",
                    "products" => [
                        ["id" => 5, "img" => ["type" => "__img", "value" => __FILE_ROOT__."/src/assets/demo/images/product.jpg"], "code" => "RT1234", "rrp" => ["type" => "__currency", "value" => 1200, "currency" => "EUR"], "price" => ["type" => "__currency", "value" => 1000, "currency" => "EUR"]],
                        ["id" => 6, "img" => ["type" => "__img", "value" => __FILE_ROOT__."/src/assets/demo/images/product.jpg"], "code" => "RT5678", "rrp" => ["type" => "__currency", "value" => 1200, "currency" => "EUR"], "price" => ["type" => "__currency", "value" => 1000, "currency" => "EUR"]],
                        ["id" => 7, "img" => ["type" => "__img", "value" => __FILE_ROOT__."/src/assets/demo/images/product.jpg"], "code" => "RT9876", "rrp" => ["type" => "__currency", "value" => 1200, "currency" => "EUR"], "price" => ["type" => "__currency", "value" => 1000, "currency" => "EUR"]],
                        ["id" => 8, "img" => ["type" => "__img", "value" => __FILE_ROOT__."/src/assets/demo/images/product.jpg"], "code" => "RT5432", "rrp" => ["type" => "__currency", "value" => 1200, "currency" => "EUR"], "price" => ["type" => "__currency", "value" => 1000, "currency" => "EUR"]]
                    ]
                ],
            ];

            $selected = [
                [
                    "id" => 1, 
                    "name" => "Yatch Club", 
                    "code" => "YC",
                    "products" => [
                        ["id" => 1, "img" => ["type" => "__img", "value" => __FILE_ROOT__."/src/assets/demo/images/product.jpg"], "code" => "JB1234", "rrp" => ["type" => "__currency", "value" => 1200, "currency" => "EUR"], "price" => ["type" => "__currency", "value" => 1000, "currency" => "EUR"]],
                        ["id" => 2, "img" => ["type" => "__img", "value" => __FILE_ROOT__."/src/assets/demo/images/product.jpg"], "code" => "JB5678", "rrp" => ["type" => "__currency", "value" => 1200, "currency" => "EUR"], "price" => ["type" => "__currency", "value" => 1000, "currency" => "EUR"]],                        
                    ]
                ],
                [
                    "id" => 2, 
                    "name" => "Regata", 
                    "code" => "RT",
                    "products" => [
                        ["id" => 5, "img" => ["type" => "__img", "value" => __FILE_ROOT__."/src/assets/demo/images/product.jpg"], "code" => "RT1234", "rrp" => ["type" => "__currency", "value" => 1200, "currency" => "EUR"], "price" => ["type" => "__currency", "value" => 1000, "currency" => "EUR"]],                        
                    ]
                ],
            ];

            if($_GET){
                echo "<pre>";
                print_r($_GET);
                echo "</pre>";
            }

            $product = new \Karoo\Controllers\Product($Db);
            echo $product->productsNumber();

            $transfer_list = new \Karoo\Builder\TransferList('tl-1');
            $transfer_list->setItems($items, 'Products List', false, true);            
            $transfer_list->setItemsSubtitle('Select the items you want to add to your catalogue, the press Add');
            $transfer_list->setSelectedTitle('Selected Items');            
            $transfer_list->setSelectedSubtitle('('.count($selected).' Items Selected)');
            $transfer_list->setSelected($selected);
            echo $transfer_list->render();
            ?>
        </div>
    </div>
</div>
