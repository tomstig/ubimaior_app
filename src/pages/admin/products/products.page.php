<?php
$products = $Db->select(
    'products p',
    'p.id, p.name, p.tags, p.description, p.image, p.active, p.created_at, pp.price, pp.vat, c.symbol',    
    null,
    'p.created_at DESC',
    null,
    'LEFT JOIN '.$Db->getTablePrefix().'products_prices pp ON pp.id_product = p.id LEFT JOIN '.$Db->getTablePrefix().'currencies c ON c.id = pp.id_currency'
);
?>
<div class="admin-page-container p-4">    
    <div class="d-flex justify-content-between align-items-center">
        <div class="d-flex w-25 justify-content-start align-items-center">
            <h5 class="p-0 m-0 mr-4">Manage Products</h5>
            <small class="p-0 m-0 text-muted"><?=$products->num_rows?> <?=$products->num_rows===1 ? 'Product' : 'Products'?></small>
        </div>        
        <div class="w-50 d-flex justify-content-between align-items-center">
            <a href="<?="{$Router->getBasePath()}admin/products/add"?>" class="btn add-new-button text-decoration-none mr-3">
                <i class="fas fa-plus-circle"></i> Add New Product
            </a>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="search-product-addon">
                        <i class="fas fa-search"></i>
                    </span>                
                </div>
                <input type="text" class="form-control" placeholder="search product by keyword" aria-label="search product by keyword" aria-describedby="search-product-addon">            
            </div>
        </div>        
    </div>    
    <div class="spacer30"></div>
    <?php include("src/components/ui/tables/products.table.inc.php"); ?>
</div>