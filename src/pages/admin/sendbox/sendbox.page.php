<?php
$sendboxes = $Db->select('sendbox','*',null,'created_at DESC');
?>
<div class="admin-page-container p-0">    
    <div class="row">
        <div class="col-3 bg-white p-3 border-right">
            <button class="btn btn-primary btn-large">
                <i class="fas fa-plus-circle"></i> New Message                
            </button>
        </div>
        <div class="col-9">
            <div class="w-50 d-flex justify-content-end align-items-center">                
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="search-product-addon">
                            <i class="fas fa-search"></i>
                        </span>                
                    </div>
                    <input type="text" class="form-control" placeholder="search product by keyword" aria-label="search product by keyword" aria-describedby="search-product-addon">            
                </div>
            </div>
        </div>
    </div>
</div>