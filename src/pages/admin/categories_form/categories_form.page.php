<?php
if($page->params) $category = $Db->select('categories','*',"ID={$page->params[0]}")->fetch_object();
?>
<div class="admin-page-container p-4">    
    <h6 class="text-muted mb-2">
        <a href="<?="{$Router->getBasePath()}admin/categories"?>" class="text-muted text-decoration-none">
            <i class="fas fa-long-arrow-alt-left mr-1"></i> Categories
        </a>        
    </h6>
    <div class="d-flex justify-content-start align-items-center">        
        <h5 class="p-0 m-0"><?=isset($category) ? 'Edit' : 'Add'?> Category</h5>                              
    </div>    
    <div class="spacer30"></div>
    <div class="bg-white p-3 shadow-sm">
        <form action="<?=\Karoo\Core\Configuration::getWsUrl()?>" method="POST" class="needs-validation" autocomplete="off" novalidate>            
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="name">Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" autocomplete="off" name="name" id="name" value="<?=$category->category?>" placeholder="e.g. Category demo" required />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="slug">Slug</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" autocomplete="off" name="slug" id="slug" value="<?=$category->slug?>" placeholder="e.g. categody-demo" required />
                </div>
            </div>             
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="description">Description (optional)</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="description" id="description" rows="5" placeholder="Add description here"><?=$category->description?></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="icon">Icon (optional)</label>
                <div class="col-sm-9">                    
                    <?php if($category->icon): ?>
                        <p class="mb-1">Current Icon: <i class="<?=$category->icon?>"></i></p>
                    <?php endif; ?>
                    <input type="text" name="icon" autocomplete="off" id="icon" class="form-control" value="<?=$category->icon?>" placeholder="e.g. fas fa-home" />                    
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="image">Image (optional)</label>
                <div class="col-sm-9">
                    <?php if($category && $category->image): ?>
                        <div class="border p-2 d-flex justify-content-flex-start align-items-center">
                            <img class="border" src="<?=__FILE_ROOT__?>/<?=$category->image?>" height="100" alt="category picture" />
                            <div class="ml-3">
                                <p class="text-secondary text-break">Path: <small><?=$category->image?></small></p>                                
                                <p class="text-black-50">Size: <?=round(filesize($category->image) / 1024, 2)?> kb</p>
                            </div>
                        </div>
                        <div class="spacer10"></div>
                    <?php endif; ?>
                    <div class="custom-file">
                        <input type="file" name="image" class="item-image custom-file-input" id="customFile" accept=".png, .jpg, .jpeg" />
                        <label class="custom-file-label" for="customFile">
                            <?=isset($category) ? 'Change image' : 'Choose image' ?>
                        </label>
                        <div class="item-image-preview"></div>
                    </div>
                </div>
            </div>        
            <div class="spacer20"></div>
            <input type="hidden" name="action" value="add-license">
            <div class="text-center">
                <a class="btn btn-secondary" href="<?="{$Router->getBasePath()}admin/categories"?>">
                    Cancel
                </a>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>        
        </form>
    </div>
</div>