<div class="container mt-5">
    <div class="row">
        <div class="col-12 col-sm-12" style="">
            <div id="map" class="mt-5" style="width:100%; min-height:600px"></div>
        </div>
    </div>
    <div class="spacer50"></div>
</div>
<div class="container resellers-list">
    <div class="row">
        <div class="col-12 col-sm-6">
            <div class="m-t-50" style="width:100%; min-height:600px">
                <h2>Resellers</h2>
                <div class="spacer10"></div>
                <ul>
                    <h5 class="country">ARGENTINA</h5>
                    <li class="reseller-unit">
                        <img src="<?=__FILE_ROOT__?>/src/assets/images/reseller-southamericarigging.jpg" alt="South American Rigging - reseller UbiMaiorItalia - Argentina" width="150" height="65">
                        <p>
                            <b>SOUTH AMERICAN RIGGING</b> <br>
                            <span>Escalada 2028,<br>
                            1646 San Fernando, Argentina<br>
                            <b>Phone:</b> +54 114745-0031<br>
                            <b>Email:</b> <a href="mailto.fioriti@rigging.com.ar">fioriti@rigging.com.ar</a> <br>
                            <b><a href="http://www.rigging.com.ar/" target="_blank">Visit web </a></b></span>
                        </p>
                    </li>
                    <h5 class="country">AUSTRALIA</h5>
                    <li class="reseller-unit">
                        <img src="<?=__FILE_ROOT__?>/src/assets/images/reseller-myyacht.png" alt="My Yacht reseller UbiMaiorItalia - Australia" width="150" height="65">
                        <p>
                            <b>MY YACHT</b> <br>
                            <span>Kirribilli Marina, Unit 14-1 Bradly Ave Milsons Point<br>
                            NSW 2061, Australia<br>
                            <b>Email:</b> <a href="mailto.parts@myyacht.com.au">parts@myyacht.com.au</a> <br>
                            <b><a href="http://www.myyacht.com.au/" target="_blank">Visit web </a></b></span>
                        </p>
                    </li>
                    <h5 class="country">ENGLAND</h5>
                    <li class="reseller-unit">
                        <img src="<?=__FILE_ROOT__?>/src/assets/images/reseller-rigshop.jpg" alt="Rigshop reseller UbiMaiorItalia - England" width="150" height="65">
                        <p>
                            <b>THE RIG SHOP</b> <br>
                            <span>	Unit 7, Saxon Wharf Lower York Street<br>
                            SO14 5QF, Northam Southampton, UK<br>
                            <b>Phone:</b> +44 (0)2380338341<br>
                            <b>Email:</b> <a href="mailto.sales@rigshop.com">sales@rigshop.com</a> <br>
                            <b><a href="http://www.rigshop.com/" target="_blank">Visit web </a></b></span>
                        </p>
                    </li>
                    <h5 class="country">GERMANY</h5>
                    <li class="reseller-unit">
                        <img src="<?=__FILE_ROOT__?>/src/assets/images/reseller-lindemann.jpg" alt="EuroMarine reseller UbiMaiorItalia - USA" width="150" height="65">
                        <p>
                            <b>LINDEMANN</b> <br>
                            <span>Wendenstr. 455<br>
                            20537 Hamburg, Germany <br>
                            <b>Phone:</b> +49 (0)40211197 - 12 <br>
                            <b>Email:</b> <a href="mailto.verkauf@lindemann-kg.de">verkauf@lindemann-kg.de</a> <br>
                            <b><a href="https://www.lindemann-kg.de/en/" target="_blank">Visit web </a></b></span>
                        </p>
                    </li>
                    <h5 class="country">GREECE</h5>
                    <li class="reseller-unit">
                        <img src="<?=__FILE_ROOT__?>/src/assets/images/reseller-atlantamarine.jpg" alt="Atlanta Marine reseller UbiMaiorItalia - Greece" width="150" height="65">
                        <p>
                            <b>ATLANTA MARINE</b> <br>
                            <span>Iosifidou 3, Elliniko Argiroupoli<br>
                            16777, Athens, Greece<br>
                            <b>Phone:</b> +30 2109910722<br>
                            <b>Email:</b> <a href="mailto.info@atalantamarine.com">info@atalantamarine.com</a> <br>
                            <b><a href="https://atalantamarine.com/" target="_blank">Visit web </a></b></span>
                        </p>
                    </li>
                    <h5 class="country">FINLAND</h5>
                    <li class="reseller-unit">
                        <img src="<?=__FILE_ROOT__?>/src/assets/images/reseller-ropex.jpg" alt="Ropex reseller UbiMaiorItalia - Finland" width="150" height="65">
                        <p>
                            <b>ROPEX</b> <br>
                            <span>Turku, 20200 Finland<br>
                            <b>Phone:</b> +358 (0)504064 689<br>
                            <b>Email:</b> <a href="mailto.myynti@ropex.fi">myynti@ropex.fi</a> <br>
                            <b><a href="https://ropex.fi/" target="_blank">Visit web </a></b></span>
                        </p>
                    </li>
                    <h5 class="country">FRANCE</h5>
                    <li class="reseller-unit">
                        <img src="<?=__FILE_ROOT__?>/src/assets/images/reseller-nautexinternational.jpg" alt="Nautex International - reseller UbiMaiorItalia - France" width="150" height="65">
                        <p>
                            <b>NAUTEX INTERNATIONAL</b> <br>
                            <span>15 rue de la Brigantine<br>
                            17000 La Rochelle, France<br>
                            <b>Phone:</b> +33 0546520104<br>
                            <b>Email:</b> <a href="mailto.info@nautex-international.com">info@nautex-international.com</a> <br>
                            <b><a href="https://www.nautex-international.com" target="_blank">Visit web </a></b></span>
                        </p>
                    </li>
                    <h5 class="country">HOLLAND</h5>
                    <li class="reseller-unit">
                        <img src="<?=__FILE_ROOT__?>/src/assets/images/reseller-rake.png" alt="Rake reseller UbiMaiorItalia - Holland" width="150" height="65">
                        <p>
                            <b>RAKE RIGGING</b> <br>
                            <span>	1 Overleek 7<br>
                            1671 GD Medemblik, Holland<br>
                            <b>Phone:</b> +31 653604628<br>
                            <b>Email:</b> <a href="mailto.info@rakerigging.nl">info@rakerigging.nl</a> <br>
                            <b><a href="https://rakerigging.com/" target="_blank">Visit web </a></b></span>
                        </p>
                    </li>
                    <h5 class="country">HUNGARY</h5>
                    <li class="reseller-unit">
                        <img src="<?=__FILE_ROOT__?>/src/assets/images/reseller-csepregi.jpg" alt="Csepregi Vizisport reseller UbiMaiorItalia - Hungary" width="150" height="65">
                        <p>
                            <b>CSEPREGI VIZISPORT</b> <br>
                            <span>	2 Weiss Manfréd út<br>
                            1211, Budapest Hungary<br>
                            <b>Phone:</b> +36 12764873<br>
                            <b>Email:</b> <a href="mailto.info@csepregi-vizisport.hu">info@csepregi-vizisport.hu</a> <br>
                            <b><a href="https://www.csepregi-vizisport.hu/" target="_blank">Visit web </a></b></span>
                        </p>
                    </li>
                    <h5 class="country">SPAIN</h5>
                    <li class="reseller-unit">
                        <img src="<?=__FILE_ROOT__?>/src/assets/images/reseller-evmmarine.jpg" alt="Emvmarine reseller UbiMaiorItalia - Spain Barcelona" width="150" height="65">
                        <p>
                            <b>EMV MARINE</b> <br>
                            <span>	Moll de la Ribera local 13-14<br>
                            08912, Badalona, Barcelona, Spain<br>
                            <b>Phone:</b> +34 933207531<br>
                            <b>Email:</b> <a href="mailto.info@emvmarine.com">info@emvmarine.com</a> <br>
                            <b><a href="http://www.emvmarine.com/" target="_blank">Visit web </a></b></span>
                        </p>
                    </li>
                    <li class="reseller-unit">
                        <img src="<?=__FILE_ROOT__?>/src/assets/images/reseller-rsbrigging.jpg" alt="RSB rigging reseller UbiMaiorItalia - Spain Palma de Maiorca" width="150" height="65">
                        <p>
                            <b>RSB RIGGING</b> <br>
                            <span>Edificio Global Espigon Exterior, S/N Oficina 24<br>
                            07012 Palma, Balearic Islands, Spain<br>
                            <b>Phone:</b> +34 971495931<br>
                            <b>Email:</b> <a href="mailto.russ@rsb-rigging.com">russ@rsb-rigging.com</a> <br>
                            <b><a href="http://www.rsb-rigging.com/" target="_blank">Visit web </a></b></span>
                        </p>
                    </li>
                    <h5 class="country">TAIWAN</h5>
                    <li class="reseller-unit">
                        <img src="<?=__FILE_ROOT__?>/src/assets/images/reseller-offshoreperformancetaiwan.jpg" alt="Offshore performance - reseller UbiMaiorItalia - Taiwan" width="150" height="65">
                        <p>
                            <b>TAIWAN OFFSHORE PERFORMANCE</b> <br>
                            <span>25F-1B, No.333 JiangKang Rd. Sec.3<br>
                            Anping District Tainan City 70843, Taiwan<br>
                            <b>Phone:</b> +886 983732550<br>
                            <b>Email:</b> <a href="mailto.topsailing.tw@gmail.com">topsailing.tw@gmail.com</a> <br>
                            <b><a href="https://www.facebook.com/topsailing.tw" target="_blank">Visit web </a></b></span>
                        </p>
                    </li>
                    <h5 class="country">TURKEY</h5>
                    <li class="reseller-unit">
                        <img src="<?=__FILE_ROOT__?>/src/assets/images/reseller-uksailmakersturkey.jpg" alt="UK Sailmakers - reseller UbiMaiorItalia - Turkey" width="150" height="65">
                        <p>
                            <b>UK SAILMAKERS TURKEY</b> <br>
                            <span>Marmaris, 48700<br>
                            Turkey<br>
                            <b>Phone:</b> +90 252 412 27 00 / 01<br>
                            <b>Email:</b> <a href="mailto.uksailmakers@mss.com.tr">uksailmakers@mss.com.tr</a> <br>
                            <b><a href="https://www.uksailmakersturkey.com" target="_blank">Visit web </a></b></span>
                        </p>
                    </li>
                    <h5 class="country">USA</h5>
                    <li class="reseller-unit">
                        <img src="<?=__FILE_ROOT__?>/src/assets/images/reseller-euromarine.jpg" alt="EuroMarine reseller UbiMaiorItalia - USA" width="150" height="65">
                        <p>
                            <b>EURO MARINE TRADING INC</b> <br>
                            <span>62 Halsey Street, Unit M <br>
                            02840, Newport, RI Usa <br>
                            <b>Phone:</b> +001 401-849-0060 <br>
                            <b>Email:</b> <a href="mailto.info@euromarinetrading.com">info@euromarinetrading.com</a> <br>
                            <b><a href="https://euromarinetrading.com/" target="_blank">Visit web </a></b></span>
                        </p>
                    </li>
                    <h5 class="country">NEW</h5>
                    <li class="reseller-unit">
                        <img src="<?=__FILE_ROOT__?>/src/assets/images/reseller-blank.png" alt="Ubimaior Italia resellers list" width="150" height="65">
                        <p>
                            <b>CONTACT US</b> <br>
                            <span>Are you are interested in becoming<br>
                            a new reseller for UbiMaiorItalia?<br>
                            <b>Call us:</b> +39 055 8364421 <br>
                            <b>Or email us:</b> <a href="mailto.info@ubimaioritalia.com">info@ubimaioritalia.com</a> <br>
                        </p>
                    </li>
                    <div class="spacer50"></div>
                </ul>
            </div>
        </div>
        <div class="col-12 col-sm-6">
            <div class="m-t-50" style="width:100%; min-height:600px">
                <h2>Maior Tech Team</h2>
                <div class="spacer10"></div>
                <ul>
                    <h5 class="country">ITALY</h5>
                    <li class="reseller-unit">
                        <div>
                            <img src="<?=__FILE_ROOT__?>/src/assets/images/techteam-osn.jpg" alt="OSN rigging Tech Team UbiMaiorItalia - Italy Pisa" width="150" height="65"><br>
                            <p class="fornitore-strallo"> &nbsp;<?=$lang[$_COOKIE['ubi_lang']]['_ALL_ROD_SUPPLIER']?></p>
                        </div>    
                        <p>
                            <b>OSN YACHT RIGGING</b> <br>
                            <span>Strada Provinciale Val di Cecina 41/11<br>
                            56040 Montescudaio, Pisa - Italy<br>
                            <b>Phone:</b> +39 339 6942222 / +39 0586 686330<br>
                            <b>Email:</b> <a href="mailto.info@osnrigging.com">info@osnrigging.com</a> <br>
                            <b><a href="http://www.osnrigging.com/" target="_blank">Visit web </a></b></span>
                        </p>
                    </li>
                    <h5 class="country">NEW</h5>
                    <li class="reseller-unit">
                        <img src="<?=__FILE_ROOT__?>/src/assets/images/reseller-blank.png" alt="Ubimaior Italia resellers list" width="150" height="65">
                        <p>
                            <b>CONTACT US</b> <br>
                            <span>Are you are interested in becoming<br>
                            part of our Tech Team?<br>
                            <b>Call us:</b> +39 055 8364421 <br>
                            <b>Or email us:</b> <a href="mailto.info@ubimaioritalia.com">info@ubimaioritalia.com</a> <br>
                        </p>
                    </li>
                    <div class="spacer20"></div>
                    <h4 style="color:#fff;">Section in progress</h4>
                   <!-- <li class="reseller-unit">
                        <img src="<?=__FILE_ROOT__?>/src/assets/images/techteam-rigpro.jpg" alt="Rig Pro Tech Team UbiMaiorItalia - Italy Fano" width="150" height="65">
                        <p>
                            <b>RIG PRO</b> <br>
                            <span>	Lungomare Mediterraneo, 26<br>
                            61032 Fano PU, Italy<br>
                            <b>Phone:</b> +39 339 731 8541 / +39 0721 846231<br>
                            <b>Email:</b> <a href="mailto.service-pro@sailsolutions.eu">service-pro@sailsolutions.eu</a> <br>
                            <b><a href="https://rig-pro.com/" target="_blank">Visit web </a></b></span>
                        </p>
                    </li>
                    <li class="reseller-unit">
                        <img src="<?=__FILE_ROOT__?>/src/assets/images/techteam-rigpro.jpg" alt="Rig Pro Tech Team UbiMaiorItalia - Italy La Spezia" width="150" height="65">
                        <p>
                            <b>RIG PRO</b> <br>
                            <span>	Viale Giovanni Amendola, 1<br>
                            19121 La Spezia SP, Italy<br>
                            <b>Phone:</b> +39 339 731 8541 / +39 0721 846231<br>
                            <b>Email:</b> <a href="mailto.service-pro@sailsolutions.eu">service-pro@sailsolutions.eu</a> <br>
                            <b><a href="https://rig-pro.com/" target="_blank">Visit web </a></b></span>
                        </p>
                    </li>
                    <h5 class="country">HOLLAND</h5>
                    <li class="reseller-unit">
                        <img src="<?=__FILE_ROOT__?>/src/assets/images/techteam-rigpro.jpg" alt="Rig Pro Tech Team UbiMaiorItalia - Holland" width="150" height="65">
                        <p>
                            <b>RIG PRO</b> <br>
                            <span>Breskens - Netherlands<br>
                            <b>Phone:</b> +31 117381397<br>
                            <b>Email:</b> <a href="mailto.enquiries@rig-pro.com">enquiries@rig-pro.com</a> <br>
                            <b><a href="https://rig-pro.com/" target="_blank">Visit web </a></b></span>
                        </p>
                    </li>
                    <h5 class="country">SPAIN</h5>
                    <li class="reseller-unit">
                        <img src="<?=__FILE_ROOT__?>/src/assets/images/reseller-rsbrigging.jpg" alt="RSB rigging reseller UbiMaiorItalia - Spain Palma de Maiorca" width="150" height="65">
                        <p>
                            <b>RSB RIGGING</b> <br>
                            <span>Edificio Global Espigon Exterior, S/N Oficina 24<br>
                            07012 Palma, Balearic Islands, Spain<br>
                            <b>Phone:</b> +34 971495931<br>
                            <b>Email:</b> <a href="mailto.russ@rsb-rigging.com">russ@rsb-rigging.com</a> <br>
                            <b><a href="http://www.rsb-rigging.com/" target="_blank">Visit web </a></b></span>
                        </p>
                    </li>
                    <li class="reseller-unit">
                        <img src="<?=__FILE_ROOT__?>/src/assets/images/techteam-rigpro.jpg" alt="Rigshop Tech Team UbiMaiorItalia - Spain Valencia" width="150" height="65">
                        <p>
                            <b>RIG PRO</b> <br>
                            <span>Valencia, Spain<br>
                            <b>Phone:</b> +34 961452135 <br>
                            <b>Email:</b> <a href="mailto.enquiries@rig-pro.com">enquiries@rig-pro.com</a> <br>
                            <b><a href="https://rig-pro.com/" target="_blank">Visit web </a></b></span>
                        </p>
                    </li>
                    <li class="reseller-unit">
                        <img src="<?=__FILE_ROOT__?>/src/assets/images/techteam-rigpro.jpg" alt="Rigshop Tech Team UbiMaiorItalia - Spain Palma de Maiorca" width="150" height="65">
                        <p>
                            <b>RIG PRO</b> <br>
                            <span>Palma de Maiorca, Spain<br>
                            <b>Phone:</b> +34 871719874 <br>
                            <b>Email:</b> <a href="mailto.enquiries@rig-pro.com">enquiries@rig-pro.com</a> <br>
                            <b><a href="https://rig-pro.com/" target="_blank">Visit web </a></b></span>
                        </p>
                    </li>
                    <h5 class="country">USA</h5>
                    <li class="reseller-unit">
                        <img src="<?=__FILE_ROOT__?>/src/assets/images/techteam-rigpro.jpg" alt="Rigshop Tech Team UbiMaiorItalia - USA" width="150" height="65">
                        <p>
                            <b>RIG PRO</b> <br>
                            <span>Newport - USA<br>
                            <b>Phone:</b> +1 401 683 6966<br>
                            <b>Email:</b> <a href="mailto.enquiries@rig-pro.com">enquiries@rig-pro.com</a> <br>
                            <b><a href="https://rig-pro.com/" target="_blank">Visit web </a></b></span>
                        </p>
                    </li>-->
                    <div class="spacer50"></div>
                </ul> 
            </div>
        </div>
    </div>
</div>


<script>
    function initMap() {
        var markerRed = 'images/marker-red.png';
        var markerWhite = 'images/marker-white.png';

        var Holland = '<div><img src="<?=__FILE_ROOT__?>/src/assets/images/reseller-rake.png" alt="" /></div><div class="panel border-0"><div class="panel-heading"><div class="panel-title">RAKE RIGGING</div></div><div class="panel-body"><table class="infoWindowTable"><tr><td class="text-center"><i class="fa fa-map-marker"></i></td><td>1  Overleek 7 <br>1671 GD Medemblik<br>Holland<br>Ph. +31 653604628</td></tr><tr><td class="text-center"><i class="fa fa-globe"></i></td><td><a href="https://rakerigging.com/" target="_blank" class="map-link">www.rakerigging.nl</a></td></tr><tr><td class="text-center"><i class="fa fa-envelope"></i></td><td><a href="mailto:info@rakerigging.nl" class="map-link">info@rakerigging.nl</a></td></tr></table></div></div>';
        
        var Italy = '<div><img src="<?=__FILE_ROOT__?>/src/assets/images/reseller-ubi.jpg" alt="" /></div><div class="panel border-0"><div class="panel-heading"><div class="panel-title">UBI MAIOR ITALIA</div></div><div class="panel-body"><table class="infoWindowTable"><tr><td class="text-center"><i class="fa fa-map-marker"></i></td><td>Via Serravalle 35-39 <br>50065 Molino del Piano<br>Italy<br>Ph. +39 0558364421</td></tr><tr><td class="text-center"><i class="fa fa-globe"></i></td><td>www.ubimaioritalia.com</td></tr><tr><td class="text-center"><i class="fa fa-envelope"></i></td><td><a href="mailto:info@ubimaioritalia.com" class="map-link">info@ubimaioritalia.com</a></td></tr></table></div></div>';
        
        var England = '<div><img src="<?=__FILE_ROOT__?>/src/assets/images/reseller-rigshop.jpg" alt="" /></div><div class="panel border-0"><div class="panel-heading"><div class="panel-title">THE RIG SHOP</div></div><div class="panel-body"><table class="infoWindowTable"><tr><td class="text-center"><i class="fa fa-map-marker"></i></td><td>Unit 7, Saxon Wharf Lower York Street <br>SO14 5QF, Northam Southampton<br>UK<br>Ph. +44 (0)2380338341</td></tr><tr><td class="text-center"><i class="fa fa-globe"></i></td><td><a href="http://www.rigshop.com/" target="_blank" class="map-link">www.rigshop.com</a></td></tr><tr><td class="text-center"><i class="fa fa-envelope"></i></td><td><a href="mailto:sales@rigshop.com" class="map-link">sales@rigshop.com</a></td></tr></table></div></div>';
        
        var SpainBarcelona = '<div><img src="<?=__FILE_ROOT__?>/src/assets/images/reseller-evmmarine.jpg" alt="" /></div><div class="panel border-0"><div class="panel-heading"><div class="panel-title">EMV MARINE</div></div><div class="panel-body"><table class="infoWindowTable"><tr><td class="text-center"><i class="fa fa-map-marker"></i></td><td>Moll de la Ribera local 13-14<br>08912, Badalona, Barcelona<br>Spain<br>Ph. +34 933207531</td></tr><tr><td class="text-center"><i class="fa fa-globe"></i></td><td><a href="http://www.emvmarine.com/" target="_blank" class="map-link">www.emvmarine.com</a></td></tr><tr><td class="text-center"><i class="fa fa-envelope"></i></td><td><a href="mailto:info@emvmarine.com" class="map-link">info@emvmarine.com</a></td></tr></table></div></div>';
        
        var SpainPalma = '<div><img src="<?=__FILE_ROOT__?>/src/assets/images/reseller-rsbrigging.jpg" alt="" /></div><div class="panel border-0"><div class="panel-heading"><div class="panel-title">RSB RIGGING</div></div><div class="panel-body"><table class="infoWindowTable"><tr><td class="text-center"><i class="fa fa-map-marker"></i></td><td>Edificio Global Espigon Exterior, S/N Oficina 24<br>07012 Palma, Balearic Islands<br>Spain<br>Ph. +34 971495931</td></tr><tr><td class="text-center"><i class="fa fa-globe"></i></td><td><a href="http://www.rsb-rigging.com/" target="_blank" class="map-link">www.rsb-rigging.com</a></td></tr><tr><td class="text-center"><i class="fa fa-envelope"></i></td><td><a href="mailto:russ@rsb-rigging.com" class="map-link">russ@rsb-rigging.com</a></td></tr></table></div></div>';
        
        var Australia = '<div><img src="<?=__FILE_ROOT__?>/src/assets/images/reseller-myyacht.png" alt="" /></div><div class="panel border-0"><div class="panel-heading"><div class="panel-title">MY YACHT</div></div><div class="panel-body"><table class="infoWindowTable"><tr><td class="text-center"><i class="fa fa-map-marker"></i></td><td>Kirribilli Marina<br> Unit 14-1 Bradly Ave <br> Milsons Point <br> NSW 2061 <br> Australia</td></tr><tr><td class="text-center"><i class="fa fa-globe"></i></td><td><a href="http://www.myyacht.com.au/" target="_blank" class="map-link">www.myyacht.com.au</a></td></tr><tr><td class="text-center"><i class="fa fa-envelope"></i></td><td><a href="mailto:parts@myyacht.com.au" class="map-link">parts@myyacht.com.au</a></td></tr></table></div></div>';
        
        var Hungary = '<div><img src="<?=__FILE_ROOT__?>/src/assets/images/reseller-csepregi.jpg" alt="" /></div><div class="panel border-0"><div class="panel-heading"><div class="panel-title">CSEPREGI VIZISPORT</div></div><div class="panel-body"><table class="infoWindowTable"><tr><td class="text-center"><i class="fa fa-map-marker"></i></td><td>2 Weiss Manfréd út <br> 1211, Budapest <br> Hungary <br>Ph. +36 12764873</td></tr><tr><td class="text-center"><i class="fa fa-globe"></i></td><td><a href="https://www.csepregi-vizisport.hu/" target="_blank" class="map-link">www.csepregi-vizisport.hu</a></td></tr><tr><td class="text-center"><i class="fa fa-envelope"></i></td><td><a href="mailto:info@csepregi-vizisport.hu" class="map-link">info@csepregi-vizisport.hu</a></td></tr></table></div></div>';
        
        var Usa = '<div><img src="<?=__FILE_ROOT__?>/src/assets/images/reseller-euromarine.jpg" alt="" /></div><div class="panel border-0"><div class="panel-heading"><div class="panel-title">EURO MARINE TRADING INC</div></div><div class="panel-body"><table class="infoWindowTable"><tr><td class="text-center"><i class="fa fa-map-marker"></i></td><td>62 Halsey Street, Unit M <br> 02840, Newport, RI <br> Usa<br>Ph. +001 401-849-0060</td></tr><tr><td class="text-center"><i class="fa fa-globe"></i></td><td><a href="https://euromarinetrading.com/" target="_blank" class="map-link">www.euromarinetrading.com</a></td></tr><tr><td class="text-center"><i class="fa fa-envelope"></i></td><td><a href="mailto:info@euromarinetrading.com" class="map-link">info@euromarinetrading.com</a></td></tr></table></div></div>';
        
        var Greece = '<div><img src="<?=__FILE_ROOT__?>/src/assets/images/reseller-atlantamarine.jpg" alt="" /></div><div class="panel border-0"><div class="panel-heading"><div class="panel-title">ATLANTA MARINE</div></div><div class="panel-body"><table class="infoWindowTable"><tr><td class="text-center"><i class="fa fa-map-marker"></i></td><td>Iosifidou 3, Elliniko Argiroupoli <br> 16777, Athens <br> Greece<br>Ph. +30 2109910722</td></tr><tr><td class="text-center"><i class="fa fa-globe"></i></td><td><a href="https://atalantamarine.com/" target="_blank" class="map-link">www.atalantamarine.com</a></td></tr><tr><td class="text-center"><i class="fa fa-envelope"></i></td><td><a href="mailto:info@atalantamarine.com" class="map-link">info@atalantamarine.com</a></td></tr></table></div></div>';

        var Germany = '<div><img src="<?=__FILE_ROOT__?>/src/assets/images/reseller-lindemann.jpg" alt="" /></div><div class="panel border-0"><div class="panel-heading"><div class="panel-title">LINDEMANN</div></div><div class="panel-body"><table class="infoWindowTable"><tr><td class="text-center"><i class="fa fa-map-marker"></i></td><td>Wendenstr. 455 <br>20537 Hamburg<br> Germany <br>Ph. +49 (0)40211197 - 12</td></tr><tr><td class="text-center"><i class="fa fa-globe"></i></td><td><a href="https://www.lindemann-kg.de/en/" target="_blank" class="map-link">www.lindemann-kg.de</a></td></tr><tr><td class="text-center"><i class="fa fa-envelope"></i></td><td><a href="mailto:verkauf@lindemann-kg.de" class="map-link">verkauf@lindemann-kg.de</a></td></tr></table></div></div>';

        var Finland = '<div><img src="<?=__FILE_ROOT__?>/src/assets/images/reseller-ropex.jpg" alt="" /></div><div class="panel border-0"><div class="panel-heading"><div class="panel-title">ROPEX</div></div><div class="panel-body"><table class="infoWindowTable"><tr><td class="text-center"><i class="fa fa-map-marker"></i></td><td>Turku, 20200 <br> Finland <br>Ph. +358 (0)504064 689</td></tr><tr><td class="text-center"><i class="fa fa-globe"></i></td><td><a href="https://ropex.fi/" target="_blank" class="map-link">www.ropex.fi</a></td></tr><tr><td class="text-center"><i class="fa fa-envelope"></i></td><td><a href="mailto:myynti@ropex.fi" class="map-link">myynti@ropex.fi</a></td></tr></table></div></div>';

        var Turkey = '<div><img src="<?=__FILE_ROOT__?>/src/assets/images/reseller-uksailmakersturkey.jpg" alt="" /></div><div class="panel border-0"><div class="panel-heading"><div class="panel-title">UK SAILMAKERS TURKEY</div></div><div class="panel-body"><table class="infoWindowTable"><tr><td class="text-center"><i class="fa fa-map-marker"></i></td><td>Marmaris, 48700 <br> Turkey <br>Ph. +90 252 412 27 00 / 01</td></tr><tr><td class="text-center"><i class="fa fa-globe"></i></td><td><a href="https://www.uksailmakersturkey.com/" target="_blank" class="map-link">www.uksailmakersturkey.com</a></td></tr><tr><td class="text-center"><i class="fa fa-envelope"></i></td><td><a href="mailto:uksailmakers@mss.com.tr" class="map-link">uksailmakers@mss.com.tr</a></td></tr></table></div></div>';

        var Argentina = '<div><img src="<?=__FILE_ROOT__?>/src/assets/images/reseller-southamericarigging.jpg" alt="" /></div><div class="panel border-0"><div class="panel-heading"><div class="panel-title">SOUTH AMERICAN RIGGING</div></div><div class="panel-body"><table class="infoWindowTable"><tr><td class="text-center"><i class="fa fa-map-marker"></i></td><td>Escalada 2028, 1646 San Fernando <br> Argentina <br>Ph. +54 114745-0031</td></tr><tr><td class="text-center"><i class="fa fa-globe"></i></td><td><a href="http://www.rigging.com.ar/" target="_blank" class="map-link">www.rigging.com.ar</a></td></tr><tr><td class="text-center"><i class="fa fa-envelope"></i></td><td><a href="mailto:fioriti@rigging.com.ar" class="map-link">fioriti@rigging.com.ar</a></td></tr></table></div></div>';

        var France = '<div><img src="<?=__FILE_ROOT__?>/src/assets/images/reseller-nautexinternational.jpg" alt="" /></div><div class="panel border-0"><div class="panel-heading"><div class="panel-title">NAUTEX INTERNATIONAL</div></div><div class="panel-body"><table class="infoWindowTable"><tr><td class="text-center"><i class="fa fa-map-marker"></i></td><td>15 rue de la Brigantine, 17000 LA ROCHELLE <br> France <br>Ph. +33 0546520104</td></tr><tr><td class="text-center"><i class="fa fa-globe"></i></td><td><a href="https://www.nautex-international.com/" target="_blank" class="map-link">www.nautex-international.com</a></td></tr><tr><td class="text-center"><i class="fa fa-envelope"></i></td><td><a href="mailto:info@nautex-international.com" class="map-link">info@nautex-international.com</a></td></tr></table></div></div>';

        var Taiwan = '<div><img src="<?=__FILE_ROOT__?>/src/assets/images/reseller-offshoreperformancetaiwan.jpg" alt="" /></div><div class="panel border-0"><div class="panel-heading"><div class="panel-title">TAIWAN OFFSHORE PREFORMANCE</div></div><div class="panel-body"><table class="infoWindowTable"><tr><td class="text-center"><i class="fa fa-map-marker"></i></td><td>25F-1B, No.333 JiangKang Rd. Sec.3<br> Anping District, Tainan City 70843, Taiwan<br>Ph. +886 983732550</td></tr><tr><td class="text-center"><i class="fa fa-globe"></i></td><td><a href="https://www.facebook.com/topsailing.tw" target="_blank" class="map-link">www.facebook.com/topsailing.tw</a></td></tr><tr><td class="text-center"><i class="fa fa-envelope"></i></td><td><a href="mailto:topsailing.tw@gmail.com" class="map-link">topsailing.tw@gmail.com</a></td></tr></table></div></div>';
        
        var locations = [
            [Italy, 43, 12, 1, markerRed],
            [England, 50.91, -1.38, 2, markerWhite],
            [Argentina, -34.44, -58.54, 3, markerWhite],
            [SpainBarcelona, 41.39, 2.07, 4, markerWhite],
            [SpainPalma, 39.57, 2.64, 5, markerWhite],
            [Hungary, 47.46, 19.17, 6, markerWhite],
            [Holland, 52.12, 5.3, 7, markerWhite],
            [Australia, -33.84, 151.21, 8, markerWhite],
            [Usa, 41.5, -71.3, 9, markerWhite],
            [Greece, 37.9, 23.7, 10, markerWhite],
            [Germany, 53.54, 10.05, 11, markerWhite],
            [Finland, 60.44, 22.20, 12, markerWhite],
            [Turkey, 36.85, 28.26, 13, markerWhite],
            [France, 46.14, -1.16, 14, markerWhite],
            [Taiwan, 22.98, 120.16, 3, markerWhite]
        ];

        // Create a map object and specify the DOM element for display.
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {
                lat: 23,
                lng: 63
            },

            scrollwheel: false,
            mapTypeControl: false,
            streetViewControl: false,
            zoom: 2,
            backgroundColor: 'none',
            styles: [{
                "featureType": "all",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#000000"
                }]
            }, {
                "featureType": "all",
                "elementType": "labels",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "all",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "gamma": 0.01
                }, {
                    "lightness": 20
                }]
            }, {
                "featureType": "all",
                "elementType": "labels.text.stroke",
                "stylers": [{
                    "saturation": -31
                }, {
                    "lightness": -33
                }, {
                    "weight": 2
                }, {
                    "gamma": 0.8
                }]
            }, {
                "featureType": "all",
                "elementType": "labels.icon",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "administrative",
                "elementType": "all",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "administrative.country",
                "elementType": "geometry.fill",
                "stylers": [{
                    "visibility": "simplified"
                }, {
                    "color": "#543f3f"
                }, {
                    "saturation": "54"
                }]
            }, {
                "featureType": "administrative.land_parcel",
                "elementType": "geometry.fill",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "landscape",
                "elementType": "geometry",
                "stylers": [{
                    "lightness": 30
                }, {
                    "saturation": 30
                }]
            }, {
                "featureType": "landscape.man_made",
                "elementType": "all",
                "stylers": [{
                    "color": "#4c4c51"
                }, {
                    "visibility": "off"
                }]
            }, {
                "featureType": "landscape.natural",
                "elementType": "geometry",
                "stylers": [{
                    "visibility": "on"
                }, {
                    "color": "#4c4c51"
                }]
            }, {
                "featureType": "landscape.natural",
                "elementType": "labels",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "landscape.natural.landcover",
                "elementType": "all",
                "stylers": [{
                    "visibility": "on"
                }, {
                    "color": "#4c4c51"
                }]
            }, {
                "featureType": "landscape.natural.landcover",
                "elementType": "geometry.fill",
                "stylers": [{
                    "visibility": "on"
                }, {
                    "color": "#4c4c51"
                }]
            }, {
                "featureType": "landscape.natural.landcover",
                "elementType": "geometry.stroke",
                "stylers": [{
                    "color": "#4c4c51"
                }]
            }, {
                "featureType": "landscape.natural.landcover",
                "elementType": "labels",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "poi",
                "elementType": "all",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [{
                    "saturation": 20
                }]
            }, {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [{
                    "lightness": 20
                }, {
                    "saturation": -20
                }]
            }, {
                "featureType": "road",
                "elementType": "all",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [{
                    "lightness": 10
                }, {
                    "saturation": -30
                }]
            }, {
                "featureType": "road",
                "elementType": "geometry.stroke",
                "stylers": [{
                    "saturation": 25
                }, {
                    "lightness": 25
                }]
            }, {
                "featureType": "transit",
                "elementType": "all",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "water",
                "elementType": "all",
                "stylers": [{
                    "lightness": -20
                }]
            }, {
                "featureType": "water",
                "elementType": "geometry.fill",
                "stylers": [{
                    "visibility": "on"
                }, {
                    "color": "#212127"
                }]
            }]
        });


        var infowindow = new google.maps.InfoWindow(
            //  content: contentString
        );

        var marker, i;

        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map,
                icon: locations[i][4]
            });

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    infowindow.setContent(locations[i][0]);
                    infowindow.open(map, marker);
                }
            })(marker, i));
        }
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyApj53Y-m2th0657HBR2WmInWfQHZVgsBw&callback=initMap" async defer></script>