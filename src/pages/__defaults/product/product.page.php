<?php 
$id_product = $page->params[0];
$catalogue = $Db->select('products p','p.id, p.name, p.description, p.image, pp.price, pp.vat, c.symbol','p.id='.$id_product,'name ASC',null,'LEFT JOIN '.$Db->getTablePrefix().'products_prices pp ON pp.id_product = p.id LEFT JOIN '.$Db->getTablePrefix().'currencies c ON c.id = pp.id_currency');
$product = $catalogue->fetch_object();
?>
<div class="row mt-4">
    <div class="col-sm-6 text-center">
        <img src="<?=__FILE_ROOT__."/{$product->image}"?>" width="100%" alt="demo product">
    </div>
    <div class="col-sm-6 h-100">        
        <div>
            <p class="mb-4"><a class="text-secondary" href="<?=$Router->getBasePath()?>catalogue">< Back to Catalogue</a></p>        
            <h5 class="font-weight-light">Demo</h5>
            <h3 class="font-weight-bold"><?=$product->name?></h3>
            <h5 class="font-weight-bold"><?="{$product->symbol} {$product->price}"?></h3>
            <hr />
            <p class="font-weight-light"><?=$product->description?></p>
        </div>
        <div class="spacer30"></div>
        <button type="button" class="btn btn-danger text-uppercase rounded-0">add to whishlist</button>
        <div class="spacer20"></div>
        <p class="mb-4"><a class="text-secondary" href="<?=$Router->getBasePath()?>checkout">Go to Checkout</a></p>
        <?php        
        // $intent = \Stripe\PaymentIntent::create([
        //     'amount' => 2000,
        //     'currency' => 'usd',
        //     'payment_method_types' => ['card'],
        // ]);
        ?>
        <!-- <button id="card-button" data-secret="<?= $intent->client_secret ?>">
            Submit Payment
        </button> -->
    </div>
</div>