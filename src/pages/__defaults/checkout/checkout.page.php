<form action="<?=\Karoo\Core\Configuration::getWsUrl()?>" method="POST" id="payment-form">
    <div class="form-row">
        <input type="text" name="amount" placeholder="Enter Amount" />
    </div>
    <div class="form-row">
        <label for="card-element">Credit or debit card</label>
        <div id="card-element">
        <!-- A Stripe Element will be inserted here. -->
        </div>
 
        <!-- Used to display form errors. -->
        <div id="card-errors" role="alert"></div>
    </div>
    <input type="hidden" name="action" value="process-payment">
    <button>Submit Payment</button>
</form>