<div style="background:rgb(222, 232, 243); width:100%; height:100vh;overflow:hidden;">
    <div style="margin:150px auto 0;width:75%;padding:100px;display:flex;justify-content:center;align-items:center;border-radius: 50px;border-radius: 50px; background: #dee8f3;box-shadow:  20px 20px 60px #bdc5cf, -20px -20px 60px #ffffff;">
        <div style="width:100%;">                    
            <h1>403 - Permission Denied.</h1>
            <p> Your user does not have grants to access the page requested. Check out <b><a href="<?=$Router->getBasePath()?>">the home page</a></b> instead, it's usually more accessible than this page</p>                        
        </div>
    </div>
</div>