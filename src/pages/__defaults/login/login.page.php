<div class="container" style="margin: 150px auto;">
    <div class="row justify-content-center">
        <div class="col col-6">    
            <div class="card">
                <div class="card-body">                                
                    <h3>Auth Required - Please login</h3>
                    <a href="<?=$Router->getBasePath()?>">Back to Home</a>
                    <div class="spacer20"></div>

                    <?=$Form->generateForm([
                        "action" => \Karoo\Core\Configuration::getWsUrl(),
                        "method" => "post",
                        "attributes" => [
                            "class" => "needs-validation",
                            "novalidate" => true
                        ],
                        "submit_text" => "Login",
                        "submit_classes" => "mx-auto",                        
                        "inputs" => [
                            [
                                "type" => "email",
                                "id" => "email",
                                "label" => "Email",                                                                
                                "attributes" => $Builder->generateAttributes([
                                    "name" => "email",
                                    "required" => true,
                                    "placeholder" => "insert email"
                                ]),
                                "invalid_feedback" => "Required field"
                            ],
                            [
                                "type" => "password",
                                "id" => "password",
                                "label" => "Password",
                                "attributes" => $Builder->generateAttributes([
                                    "name" => "password",
                                    "required" => true,
                                    "placeholder" => "insert password"                                    
                                ]),
                                "invalid_feedback" => "Required field"
                            ],
                            [
                                "type" => "hidden",
                                "attributes" => $Builder->generateAttributes([
                                    "name" => "controller",
                                    "value" => "authentication"
                                ]),
                            ],
                            [
                                "type" => "hidden",
                                "attributes" => $Builder->generateAttributes([
                                    "name" => "action",
                                    "value" => "login"
                                ])                                
                            ],  
                            [
                                "type" => "hidden",
                                "attributes" => $Builder->generateAttributes([
                                    "name" => "redirect_to",
                                    "value" => $_REQUEST['redirect_to']
                                ]),                                
                            ],                          
                        ]
                    ])?>                    
                </div>
            </div>
        </div>
    </div>
</div>
<div class="spacer100"></div>