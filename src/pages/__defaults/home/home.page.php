<div class="vh-100 slide-intro">    
    <div class="row h-100 justify-content-center align-items-center clearfix wrapper-img-home">
        <div class="col-6">
            <img src="<?=__FILE_ROOT__?>/src/assets/images/disegni_home.png" alt="Ubi major design" class="image-responsive-height first-image" />
        </div>
        <div class="col-6">
            <h1 class="second-image">
                <img src="<?=__FILE_ROOT__?>/src/assets/images/logo_ubi_white.png" alt="Ubi major logo" class="image-responsive-height"/><br>
                <span class="second-claim"><?=L::home_claim1?></span>
            </h1>
        </div>
        <div class="col-12 subtitle col text-center text-white">
            <h2 class="text-white"><?=L::home_claim2?></h2>
        </div>
    </div>
    
    <div class="mouse-wrapper">
        <div class="mouse">
            <div class="mouse-scroll"></div>
        </div>
    </div>
</div>
<div id="hugo-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4 offset-sm-1 text-center">
                <h2 class="m-b-20"><b>ALEX THOMSON RACING</b></h2>
                <p class="m-b-50">
                    <?=L::home_alex_thomson1?>
                </p>
                <p class="bold-ita">
                    <?=L::home_alex_thomson2?>
                </p>                
            </div>
        </div>
    </div>
</div>
<div id="who-we-are-section" class="text-white">
    <div class="container">
        <div class="row justify-content-end">
            <div class="col-12 col-md-6">
                <h2 class="text-red p-b-25">UBI MAIOR ITALIA&#174;</h2>
                <ul class="unstyled-list feature-list">
                    <li><?=L::home_about1?></li>
                    <li><?=L::home_about2?></li>
                    <li><?=L::home_about3?></li>
                    <li><?=L::home_about4?></li>
                </ul>                
                <div class="row pt-5">
                    <div class="col-sm-8">
                        <img src="<?=__FILE_ROOT__?>/src/assets/images/home-tech-small.png" class="img-responsive" alt="Ubi major craft">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!---------------- SEZIONE CATEGORIE NUOVA ------------------>
<div id="category-section" class="category-section">
    <div class="container-fluid">
        <div class="row text-center">            

            <?php 
            $lines = [
                [
                    'container_css' => 'col-xs-12 col-sm-6 col-lg-4',
                    'outer_css' => 'col-sm-12 featured-product padding-20',
                    'feature_css' => 'feature-flag feat-yacht',
                    'line_name' => 'YC Yatch Club',
                    'image' => __FILE_ROOT__."/src/assets/images/category-yc.png",
                    'image_alt' => 'Ubi Maior Italia - product category - Yatch Club',
                    'description' => L::home_yc,
                    'read_more_link' => '/lines/yatch-club',
                    'read_more_class' => 'yc-but',
                    'products_link' => 'index.php?p=catalog&idl=4',
                    'products_css' => 'btn btn-outline-light yc-but m-b-20 text-white'
                ],
                [
                    'container_css' => 'col-xs-12 col-sm-6 col-lg-4',
                    'outer_css' => 'col-sm-12 featured-product padding-20',
                    'feature_css' => 'feature-flag feat-regata',
                    'line_name' => 'RT Regata',
                    'image' => __FILE_ROOT__."/src/assets/images/category-rt.png",
                    'image_alt' => 'Ubi Maior Italia - product category - Regata',
                    'description' => L::home_rt,
                    'read_more_link' => '/lines/regata',
                    'read_more_class' => 'rt-but',
                    'products_link' => 'index.php?p=catalog&idl=3',
                    'products_css' => 'btn btn-outline-light rt-but m-b-20 text-white'
                ],
                [
                    'container_css' => 'col-xs-12 col-sm-6 col-lg-4',
                    'outer_css' => 'col-sm-12 featured-product padding-20',
                    'feature_css' => 'feature-flag feat-x3m',
                    'line_name' => 'X3M Flight',
                    'image' => __FILE_ROOT__."/src/assets/images/category-x3m.png",
                    'image_alt' => 'Ubi Maior Italia - product category - X3M',
                    'description' => L::home_x3m,
                    'read_more_link' => '/lines/x3m-flight',
                    'read_more_class' => 'x3m-but',
                    'products_link' => 'index.php?p=catalog&idl=2',
                    'products_css' => 'btn btn-outline-light x3m-but m-b-20 text-white'
                ],
                [
                    'container_css' => 'col-xs-12 col-sm-6 col-lg-4',
                    'outer_css' => 'col-sm-12 featured-product padding-20',
                    'feature_css' => 'feature-flag feat-jiber',
                    'line_name' => 'JB Jiber',
                    'image' => __FILE_ROOT__."/src/assets/images/category-jiber.png",
                    'image_alt' => 'Ubi Maior Italia - product category - Jiber',
                    'description' => L::home_jiber,
                    'read_more_link' => '/lines/jiber',
                    'read_more_class' => 'jib-but',
                    'products_link' => 'index.php?p=catalog&idl=8',
                    'products_css' => 'btn btn-outline-light jib-but m-b-20 text-white'
                ],
                [
                    'container_css' => 'col-xs-12 col-sm-6 col-lg-4',
                    'outer_css' => 'col-sm-12 featured-product padding-20',
                    'feature_css' => 'feature-flag feat-avvolgitori',
                    'line_name' => 'FR Avvolgitori',
                    'image' => __FILE_ROOT__."/src/assets/images/category-fr.png",
                    'image_alt' => 'Ubi Maior Italia - product category - FR Avvolgitori',
                    'description' => L::home_furlers,
                    'read_more_link' => '/lines/furlers',
                    'read_more_class' => 'fr-but',
                    'products_link' => 'index.php?p=catalog&idl=1',
                    'products_css' => 'btn btn-outline-light fr-but m-b-20 text-white'
                ],
                [
                    'container_css' => 'col-xs-12 col-sm-6 col-lg-4',
                    'outer_css' => 'col-sm-12 featured-product padding-20',
                    'feature_css' => 'feature-flag feat-accessories',
                    'line_name' => 'Accessories',
                    'image' => __FILE_ROOT__."/src/assets/images/category-accessories.png",
                    'image_alt' => 'Ubi Maior Italia - product category - Accessories',
                    'description' => L::home_accessories,
                    'read_more_link' => '/lines/accessories',
                    'read_more_class' => 'acc-but',
                    'products_link' => 'index.php?p=catalog&idl=5',
                    'products_css' => 'btn btn-outline-light acc-but m-b-20 text-white'
                ],
                [
                    'container_css' => 'col-xs-12 col-sm-6 col-lg-4',
                    'outer_css' => 'col-sm-12 featured-product padding-20',
                    'feature_css' => 'feature-flag feat-sailmakers',
                    'line_name' => 'Sailmakers',
                    'image' => __FILE_ROOT__."/src/assets/images/category-sailmakers.png",
                    'image_alt' => 'Ubi Maior Italia - product category - Sailmakers',
                    'description' => L::home_sailmakers,
                    'read_more_link' => '/lines/sailmakers',
                    'read_more_class' => 'sail-but',
                    'products_link' => 'index.php?p=catalog&idl=15',
                    'products_css' => 'btn btn-outline-light sail-but m-b-20 text-white'
                ],
                [
                    'container_css' => 'col-xs-12 col-sm-6 col-lg-4',
                    'outer_css' => 'col-sm-12 featured-product padding-20',
                    'feature_css' => 'feature-flag feat-alphalock',
                    'line_name' => 'Alphalock',
                    'image' => __FILE_ROOT__."/src/assets/images/category-alphalock.png",
                    'image_alt' => 'Ubi Maior Italia - product category - Alphalock',
                    'description' => L::home_alphalock,
                    'read_more_link' => '/lines/alphalock',
                    'read_more_class' => 'lck-but',
                    'products_link' => 'index.php?p=catalog&idl=16',
                    'products_css' => 'btn btn-outline-light lck-but m-b-20 text-white'
                ],
                [
                    'container_css' => 'col-xs-12 col-sm-6 col-lg-4',
                    'outer_css' => 'col-sm-12 featured-product padding-20',
                    'feature_css' => 'feature-flag feat-custom',
                    'line_name' => 'Custom',
                    'image' => __FILE_ROOT__."/src/assets/images/category-custom.png",
                    'image_alt' => 'Ubi Maior Italia - product category - Custom',
                    'description' => L::home_custom,
                    'read_more_link' => '/lines/custom',
                    'read_more_class' => 'cust-but',
                    'products_link' => null,
                    'products_css' => null
                ],
                [
                    'container_css' => 'col-xs-12 col-sm-6 bottom-category',
                    'outer_css' => 'col-sm-12 featured-product',
                    'feature_css' => 'feature-flag feat-apparel',
                    'line_name' => 'Apparel',
                    'image' => __FILE_ROOT__."/src/assets/images/category-apparel.png",
                    'image_alt' => 'Ubi Maior Italia - product category - Apparel',
                    'description' => L::home_apparel,
                    'read_more_link' => null,
                    'read_more_class' => null,
                    'products_link' => 'index.php?p=apparel',
                    'products_css' => 'btn btn-outline-light apparel-but m-b-20 text-white'
                ],
                [
                    'container_css' => 'col-xs-12 col-sm-6 bottom-category',
                    'outer_css' => 'col-sm-12 featured-product',
                    'feature_css' => 'feature-flag feat-brands',
                    'line_name' => L::menu_others,
                    'image' => __FILE_ROOT__."/src/assets/images/category-otherbrands.png",
                    'image_alt' => 'Ubi Maior Italia - product category - '.L::menu_others,
                    'description' => L::home_others,
                    'read_more_link' => '/lines/other-brands',
                    'read_more_class' => 'brands-color',
                    'products_link' => null,
                    'products_css' => null
                ],
            ];
            $Lines::generateLineHomeCards($lines,L::shared_read_more,L::shared_products);
            ?>

        </div>
    </div>
</div>

<div class="warranty text-center m-t-20 m-b-60">
    <a class="btn btn-outline-light jib-but" rel="group" href="pdf/WLG-<?=$_SESSION[$_ENV['KRO_COOKIE_PREFIX'].'lang']?>.pdf" target="_blank">
        <span class="fa fa-file-pdf"></span> &nbsp; <?=L::home_warranty?>
    </a>
</div>

<section>  
    <style>
        .item_box{
        height:500px;
    }
    
    .photo-thumb{
        width:90%;
        height:auto;
        border: thin solid #d1d1d1;
        margin:0 1em .5em 0;
        float:left; 
    }
    </style>
    <?php
    $tokens = $Db->select('ig_tokens','*');
    $instagram_token = NULL;
    if($tokens->num_rows>0){
        $item = $tokens->fetch_object();
        $instagram_token = $item->value;
        $instagram_expiring = $item->expiring;
        $datetime1 = strtotime($instagram_expiring);
        $datetime2 = strtotime(date('Y-m-d H:i:s'));
        $secs = $datetime1 - $datetime2;
        $days = $secs / 86400;
        if($days < 10){
            if($days > 0){
                $refresh_token_url = "https://graph.instagram.com/refresh_access_token?grant_type=ig_refresh_token&access_token={$instagram_token}";
                $refresh_json = file_get_contents($refresh_token_url);
                $refresh_obj = json_decode($refresh_json, true, 512, JSON_BIGINT_AS_STRING);
                print_r($refresh_obj['expires_in']);
                $instagram_token = $refresh_obj['access_token'];
                $instagram_expiring = date('Y-m-d H:i:s', strtotime("+{$refresh_obj['expires_in']} seconds", $datetime2));

                $res = $Db->update("ig_tokens","value='{$instagram_token}', expiring='{$instagram_expiring}'","id=1");
                if($res->status == 500)  throw new Exception($res->errno." - ".$res->error." - <br />".$res->query);
                if($res->status == 400)  throw new Exception($res->error);                
            }
            else{
                $instagram_token = NULL;
            }
        }
    }


    $obj = NULL;
    if($instagram_token){
        //$instagram_token="IGQVJWTGNzVi15RlNYNkY5UUJDVC1pZA1huZA3U4WEVPOFpMZAl9icGxIb1lWdDYyQ2xFdjBkcU1uMW1TR0tqZAWVWbE5uQ3c3VzNjQ0pwWGdsU3lKcng1UGxCTWZACTG9XZAGdlTFFLOS13"; //generato il 13/07/2020, durata 60 gg
        $user_id="17841401912849275";
        $fields="caption,id,media_type,media_url,permalink,thumbnail_url";
        $limit="12";
        $json_link="https://graph.instagram.com/{$user_id}/media/?";
        $json_link.="access_token={$instagram_token}&fields={$fields}&limit={$limit}";
        $json = file_get_contents($json_link);
        $obj = json_decode($json, true, 512, JSON_BIGINT_AS_STRING);
    }

    ?>    
    <div>
        <div class="col-xs-12 text-center m-b-15">            
            <h3>
                <a class="text-fff" href="https://www.instagram.com/ubimaioritalia/" target="_blank">
                    <?=L::home_follow_us?> @ubimaioritalia
                </a>
            </h3>
        </div>
    </div>
    <div class="row">
        <?php
        if($obj){
            foreach($obj["data"] as $key => $value){
                ?>
                <div class="col-6 col-sm-4 col-md-4 col-lg-3 mb-4">
                    <a href="<?=$value["permalink"]?>" target="_blank">
                        <img src="<?=$value["thumbnail_url"] ? $value["thumbnail_url"] : $value["media_url"] ?>" alt="Instafeed - UbiMaior" class="img-responsive" style=" width: 90%;margin-left: 5%;margin-top: 5%;">
                    </a>
                </div>
                <?php
            }
        }
        ?>
    </div>
</section>

<div class="spacer10"></div>

<section id="comments-section">
    <div class="quotes-container">        
        <div class="row justify-content-center">
            <div class="col">
                <div class="carousel slide" data-ride="carousel" id="quote-carousel">        
                    <!-- Carousel Slides / Quotes -->
                    <div class="carousel-inner row">
                        <!-- Quote 1 -->
                        <div class="carousel-item active">
                            <div class="row justify-content-center">
                                <div class="col-12 col-sm-5" style="padding:0 60px !important;">
                                    <blockquote  class="blockquote">
                                        <p>UBI MAIOR ITALIA® helped us design and machine every single detail onboard trying, maniacally, to save every gram.</p>
                                        <footer class="blockquote-footer">ANTOINE MERMOD, Project manager IMOCA60</footer>
                                    </blockquote>
                                    </div>
                                    <div class="col-12 col-sm-5" style="padding:0 60px !important;">
                                    <blockquote  class="blockquote">
                                        <p>Using UBI MAIOR ITALIA®’s products allows me to find the right solutions for my projects. An optimized deck plan with their products truly makes a difference when racing.</p>
                                        <footer class="blockquote-footer">MATTEO POLLI, Yacht designer and Project manager</footer>
                                    </blockquote>
                                </div>
                            </div>
                        </div>
                        <!-- Quote 2 -->
                        <div class="carousel-item">
                            <div class="row justify-content-center">
                                <div class="col-12 col-sm-5" style="padding:0 60px !important;">
                                    <blockquote  class="blockquote">
                                        <p>The high technical level combined with the maximum guaranteed quality make UBI MAIOR ITALIA® an important partner with us.</p>
                                        <footer class="blockquote-footer">VALENTINA GANDINI, Mylius Yacht</footer>
                                    </blockquote>
                                </div>
                                <div class="col-12 col-sm-5" style="padding:0 60px !important;">
                                    <blockquote  class="blockquote">
                                        <p>Our yard is very focused on aesthetic details that can make a boat unique and UBI MAIOR ITALIA® did this for us.</p>
                                        <footer class="blockquote-footer">LAURA CAPPELLETTI, Vismara Marine</footer>
                                    </blockquote>
                                </div>
                            </div>
                        </div>
                        <!-- Quote 3 -->
                        <div class="carousel-item">
                            <div class="row justify-content-center">
                                <div class="col-12 col-sm-5" style="padding:0 60px !important;">
                                    <blockquote  class="blockquote">
                                        <p>We chose UBI MAIOR ITALIA® for the high quality of their products as well as their flexibility in creating custom parts.</p>
                                        <footer class="blockquote-footer">MAX SIRENA, Emirates Team New Zealand</footer>
                                    </blockquote>
                                </div>
                                <div class="col-12 col-sm-5" style="padding:0 60px !important;">
                                    <blockquote  class="blockquote">
                                        <p>Lighter is faster, but to finish first, first you have to finish based on this, I choose UBI MAIOR ITALIA® as they offer the best relationship between light and reliable.</p>
                                        <footer class="blockquote-footer">NACHO POSTIGO, Pro sailor and Mini Transat Skipper</footer>
                                    </blockquote>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>   
            </div>                
        </div>
    </div>
</section>

<div class="spacer50"></div>

<div id="carousel-section">
  <div class="image-carousel">
    <img src="<?=__FILE_ROOT__?>/src/assets/images/home-gal-0.jpg" alt="UbiMaior Italia - Emirates New Zealand Team">
    <img src="<?=__FILE_ROOT__?>/src/assets/images/home-gal-0.2.jpg" alt="UbiMaior Italia - Emirates New Zealand Team">
    <img src="<?=__FILE_ROOT__?>/src/assets/images/home-gal-1.jpg" alt="UbiMaior Italia - Emirates New Zealand Team">
    <img src="<?=__FILE_ROOT__?>/src/assets/images/home-gal-4.jpg" alt="UbiMaior Italia - SAIL BLOCKS – DECK HARDWARE – CUSTOM PROJECTS">
    <img src="<?=__FILE_ROOT__?>/src/assets/images/home-gal-5.jpg" alt="UbiMaior Italia - SAIL BLOCKS – DECK HARDWARE – CUSTOM PROJECTS">
  </div>
</div>
