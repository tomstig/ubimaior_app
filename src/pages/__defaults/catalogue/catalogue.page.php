<?php 
if($_REQUEST[$_ENV['KRO_COOKIE_PREFIX'].'catalogue_sort']) $_SESSION[$_ENV['KRO_COOKIE_PREFIX'].'catalogue_sort'] = $_REQUEST[$_ENV['KRO_COOKIE_PREFIX'].'catalogue_sort'];
?>
<div class="row mt-4 mx-0">
    <div class="col-sm-3">
        <div class="sticky p-0 bg-white shadow-sm border">
            <div class="text-light bg-dark d-flex justify-content-between align-items-center px-2 py-3">
                <h4 class="p-0 m-0 text-uppercase"><?=L::catalogue_filter?></h4>
            </div>        
            <div class="text-dark py-3 px-2 mt-2">
                <h6 class="p-0 m-0 text-uppercase"><?=L::catalogue_product_category?></h6>
                <div class="d-flex justify-content-between align-items-center mt-4">
                    <p class="m-0">Category 1</p>
                    <div class="d-flex align-items-center">
                        <span class="d-inline text-muted mr-1">1</span>
                        <input class="d-inline" type="checkbox" id="blankCheckbox" value="option1" aria-label="...">
                    </div>                
                </div>
            </div>
        </div>        
    </div>
    <div class="col-sm-9 pr-0">
        <?php $catalogue = $Db->select('products p','p.id, p.name, p.description, p.image, pp.price, pp.vat, c.symbol','p.active=1','name ASC',null,'LEFT JOIN '.$Db->getTablePrefix().'products_prices pp ON pp.id_product = p.id LEFT JOIN '.$Db->getTablePrefix().'currencies c ON c.id = pp.id_currency'); ?>
        <div class="d-flex justify-content-between align-items-center mb-4">
            
        <p class="m-0"><?=$catalogue->num_rows?> <?=$catalogue->num_rows===1 ? 'result' : 'results'?></p>
            
            <?php
            $current_sort = L::catalogue_sortby_prefix;
            $sorts = [
                [ "link" => "?{$_ENV['KRO_COOKIE_PREFIX']}catalogue_sort=name", "key" => "name", "label" => L::catalogue_sortby_name],
                [ "link" => "?{$_ENV['KRO_COOKIE_PREFIX']}catalogue_sort=price_hl", "key" => "price_hl", "label" => L::catalogue_sortby_price_hl],
                [ "link" => "?{$_ENV['KRO_COOKIE_PREFIX']}catalogue_sort=price_lh", "key" => "price_lh", "label" => L::catalogue_sortby_price_lh],
                [ "link" => "?{$_ENV['KRO_COOKIE_PREFIX']}catalogue_sort=newest", "key" => "newest", "label" => L::catalogue_sortby_newest],
            ];
            foreach($sorts as $sort){
                if($sort['key'] === $_SESSION[$_ENV['KRO_COOKIE_PREFIX'].'catalogue_sort']) $current_sort .= $sort['label'];                    
                $sort_options[] = array(
                    'link' => $sort['link'],
                    'label' => $sort['label']
                );
            }                                
            $Builder::generateDropdown('catalogue_sortby',$current_sort,$sort_options,"p-2 shadow-sm border");
            ?>
            
        </div>
        <div class="card-columns">
            <?php            
            while($item = $catalogue->fetch_object()){
                ?>
                <div class="card shadow-sm" id="product">
                    <img src="<?=__FILE_ROOT__."/{$item->image}"?>" class="card-img-top" alt="demo product">
                    <div class="card-body">
                        <h5 class="card-title font-weight-light"><?=$item->name?></h5>
                        <h6 class="card-subtitle mb-2 text-muted font-weight-light"><?=$item->description?></h6>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item pl-0 font-weight-light"><?="{$item->symbol} {$item->price}"?></li>                
                        </ul>
                        <div class="card-footer bg-transparent pb-0 d-flex justify-content-between align-items-center">
                            <a href="<?=$Router->getBasePath()?>catalogue/product/<?=$item->id?>" class="card-link text-decoration-none text-danger">
                                <small>View</small>
                            </a>
                            <a href="#" class="card-link text-decoration-none text-danger">
                                <small>Add to Wishlist</small>
                            </a>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>                
        </div>

    </div>
</div>