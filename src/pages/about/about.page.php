<div id="who-page" class="clearfix container-fluid">
    <div class="row">
        <div class="who-intro col-12 clearfix">
            <div class="row">
                <div class="col-12 col-sm-5 who-intro-left"></div>
                <div class="col-12 col-sm-7 who-intro-right">
                    <h2 class="text-red">UBI MAIOR ITALIA&#174;</h2>
                    <h3 class="text-grey">SAIL BLOCKS – DECK HARDWARE – CUSTOM PROJECTS</h3> 
                    <h4 class="text-white">
                        <?=L::about_intro?> 
                    </h4>
                </div>
            </div>
        </div>

        <div class="who-who col-12 xs-m-t-20 m-t-50 m-b-50 clearfix">
            <div class="row">
                <div class="col-12 col-sm-10 col-sm-offset-1 col-lg-6 col-lg-offset-5">
                    <h3 class="m-b-30 text-grey"><?=L::about_text1_title?></h3>
                    <p class="m-b-30 text-white">
                        <?=L::about_text1?>
                    </p>
                </div>
            </div>
        </div>

        <div class="who-quote col-12 clearfix">
            <div class="row">
                <div class="col-sm-5 quote-text">
                    <h4 class="m-b-50 text-white">
                        <?=L::about_text2?>
                    </h4>
                </div>
                <ul class="col-sm-6 col-sm-offset-1 nine-steps">
                    <a class="fancybox" rel="group" href="images/chisiamo-1.jpg">
                        <li><img src="<?=__FILE_ROOT__?>/src/assets/images/chisiamo-1.jpg" width="100%" alt=""></li>
                    </a>
                    <a class="fancybox" rel="group" href="images/chisiamo-2.jpg">
                        <li><img src="<?=__FILE_ROOT__?>/src/assets/images/chisiamo-2.jpg" width="100%" alt=""></li>
                    </a>
                    <a class="fancybox" rel="group" href="images/chisiamo-3.jpg">
                        <li><img src="<?=__FILE_ROOT__?>/src/assets/images/chisiamo-3.jpg" width="100%" alt=""></li>
                    </a>
                    <a class="fancybox" rel="group" href="images/chisiamo-4.jpg">
                        <li><img src="<?=__FILE_ROOT__?>/src/assets/images/chisiamo-4.jpg" width="100%" alt=""></li>
                    </a>
                    <a class="fancybox" rel="group" href="images/chisiamo-5.jpg">
                        <li><img src="<?=__FILE_ROOT__?>/src/assets/images/chisiamo-5.jpg" width="100%" alt=""></li>
                    </a>
                    <a class="fancybox" rel="group" href="images/chisiamo-6.jpg">
                        <li><img src="<?=__FILE_ROOT__?>/src/assets/images/chisiamo-6.jpg" width="100%" alt=""></li>
                    </a>
                    <a class="fancybox" rel="group" href="images/chisiamo-7.jpg">
                        <li><img src="<?=__FILE_ROOT__?>/src/assets/images/chisiamo-7.jpg" width="100%" alt=""></li>
                    </a>
                    <a class="fancybox" rel="group" href="images/chisiamo-8.jpg">
                        <li><img src="<?=__FILE_ROOT__?>/src/assets/images/chisiamo-8.jpg" width="100%" alt=""></li>
                    </a>
                    <a class="fancybox" rel="group" href="images/chisiamo-9.jpg">
                        <li><img src="<?=__FILE_ROOT__?>/src/assets/images/chisiamo-9.jpg" width="100%" alt=""></li>
                    </a>   
                </ul>
            </div>
        </div>

        <div class="who-people col-12 clearfix text-center">
            <div class="row who-content">
                <h3 class="m-b-30 text-grey"><?=L::about_team_title?></h3>
                <!-- Line one -->
                <div class="col-12 col-sm-4 col-md-4 col-lg-3 people">
                    <img src="<?=__FILE_ROOT__?>/src/assets/images/gianni_dini.jpg" width="100%" >
                    <div class="people-text">
                        <h5>GIANNI DINI</h5>
                        <p>
                            CEO - Product Manager<br>
                            +39 0558364421<br><br>
                            <span class="people-email">gianni.dini@ubimaioritalia.com</span>
                        </p>
                    </div>
                </div>
                <div class="col-12 col-sm-4 col-md-4 col-lg-3 people">
                    <img src="<?=__FILE_ROOT__?>/src/assets/images/raffaele_dirusso.jpg" width="100%" >
                    <div class="people-text">
                        <h5>RAFFAELE DI RUSSO</h5>
                        <p>
                            Corporate Brand Manager <br>
                            +39 0558364421<br><br>
                            <span class="people-email">raffaele.dirusso@ubimaioritalia.com</span>
                        </p>
                    </div>
                </div>
                <div class="col-12 col-sm-4 col-md-4 col-lg-3 people">
                    <img src="<?=__FILE_ROOT__?>/src/assets/images/vieri_abolaffio.jpg" width="100%" >
                    <div class="people-text">
                        <h5>VIERI ABOLAFFIO</h5>
                        <p>
                            Project Manager <br>
                            +39 0558364421<br><br>
                            <span class="people-email">ufficiotecnico@ubimaioritalia.com</span>
                        </p>
                    </div>
                </div>
                <div class="col-12 col-sm-4 col-md-4 col-lg-3 people">
                    <img src="<?=__FILE_ROOT__?>/src/assets/images/matthieu_delaporte.jpg" width="100%" >
                    <div class="people-text">
                        <h5>MATTHIEU DELAPORTE</h5>
                        <p>
                            Technical Office <br>
                            +39 0558364421<br><br>
                            <span class="people-email">matthieu.delaporte@ubimaioritalia.com</span>
                        </p>
                    </div>
                </div>
                <!-- / Line one -->
                <div class="spacer10"></div> 
                <!-- Line two -->
                <div class="col-12 col-sm-4 col-md-4 col-lg-3 people">
                    <img src="<?=__FILE_ROOT__?>/src/assets/images/fabio_desimoni.jpg" width="100%" >
                    <div class="people-text">
                        <h5>FABIO DE SIMONI</h5>
                        <p>
                            Sales Manager <br>
                            +39 340 7783482<br><br>
                            <span class="people-email">fabio.desimoni@ubimaioritalia.com</span>
                        </p>
                    </div>
                </div>
                <div class="col-12 col-sm-4 col-md-4 col-lg-3 people">
                    <img src="<?=__FILE_ROOT__?>/src/assets/images/nicola_stedile.jpg" width="100%" >
                    <div class="people-text">
                        <h5>NICOLA STEDILE</h5>
                        <p>
                            Sales Italy, Adriatic Area <br>
                            +39 366 719 6775<br><br>
                            <span class="people-email">nicola.stedile@ubimaioritalia.com</span>
                        </p>
                    </div>
                </div>
                <div class="col-12 col-sm-4 col-md-4 col-lg-3 people">
                    <img src="<?=__FILE_ROOT__?>/src/assets/images/edoardo_celli.jpg" width="100%" >
                    <div class="people-text">
                        <h5>EDOARDO CELLI</h5>
                        <p>
                            Sales/Purchase Office  <br>
                            +39 0558364421 <br> <br>
                            <span class="people-email">amministrazione@ubimaioritalia.com</span>
                        </p>
                    </div>
                </div>
                <!-- / Line two -->
                <div class="spacer10"></div> 
                <!-- Line three -->
                <div class="col-12 col-sm-4 col-md-4 col-lg-3 people">
                    <img src="<?=__FILE_ROOT__?>/src/assets/images/patrizio_solli.jpg" width="100%" >
                    <div class="people-text">
                        <h5>PATRIZIO SOLLI</h5>
                        <p>
                            Delivery-Warehouse  <br>
                            +39 0558364421 <br> <br>
                            <span class="people-email">magazzino@ubimaioritalia.com</span>
                        </p>
                    </div>
                </div>
                <div class="col-12 col-sm-4 col-md-4 col-lg-3 people">
                    <img src="<?=__FILE_ROOT__?>/src/assets/images/andrea_focardi.jpg" width="100%" >
                    <div class="people-text">
                        <h5>ANDREA FOCARDI</h5>
                        <p>
                            Delivery-Warehouse  <br>
                            +39 0558364421 <br> <br>
                            <span class="people-email">magazzino@ubimaioritalia.com</span>
                        </p>
                    </div>
                </div>
                <div class="col-12 col-sm-4 col-md-4 col-lg-3 people">
                    <img src="<?=__FILE_ROOT__?>/src/assets/images/silvia_sestini.jpg" width="100%" >
                    <div class="people-text">
                        <h5>SILVIA SESTINI</h5>
                        <p>
                            <?=L::about_team_admin?><br>
                            +39 0558364421 <br> <br>
                            <span class="people-email">amministrazione@ubimaioritalia.com</span>
                        </p>
                    </div>
                </div>
                <div class="col-12 col-sm-4 col-md-4 col-lg-3 people">
                    <img src="<?=__FILE_ROOT__?>/src/assets/images/cinzia_carotti.jpg" width="100%" >
                    <div class="people-text">
                        <h5>CINZIA CAROTTI</h5>
                        <p>
                            <?=L::about_team_admin?><br>
                            +39 0558364421 <br> <br>
                            <span class="people-email">amministrazione@ubimaioritalia.com</span>
                        </p>
                    </div>
                </div>
                <!-- / Line three -->
            </div>
        </div>
        
        <div class="who-all col-12 m-t-50 clearfix"></div>
    
    </div>
</div>
