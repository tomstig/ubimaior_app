<form action="<?=\Karoo\Core\Configuration::getWsUrl()?>" method="POST" class="item-form needs-validation" enctype='multipart/form-data' novalidate>
    <div class="row">
        <!-- Left Side --> 
        <div class="col-md-12 col-lg-8">
            <div class="card shadow-sm mb-3">        
                <div class="card-body">
                    <h5 class="card-title">Primary Details</h5>
                    <div class="form-row">
                        <div class="col">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" name="name" class="form-control" id="name" value="<?=$product->name?>" required />
                            </div>
                        </div>                        
                    </div>
                    <div class="form-row">
                        <div class="col">
                            <div class="form-group">
                                <label for="description">Description (optional)</label>       
                                <div data-toggle="rte">
                                    <?=$product->description?>
                                </div>
                                <textarea class="d-none form-control" data-target="rte" name="description" id="description"></textarea>
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="card shadow-sm mb-3">        
                <div class="card-body">
                    <h5 class="card-title">Associations</h5>
                    <div class="form-row">
                        <div class="col">
                            <div class="form-group">
                                <p>Categories</p>
                                <?php 
                                $categories_options = (object) [
                                    "label" => "Search Category",
                                    "help_tooltip" => (object) [
                                        "placement" => "top",
                                        "text" => "To select one, click or press enter"
                                    ],
                                    "id" => "category-autocomplete",
                                    "entity" => "categories",
                                    "fields" => (object) [
                                        "id" => (object) [ "default" => 0 ],
                                        "category" => (object) [ "default" => 1 ],
                                        "image" => (object) [ "default" => 0 ]
                                    ],
                                    "search_by" => "category",
                                    "order_by" => "category ASC",
                                    "layout" => (object) [
                                        "image" => 1,
                                        "icon" => 0
                                    ],
                                    "input" => (object) [
                                        "name" => "categories",
                                        "placeholder" => "start typing a keyword",
                                        "invalid_feedback_message" => "Invalid category provided"
                                    ],
                                    // "values" => [
                                    //     ["label" => "Category 1", "value" => 1],
                                    //     ["label" => "Category 2", "value" => 2]
                                    // ]
                                ];
                                $Builder->generateAutocomplete($categories_options);
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col">
                            <div class="form-group">
                                <label for="title">Tags (optional)</label>                                    
                                <?php 
                                $tags_options = (object) [
                                    "label" => "Search Tags",
                                    "help_tooltip" => (object) [
                                        "placement" => "top",
                                        "text" => "To select one, click or press enter"
                                    ],
                                    "id" => "tags-autocomplete",
                                    "entity" => "categories",
                                    "fields" => (object) [
                                        "id" => (object) [ "default" => 0 ],
                                        "category" => (object) [ "default" => 1 ],
                                        "image" => (object) [ "default" => 0 ]
                                    ],
                                    "search_by" => "category",
                                    "order_by" => "category ASC",
                                    "layout" => (object) [
                                        "image" => 1,
                                        "icon" => 0
                                    ],
                                    "input" => (object) [
                                        "name" => "tags",
                                        "placeholder" => "start typing a keyword",
                                        "invalid_feedback_message" => "Invalid tags provided"
                                    ]                                    
                                ];
                                $Builder->generateAutocomplete($tags_options);
                                ?>                                   
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <!-- Right Side --> 
        <div class="col-md-12 col-lg-4">                

            <div class="card shadow-sm mb-3">        
                <div class="card-body">
                    <h5 class="card-title">Visibility</h5>
                    <p class="text-muted">Set up product visibility for the customers.</p>                        
                    <div class="d-flex justify-content-between align-items-center py-3 rounded">
                        <div class="text-success">                                
                            <div class="p-2 mr-2 d-inline-flex justify-content-center align-items-center bg-success text-white rounded">
                                <i class="far fa-eye"></i>
                            </div>
                            <b>Visible</b>
                        </div>
                        <div class="table-switch text-left">
                            <input id="table-switch-<?=$product->id?>" name="table-switch" type="checkbox" <?=$product->active ? 'checked' : ''?>/>
                            <label for="table-switch-<?=$product->id?>"></label>
                        </div>
                    </div>                    
                </div>
            </div>
            
            <?php if(isset($product)): ?>
            <div class="card shadow-sm mb-3">        
                <div class="card-body">
                    <h5 class="card-title">Preview</h5>
                    <p class="text-muted">See preview of the product. In the same way, users will see a product in the catalogue.</p>
                    <div class="text-center">
                        <a href="#" class="btn btn-outline-primary">See Preview</a>
                    </div>                        
                </div>
            </div>
            <?php endif; ?>

            <div class="card shadow-sm mb-3">        
                <div class="card-body">
                    <h5 class="card-title">Primary Image</h5>
                    <?php if($product_id && $product->image): ?>
                    <div class="border p-2 d-flex justify-content-flex-start align-items-center">
                        <img class="border" src="<?=__FILE_ROOT__."/{$product->image}"?>" height="100" alt="product" />
                        <div class="ml-3">
                            <p class="text-secondary text-break">Path: <small><?=$product->image?></small></p>
                            <p class="text-black-50">Size: <?=round(filesize($product->image) / 1024, 2)?> kb</p>
                        </div>
                    </div>
                    <div class="spacer10"></div>                        
                    <?php endif; ?>
                    <div class="custom-file">
                        <input type="file" name="image" class="item-image custom-file-input" id="customFile" accept=".png, .jpg, .jpeg"  <?=!$product ? 'required' : ''?>>
                        <label class="custom-file-label" for="customFile">
                            <?=$product_id ? 'Change image' : 'Choose image' ?>
                        </label>
                        <div class="item-image-preview"></div>
                    </div>
                </div>
            </div>
            
            
            <div class="card shadow-sm mb-3">        
                <div class="card-body">
                    <h5 class="card-title">Product Price</h5>
                    <p class="text-muted">Here you can set the main product price for your product</p>
                    <div class="form-row">
                        <div class="col">
                            <div class="form-group">
                                <label for="name">Specify Product Price</label>
                                <input type="text" name="price" class="form-control" id="price" placeholder="e.g. 100,00" required />
                            </div>
                        </div>                        
                    </div>
                    <?php                    
                    if($product_id && ($Db->select('products_sizes','id','id_product='.$product_id)->num_rows>0 || $Db->select('products_colors','id','id_product='.$product_id)->num_rows>0)){
                    ?>
                        <div class="spacer20"></div>
                        <p class="text-muted">You can manage your product prices into the prices tab</p>
                        <div class="text-center">
                            <a href="#" class="btn btn-outline-primary">Manage prices by variations</a>
                        </div>
                    <?php
                    }
                    ?>
                </div>
            </div>

        </div>
    </div>
    <input type="hidden" name="controlloer" value="products" />
    <input type="hidden" name="action" value="<?=$product ? 'edit' : 'create'?>" />
    <input type="hidden" name="id" value="<?=$product ? $product->id : null ?>" />
    <input type="hidden" name="tab" value="info" />            
    <div class="spacer20"></div>
    <div class="text-center">
        <a class="btn btn-link" href="<?="{$Router->getBasePath()}admin/products"?>">
            Go Back
        </a>
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>        
</form>