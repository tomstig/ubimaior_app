<?php
if(isset($_REQUEST['pid'])){
    $properties = $__db->get('products_properties','*','id='.$_REQUEST['pid']);
    $property = $properties->fetch_object();
}
?>
<form action="<?=\Karoo\Core\Configuration::getWsUrl()?>" method="POST" class="needs-validation"  enctype='multipart/form-data' novalidate>    
    <div class="card shadow-sm mb-3">        
        <div class="card-body">
            <h6 class="card-title"><?=isset($_REQUEST['pid']) ? 'Edit' : 'Add' ?> Property</h6>                    
            <div class="form-row">
                <div class="col">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" class="form-control" id="name" placeholder="e.g. brand" value="<?=$property->name?>" required />
                    </div>
                </div>                        
            </div>
            <div class="form-row">
                <div class="col">
                    <div class="form-group">
                        <label for="value">Value</label>
                        <input type="text" name="value" class="form-control" id="value" placeholder="e.g. Karoo" value="<?=$property->value?>" required />
                    </div>
                </div>                        
            </div>
            <div class="form-row">
                <div class="col">
                    <div class="form-group">
                        <label for="name">Status</label>
                        <div class="table-switch text-left">
                            <input id="table-switch" name="table-switch" type="checkbox" <?=(!isset($property) || $property->active) ? 'checked' : ''?>/>
                            <label for="table-switch"></label>
                        </div>
                    </div>
                </div>                        
            </div>            
            <input type="hidden" name="a" value="create-property" />            
            <div class="form-row">
                <div class="col text-center mt-3">                        
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>       
        </div>
    </div>
</form>