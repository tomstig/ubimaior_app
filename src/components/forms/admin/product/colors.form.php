<?php
if(isset($_REQUEST['cid'])){
    $colors = $Db->select('products_colors','*','id='.$_REQUEST['cid']);
    $color = $colors->fetch_object();
}
?>
<form action="<?=\Karoo\Core\Configuration::getWsUrl()?>" method="POST" class="needs-validation"  enctype='multipart/form-data' novalidate>    
    <div class="card shadow-sm mb-3">        
        <div class="card-body">
            <h5 class="card-title"><?=isset($_REQUEST['cid']) ? 'Edit' : 'Add' ?> Color</h5>                    
            <div class="form-row">
                <div class="col">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" class="form-control" id="name" required />
                    </div>
                </div>                        
            </div>
            <div class="form-row">
                <div class="col">
                    <div class="form-group">
                        <label for="code">Code</label>
                        <input type="text" name="code" class="form-control" id="code" required />
                    </div>
                </div>                        
            </div>
            <div class="form-row">
                <div class="col">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input color-type" type="radio" name="color-type" id="color-type-pick" value="pick-color" checked="checked" required>
                        <label class="form-check-label" for="color-type-pick">Pick Color</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input color-type" type="radio" name="color-type" id="color-type-image" value="image-color" required>
                        <label class="form-check-label" for="color-type-image">Upload Image</label>
                    </div>
                </div>
                <div class="spacer20"></div>
            </div>
            <div class="form-row pick-color-container">
                <div class="col">
                    <div class="form-group">
                        <label for="color">Color</label>
                        <div class="input-group mb-3 pick-color-input-container">
                            <input type="text" class="form-control picked-color" placeholder="e.g. #ffffff " required />
                            <div class="input-group-append">
                                <input type="text" name="color" class="pick-color input-group-text"/>
                            </div>
                        </div>
                    </div>
                </div>                        
            </div>
            <div class="form-row color-image-container d-none">
                <div class="col">
                    <div class="custom-file color-image-input-container">                        
                        <input type="file" name="img_name" class="custom-file-input" id="form-color-image" accept=".jpg" />
                        <label class="custom-file-label" for="form-color-image">Choose JPG image</label>
                    </div>                    
                </div>
            </div>
            <input type="hidden" name="a" value="create-color" />            
            <div class="form-row">
                <div class="col text-center mt-3">                        
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>       
        </div>
    </div>
</form>