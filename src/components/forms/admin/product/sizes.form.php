<?php
if(isset($_REQUEST['cid'])){
    $colors = $__db->get('products_colors','*','id='.$_REQUEST['cid']);
    $color = $colors->fetch_object();
}
?>
<form action="<?=\Karoo\Core\Configuration::getWsUrl()?>" method="POST" class="needs-validation"  enctype='multipart/form-data' novalidate>    
    <div class="card shadow-sm mb-3">        
        <div class="card-body">
            <h6 class="card-title"><?=isset($_REQUEST['cid']) ? 'Edit' : 'Add' ?> Size</h6>                    
            <div class="form-row">
                <div class="col">
                    <div class="form-group">
                        <label for="name">Size</label>
                        <input type="text" name="name" class="form-control" id="name" placeholder="e.g. 20cm, XS, EU 42" required />
                    </div>
                </div>                        
            </div>
            <div class="form-row">
                <div class="col">
                    <div class="form-group">
                        <label for="name">Status</label>
                        <div class="table-switch text-left">
                            <input id="table-switch" name="table-switch" type="checkbox" <?=(!isset($color) || $color->active) ? 'checked' : ''?>/>
                            <label for="table-switch"></label>
                        </div>
                    </div>
                </div>                        
            </div>            
            <input type="hidden" name="a" value="create-size" />            
            <div class="form-row">
                <div class="col text-center mt-3">                        
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>       
        </div>
    </div>
</form>