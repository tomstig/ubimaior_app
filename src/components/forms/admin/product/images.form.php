<?php
$colors = $Db->select('products_colors','*',null,'color ASC');
?>
<form action="<?=\Karoo\Core\Configuration::getWsUrl()?>" method='POST' class="product-images-form needs-validation" enctype='multipart/form-data' novalidate>
    <div class="card shadow-sm mb-3">        
        <div class="card-body">
            <h5 class="card-title">Add new images</h5>                    
            <div class="form-row align-items-center">
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="id_color">Browse Image:</label>
                        <div class="custom-file">
                            <input type="file" name="file[]" class="custom-file-input product-images-input" id="uploadImages" accept=".png,.jpg,.jpeg" required multiple>
                            <label class="custom-file-label" for="uploadImages">Browse Images</label>
                            <div class="product-images-preview"></div>
                        </div>
                    </div>
                </div>

                <?php if($colors->num_rows>0): ?>
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="id_color">Related to Color (optional):</label>
                        <select name="id_color" class="form-control">
                            <option disabled selected value="">-- Pick Color --</option>
                        </select>
                    </div>
                </div>                
                <?php endif; ?>

                <input type="hidden" name="a" value="create-image" />
                <input type="hidden" name="iid" class="pid" value="<?=$product_id?>" />
                <input type="hidden" name="tab" class="tab-cb" value="images" />                    
                <div class="col-md-2 text-center">
                    <button type="submit" class="btn btn-primary mt-3">Submit</button>
                </div>
            </div>
        </div>
    </div>
</form>