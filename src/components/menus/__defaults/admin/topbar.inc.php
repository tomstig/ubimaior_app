<!-- <div class="input-group w-25">
    <input type="text" class="form-control" placeholder="search product by keyword" aria-label="search product by keyword" aria-describedby="search-product-addon">
    <div class="input-group-append">
        <span class="input-group-text" id="search-product-addon">
            <i class="fas fa-search"></i>
        </span>                
    </div>
</div> -->
<div class="nav justify-content-end">  
    <li class="nav-item">
        <a class="nav-link" href="javascript:void(0)">
            <i class="fas fa-bars text-white" data-toggle="drawer"></i>
        </a>
    </li>          
    <?php                             
    $user = \Karoo\Authentication\Auth::getUser();
    $user_menu[] = array("link" => \Karoo\Core\Configuration::getBaseUrl(), "label" => L::menu_back_to.' '.L::menu_catalogue, "active" => $page->slug==='admin' ? 'text-danger border-bottom border-danger' : '');        
    $user_menu[] = array("link" => \Karoo\Core\Configuration::getBaseUrl().'/profile', "label" => L::menu_profile, "active" => $page->slug==='profile' ? 'text-danger border-bottom border-danger' : '');            
    $user_menu[] = array("link" => \Karoo\Core\Configuration::getBaseUrl().'/logout', "label" => "Logout", "active" => '');
    $Builder::generateDropdown('user_menu',"{$user->name} {$user->surname} <i class='fas fa-user-circle user-menu-icon'></i>",$user_menu, null, 'text-decoration-none');                            
    ?>
</div>    