<?php
$menu_voices = [
    ['link' => \Karoo\Core\Configuration::getBaseUrl().'/admin/', 'icon' => 'fa-tachometer-alt', 'label' => 'Dashboard', 'active' => $current_page==='dashboard' ? 1 : 0],
    ['link' => \Karoo\Core\Configuration::getBaseUrl().'/admin/categories/', 'icon' => 'fa-grip-horizontal', 'label' => 'Categories', 'active' => $current_page==='categories' ? 1 : 0, 'actions' => ['' => '<i class="fas fa-th-list mr-2"></i> List', 'add/' => '<i class="fas fa-plus-circle mr-2"></i> Add new']],
    ['link' => \Karoo\Core\Configuration::getBaseUrl().'/admin/products/', 'icon' => 'fa-store', 'label' => 'Products', 'active' => $current_page==='products' ? 1 : 0, 'actions' => ['' => '<i class="fas fa-th-list mr-2"></i> List', 'add/' => '<i class="fas fa-plus-circle mr-2"></i> Add new']],
    ['link' => \Karoo\Core\Configuration::getBaseUrl().'/admin/sendbox/', 'icon' => 'fa-inbox', 'label' => 'Sendbox', 'active' => $current_page==='sendbox' ? 1 : 0],
    ['link' => \Karoo\Core\Configuration::getBaseUrl().'/admin/settings/', 'icon' => 'fa-tools', 'label' => 'Settings', 'active' => $current_page==='settings' ? 1 : 0],
]
?>
<div class="mt-4 w-100 p-4 h-100">
    <?php
    $Builder->generateListMenu($menu_voices,'menu');
    ?>     
</div>