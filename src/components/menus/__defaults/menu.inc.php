<nav class="fixed-top shadow-sm p-3">
    <div class="container">
        <div class="d-flex justify-content-between align-items-center">
            <a class="navbar-brand text-dark text-decoration-none" href="<?=$Router->getBasePath()?>">
                <img class="logo" src="<?=__FILE_ROOT__?>/src/assets/images/logo_ubi_white.png" alt="Ubi Major" width="200">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="nav justify-content-end">
                
                <a class="mx-4 text-white text-decoration-none" href="javascript:void(0)"><i class="fa fa-search"></i></a>
                <a class="mx-4 text-white text-decoration-none" href="/wishlist"><i class="fa fa-shopping-cart"></i></a>
                <a class="mx-4 text-white text-decoration-none menu" href="javascript:void(0)"><i class="fa fa-bars"></i></a>

                <?php
                // $current_language = L::menu_languages;
                // $languages = $Db->select('languages','*','active=1','language ASC');
                // while($language = $languages->fetch_object()){
                //     if($language->code === $_SESSION[$_ENV['KRO_COOKIE_PREFIX'].'lang']) $current_language =  $language->language;
                //     $languages_options[] = array(
                //         'link' => "?{$_ENV['KRO_COOKIE_PREFIX']}lang={$language->code}",
                //         'label' => $language->language
                //     );
                // }
                // $Builder::generateDropdown('languages',$current_language,$languages_options);
                ?>

                <?php 
                // $current_currency = L::menu_currencies;
                // $currencies = $Db->select('currencies','*','active=1','name ASC');
                // while($currency = $currencies->fetch_object()){        
                //     if($currency->code === $_SESSION[$_ENV['KRO_COOKIE_PREFIX'].'currency']) $current_currency =  "{$currency->symbol}";
                //     $currencies_options[] = array(
                //         'link' => "?{$_ENV['KRO_COOKIE_PREFIX']}currency={$currency->code}",
                //         'label' => "{$currency->name} ({$currency->symbol})"
                //     );
                // }
                // $Builder::generateDropdown('currencies',$current_currency,$currencies_options);
                ?>

                <?php 
                if($Auth::isAuthenticated()){
                    
                    $user = $Auth::getUser();

                    $user_menu[] = array("link" => $Router->getBasePath().'profile', "label" => L::menu_profile, "active" => $page->slug==='profile' ? 'text-danger border-bottom border-danger' : '');
                    if($Auth::isAdmin()) $user_menu[] = array("link" => $Router->getBasePath().'admin', "label" => L::menu_admin, "active" => $page->slug==='admin' ? 'text-danger border-bottom border-danger' : '');        
                    $user_menu[] = array("link" => $Router->getBasePath().'logout', "label" => "Logout", "active" => '');
                    $Builder::generateDropdown('user_menu',"{$user->name} {$user->surname}",$user_menu);                
                }else{
                    ?>
                    <a class="mx-2 text-white <?=$page->slug==='login' ? 'text-danger border-bottom border-danger' : ''?>" href="<?=$Router->getBasePath().'login'?>"><?=L::menu_login?></a>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</nav>

<?php include("src/components/ui/menu-overlay/menu-overlay.inc.php"); ?>