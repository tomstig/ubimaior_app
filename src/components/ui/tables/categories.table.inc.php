<?php
$table_columns = ['category', 'slug', 'status'];
foreach ($table_columns as $column_name) {
    if(!isset($_REQUEST['sortby']) && $column_name==='category'){
        $carets['category'] = 'sort-down';
    }else if($column_name === $_REQUEST['sortby']){
        if(!isset($_REQUEST['sort_dir']) || $_REQUEST['sort_dir']==='down')
            $carets[$column_name] = 'sort-down';
        else
            $carets[$column_name] = 'sort-up';
    }else
        $carets[$column_name] = 'sort-down';
}
?>
<div class="table-responsive">
    <table class="table border-left border-right categories-table shadow-sm rounded">
        <thead>
            <tr class="bg-white text-muted">
                <th class="border-bottom-0">
                    <div class="form-check">
                        <input class="form-check-input position-static" type="checkbox" id="blankCheckbox" value="option1" aria-label="...">
                    </div>
                </th>
                <th class="border-bottom-0">Category <i class="fas fa-<?=$carets['category']?>"></i></th>
                <th class="border-bottom-0">Slug <i class="fas fa-<?=$carets['slug']?>"></i></th>            
                <th class="border-bottom-0">Status <i class="fas fa-<?=$carets['status']?>"></i></th>
                <th class="border-bottom-0">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if($categories->num_rows>0){
                while($category = $categories->fetch_object()){                                
                    ?>
                    <tr class="bg-white border-bottom category-row">
                        <td width="5%">
                            <div class="form-check">
                                <input class="form-check-input position-static" type="checkbox" id="blankCheckbox" value="option1" aria-label="...">
                            </div>
                        </td>
                        <td width="30%">
                            <div class="d-flex flex-row justify-content-start align-items-start">
                                <?php if($category->image): ?>
                                    <img src="<?=__FILE_ROOT__."/{$category->image}"?>" class="mr-3 rounded" height="50" alt="demo category">
                                <?php endif; ?>
                                <div>
                                    <p class="p-0 m-0"><?=$category->category?></p>
                                    <?php if($category->icon): ?><span class="text-muted p-0 m-0">Icon: <i class="<?=$category->icon?>"></i></span><?php endif; ?>
                                </div>
                            </div>
                        </td>
                        <td width="30%">                        
                            <p class="p-0 m-0"><?=$category->slug?></p>                        
                        </td>                    
                        <td width="10%" align="center">
                            <div class="table-switch text-left mx-auto">
                                <input id="table-switch-<?=$category->ID?>" name="table-switch" type="checkbox" <?=$category->active ? 'checked' : ''?>/>
                                <label for="table-switch-<?=$category->ID?>"></label>
                            </div>
                        </td>
                        <td width="25%" align="center">
                            <a href="<?="{$Router->getBasePath()}admin/categories/edit/{$category->ID}"?>" class="text-decoration-none">
                                <span class="edit-button mr-2" data-toggle="tooltip" data-title="Edit" data-placement="top">                                
                                    <i class="fas fa-pen"></i>                                
                                </span>
                            </a>
                            <form class="d-inline" action="">
                                <buttom type="submit" class="delete-button mr-2" data-toggle="tooltip" data-title="Delete" data-placement="top">
                                    <i class="fas fa-trash"></i>
                                </buttom>
                            </form>
                            <!-- <div class="dropdown d-inline mt-2">
                                <a class="text-decoration-none text-secondary dropdown-toggle" href="#" role="button" id="<?="dropdown-".$category->ID?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-ellipsis-h"></i>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="<?="dropdown-".$category->ID?>">                                    
                                    <a class="dropdown-item text-secondary" href="<?=$Router->getBasePath()."admin/category/{$category->uuid}"?>"><i class="fas fa-chart-line"></i> View Stats</a>
                                </div>
                            </div> -->
                        </td>
                    </tr>
                    <?php
                }
            }else{
                ?>
                <tr class="border-bottom">
                    <td colspan="5" class="bg-white text-muted text-center p-4">
                        No categories available
                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
</div>