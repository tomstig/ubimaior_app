<div class="table-responsive">
    <table class="table border-left border-right shadow-sm rounded">
        <thead>
            <tr>          
                <th>Size</th>          
                <th>Status</th>                
                <th>Options</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $sizes = $Db->select('products_sizes','*',"id_product=".$product_id,'priority,size ASC');
            if($sizes->num_rows>0){
                while($size = $sizes->fetch_object()){
                    ?>
                    <tr>
                        <td align="center">
                            <?php if(is_file("img/collections/colors/sq_".$size->code.".jpg")): ?>
                                <img src="img/collections/colors/sq_<?=$size->code?>.jpg" class="border" alt="color image" height="35" />
                            <?php else: ?>
                                <div class="more-photos-missing-picture"><i class="fas fa-ban red-t"></i></div>                            
                                <small class="red-t">Missing Color Picture</small>
                            <?php endif; ?>                        
                        </td>
                        <td><?=$size->size?></td>
                        <td><?=$size->code?></td>
                        <td align="center">
                            <a class="btn btn-sm btn-secondary" href="<?=\Karoo\Core\Configuration::getWsUrl()?>?a=delete-color&cid=<?=$size->id_color?>" onclick="return confirm('Delete this color permanently?');">Delete</a>
                        </td>
                    </tr>
                    <?php
                }
            }else{
                ?>
                <tr>
                    <td colspan="3">No product sizes added yet</td>
                </tr>
                <?php
            }        
            ?>
        </tbody>
    </table>
</div>