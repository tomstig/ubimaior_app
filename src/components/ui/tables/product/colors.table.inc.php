<div class="table-responsive">
    <table class="table border-left border-right shadow-sm rounded">
        <thead>
            <tr>          
                <th>Image</th>          
                <th>Color</th>
                <th>Code</th>            
                <th>Options</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $colors = $Db->select('products_colors','*',"id_product=".$product_id,'priority,color ASC');
            if($colors->num_rows>0){
                while($color = $colors->fetch_object()){
                    ?>
                    <tr>
                        <td align="center">
                            <?php if(is_file("img/collections/colors/sq_".$color->code.".jpg")): ?>
                                <img src="img/collections/colors/sq_<?=$color->code?>.jpg" class="border" alt="color image" height="35" />
                            <?php else: ?>
                                <div class="more-photos-missing-picture"><i class="fas fa-ban red-t"></i></div>                            
                                <small class="red-t">Missing Color Picture</small>
                            <?php endif; ?>                        
                        </td>
                        <td><?=$color->name?></td>
                        <td><?=$color->code?></td>
                        <td align="center">
                            <a class="btn btn-sm btn-secondary" href="<?=\Karoo\Core\Configuration::getWsUrl()?>?a=delete-color&cid=<?=$color->id_color?>" onclick="return confirm('Delete this color permanently?');">Delete</a>
                        </td>
                    </tr>
                    <?php
                }
            }else{
                ?>
                <tr>
                    <td colspan="4">No product colors added yet</td>
                </tr>
                <?php
            }        
            ?>
        </tbody>
    </table>
</div>