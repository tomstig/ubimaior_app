<div class="table-responsive">
    <table class="table border-left border-right shadow-sm rounded">
        <thead>
            <tr>          
                <th>Property Name</th>          
                <th>Property Value</th>
                <th>Status</th>
                <th>Options</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $properties = $Db->select('products_properties','*',"id_product=".$product_id,'priority,name ASC');
            if($properties->num_rows>0){
                while($property = $properties->fetch_object()){
                    ?>
                    <tr>
                        <td align="center">
                            <?php if(is_file("img/collections/colors/sq_".$property->code.".jpg")): ?>
                                <img src="img/collections/colors/sq_<?=$property->code?>.jpg" class="border" alt="color image" height="35" />
                            <?php else: ?>
                                <div class="more-photos-missing-picture"><i class="fas fa-ban red-t"></i></div>                            
                                <small class="red-t">Missing Color Picture</small>
                            <?php endif; ?>                        
                        </td>
                        <td><?=$property->size?></td>
                        <td><?=$property->code?></td>
                        <td align="center">
                            <a class="btn btn-sm btn-secondary" href="<?=\Karoo\Core\Configuration::getWsUrl()?>?a=delete-color&cid=<?=$property->id_color?>" onclick="return confirm('Delete this color permanently?');">Delete</a>
                        </td>
                    </tr>
                    <?php
                }
            }else{
                ?>
                <tr>
                    <td colspan="3">No product properties added yet</td>
                </tr>
                <?php
            }        
            ?>
        </tbody>
    </table>
</div>