<div class="table-responsive">
    <table class="table border-left border-right shadow-sm rounded" id="product-image-table">
        <thead>
            <tr>            
                <th></th>        
                <th>Image</th>
                <th>Color</th>
                <th>Status</th>
                <th>Options</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $images = $Db->select('products_images','*',"id_product=".$product_id,'priority ASC');
            if($images->num_rows>0){        
                while($image = $images->fetch_object()){
                    ?>
                    <tr id="<?=$photo->id?>">
                        <td align="center">
                            <i class="fas fa-arrows-alt handle"></i>
                        </td>
                        <td align="center">
                            <?php if(is_file("img/collections/".$item->collection."/more_photos/".$photo->img_name)): ?>
                                <img src="img/collections/<?=$item->collection?>/more_photos/<?=$photo->img_name?>" height="75" alt="item picture" />
                            <?php else: ?>
                                <div class="more-photos-missing-picture"><i class="fas fa-ban red-t"></i></div>                            
                                <small class="red-t">Missing Picture</small>
                            <?php endif; ?>
                        </td>                
                        <td align="center">
                            <?php if($photo->active): ?>
                                <span class="badge badge-success p-2">
                                    <i class="fas fa-check "></i>
                                    Active
                                </span>                                        
                            <?php else: ?>
                                <span class="badge badge-danger p-2">
                                    <i class="fas fa-times"></i>
                                    Inactive
                                </span>
                            <?php endif; ?>
                        </td>
                        <td align="center">
                            <div class="dropdown">
                                <a href="<?=\Karoo\Core\Configuration::getWsUrl()?>?a=delete-more-photos&id=<?=$photo->id?>&iid=<?=$_REQUEST['iid']?>&cid=<?=$_REQUEST['cid']?>&tab=more-photos" onclick="return confirm('Delete this image permanently?');" class="btn btn-sm btn-secondary" type="button">Delete</a>                                        
                            </div>
                        </td>
                    </tr>
                    <?php
                }
            }else{
                ?>
                <tr>                    
                    <td colspan="5">No product images uploaded yet!</td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
</div>