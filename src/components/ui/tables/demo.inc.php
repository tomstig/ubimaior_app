<div class="row">
    <div class="col-sm-12 mt-4">
        <h4 class="text-left">Special Inputs</h4>
        <?php 
        $special_inputs_table = new \Karoo\Builder\Table("special-inputs-table");
        $special_inputs_table->setColumns(["Img","Code", "Qty", "RRP", "Discount", "Final Price", "Status", "Actions"]);
        $special_inputs_table->hideIds(true);            
        $special_inputs_table->setRows([                                        
            array(
                "id" => "1",
                "cover" => ["type" => "__img", "value" => __FILE_ROOT__."/src/assets/demo/images/product.jpg"],
                "code" => "1234abcd",
                "qty" => ["type" => "__qty", "value" => 1],
                "rrp" => ["type" => "__currency", "value" => "1000", "currency" => "EUR"],
                "discount" => ["type" => "__discount", "value" => 20, "discount_type" => "%"],
                "amount" => ["type" => "__currency","value" => "1000", "currency" => "EUR"],
                "status" => ["type" => "__bool", "value" => true],
            ),
            array(
                "id" => "2",
                "cover" => ["type" => "__img", "value" => __FILE_ROOT__."/src/assets/demo/images/product.jpg"],
                "code" => "9876zywx",
                "qty" => ["type" => "__qty", "value" => 2],
                "rrp" => ["type" => "__currency", "value" => "2000", "currency" => "EUR"],
                "discount" => ["type" => "__discount", "value" => 10, "discount_type" => "€"],
                "amount" => ["type" => "__currency","value" => "4000", "currency" => "EUR"],
                "status" => ["type" => "__bool", "value" => false],
            ),
            array(
                "id" => "3",
                "cover" => ["type" => "__icon", "value" => "fas fa-info-circle text-dark"],
                "code" => "GENOVA-FIERA",
                "qty" => 1,
                "rrp" => ["type" => "__currency", "value" => "12300", "currency" => "EUR"],
                "discount" => "",
                "amount" => ["type" => "__currency","value" => "12300", "currency" => "EUR"],
                "status" => ["type" => "__bool", "value" => false],
            ),
            array(
                "id" => "3",
                "cover" => ["type" => "__icon", "value" => "fas fa-user-lock text-dark"],
                "code" => "CUSTOM-001",
                "qty" => 1,
                "rrp" => ["type" => "__currency", "value" => "8000", "currency" => "EUR"],
                "discount" => "",
                "amount" => ["type" => "__currency","value" => "8000", "currency" => "EUR"],
                "status" => ["type" => "__bool", "value" => true],
            ),
        ]);
        $special_inputs_table->setActions([
            "delete" => [
                "confirm" => true,                                 
                "custom_text" => "Are you sure you want to delete this item?",
                "action" => "delete",
                "controller" => "test"                                
            ],
        ]);
        echo $special_inputs_table->renderTable()
        ?>
    </div>
    <div class="col-sm-12 mt-4">
        <h4 class="text-left">Catalogues (dark mode)</h4>
        <?php 
        $catalogs_table = new \Karoo\Builder\Table("catalogues-table");
        $catalogs_table->setColumns(["Catalogue", "Items", "Currency", "Discount", "Actions"]);
        $catalogs_table->hideIds(true);
        $catalogs_table->setTableClass('table-dark');
        $catalogs_table->setRows([                                        
            array("id" => "1", "catalogue" => "Munich Fair", "items" => 4, "currency" => "€", "discount" => "20%"),
            array("id" => "1", "catalogue" => "General Catalogue €", "items" => 150, "currency" => "€", "discount" => "50%"),
        ]);
        $catalogs_table->setActions([
            "open" => [ "type" => "url", "url" => "/dashboard?open=1"],
        ]);
        echo $catalogs_table->renderTable()
        ?>
    </div>
    <div class="col-sm-12 mt-4">
        <h4 class="text-left">Requested Activations</h4>
        <?php 
        $table_requests = new \Karoo\Builder\Table("table-requests");
        $table_requests->setRows([                                        
            array("id" => "1", "name" => "Veronica", "surname" => "Martelli", "email" => "v.fiera@ubimaioritalia.com", "reseller" => "reseller name"),
            array("id" => "1", "name" => "Fabio", "surname" => "Fogli", "email" => "f.foglia@compracardano.ada", "reseller" => "Pool-over"),
        ]);
        $table_requests->setActions([
            "accept" => [
                "confirm" => true,                                 
                "custom_text" => "Are you sure you want to accept this request?",
                "action" => "accept-request",
                "controller" => "test"
            ],
            "reject" => [
                "confirm" => true,                                 
                "custom_text" => "Are you sure you want to reject this request?",
                "action" => "reject-request",
                "controller" => "test"
            ]
        ]);
        echo $table_requests->renderTable()
        ?>
    </div>
    <div class="col-sm-12 mt-4">
        <h4 class="text-left">Latest catalogues</h4>
        <?php
            $table = new \Karoo\Builder\Table("table1");                
            $table->setCheckable(true);                
            $table->setColumns(["#", "Product", "Price", "Status", "Actions"]);
            $table->setTHeadClass("round");
            $table->setRows([                                        
                array("id" => "1", "name" => "Catalog 01", "price" => ["type" => "__currency", "value" => 1000, "currency" => "EUR"], "status" => ["type" => "__bool", "value" => true]),
                array("id" => "2", "name" => "Catalog 02", "price" => ["type" => "__currency", "value" => 1200, "currency" => "EUR"], "status" => ["type" => "__bool", "value" => false]),
                array("id" => "3", "name" => "Catalog 03", "price" => ["type" => "__currency", "value" => 1300, "currency" => "EUR"], "status" => ["type" => "__bool", "value" => true]),
                array("id" => "4", "name" => "Catalog 04", "price" => ["type" => "__currency", "value" => 1400, "currency" => "EUR"], "status" => ["type" => "__bool", "value" => true]),
            ]);
            $table->setActions([
                "view" => [ "type" => "url", "url" => "/dashboard?view=1"],
                "edit" => [ "type" => "url", "url" => "/dashboard?edit=1"],
                "info" => [ "type" => "tooltip", "custom_text" => "Custom text"],
                "activity" => [ "type" => "url", "url" => "/dashboard?activity=1"],
                "users" => [ "type" => "url", "url" => "/dashboard?users=1"],
                "export" => [ 
                    "confirm" => true,
                    "custom_text" => "Are you sure you want to export the seleted item?",
                    "action" => "export",
                    "controller" => "test"                               
                ],
                "send" => [ 
                    "confirm" => true,                                 
                    "custom_text" => "Are you sure you want to resend this item?",
                    "action" => "send",
                    "controller" => "test"                                
                ],
                "duplicate" => [
                    "custom_text" => "Are you sure you want to duplicate this item?",                                
                    "action" => "duplicate",
                    "controller" => "test"                            
                ],
                "delete" => [
                    "confirm" => true,                                 
                    "custom_text" => "Are you sure you want to delete this item?",
                    "action" => "delete",
                    "controller" => "test"                                
                ],
                "accept" => [
                    "confirm" => true,                                 
                    "custom_text" => "Are you sure you want to accept this request?",
                    "action" => "accept-request",
                    "controller" => "test"
                ],
                "reject" => [
                    "confirm" => true,                                 
                    "custom_text" => "Are you sure you want to reject this request?",
                    "action" => "reject-request",
                    "controller" => "test"
                ]
            ]);
            echo $table->renderTable();                                                            
        ?>
    </div>        
    <div class="col-sm-12 mt-4">
        <h4 class="text-left">Laster products</h4>
        <?php
        $table = new \Karoo\Builder\Table("table2");
        $table->setTheme("light-theme");  
        $table->setTHeadClass("round");          
        // $table->setEditable(true);
        $table->setCheckable(true);            
        $table->setSortable(true);
        $table->setColumns(["#", "Img", "Product", "Price", "Status", "Actions"]);
        $table->setRows([                                        
            array("id" => "1", "cover" => ["type" => "__img", "value" => __FILE_ROOT__."/src/assets/demo/images/product.jpg"], "name" => "Product 01", "price" => ["type" => "__currency", "value" => 1000, "currency" => "GBP"], "status" => ["type" => "__bool", "value" => true]),
            array("id" => "2", "cover" => ["type" => "__img", "value" => __FILE_ROOT__."/src/assets/demo/images/product.jpg"], "name" => "Product 02", "price" => ["type" => "__currency", "value" => 1200, "currency" => "GBP"], "status" => ["type" => "__bool", "value" => false]),
            array("id" => "3", "cover" => ["type" => "__img", "value" => __FILE_ROOT__."/src/assets/demo/images/product.jpg"], "name" => "Product 03", "price" => ["type" => "__currency", "value" => 1300, "currency" => "GBP"], "status" => ["type" => "__bool", "value" => true]),
            array("id" => "4", "cover" => ["type" => "__img", "value" => __FILE_ROOT__."/src/assets/demo/images/product.jpg"], "name" => "Product 04", "price" => ["type" => "__currency", "value" => 1400, "currency" => "GBP"], "status" => ["type" => "__bool", "value" => true]),
        ]);
        $table->setActions([
            "view" => [ "type" => "url", "url" => "/dashboard?view=1"],
            "edit" => [ "type" => "url", "url" => "/dashboard?edit=1"],
            "info" => [ "type" => "tooltip", "custom_text" => "Custom text"],
            "activity" => [ "type" => "url", "url" => "/dashboard?activity=1"],
            "users" => [ "type" => "url", "url" => "/dashboard?users=1"],
            "export" => [ 
                "confirm" => true,
                "custom_text" => "Are you sure you want to export the seleted item?",
                "action" => "export",
                "controller" => "test"                               
            ],
            "send" => [ 
                "confirm" => true,                                 
                "custom_text" => "Are you sure you want to resend this item?",
                "action" => "send",
                "controller" => "test"                                
            ],
            "duplicate" => [
                "custom_text" => "Are you sure you want to duplicate this item?",                                
                "action" => "duplicate",
                "controller" => "test"                            
            ],
            "delete" => [
                "confirm" => true,                                 
                "custom_text" => "Are you sure you want to delete this item?",
                "action" => "delete",
                "controller" => "test"                                
            ],
            "accept" => [
                "confirm" => true,                                 
                "custom_text" => "Are you sure you want to accept this request?",
                "action" => "accept-request",
                "controller" => "test"
            ],
            "reject" => [
                "confirm" => true,                                 
                "custom_text" => "Are you sure you want to reject this request?",
                "action" => "reject-request",
                "controller" => "test"
            ]
        ]);
        echo $table->renderTable();            
        ?>
    </div>   
    <div class="col-sm-12 mt-4">
        <h4 class="text-left">Laster products</h4>
        <?php
        $editable_table = new \Karoo\Builder\Table("editable-table");
        $editable_table->setTheme("light-theme");  
        $editable_table->setTHeadClass("round");          
        $editable_table->setEditable(true);            
        $editable_table->setColumns(["#", "Img", "Product", "Price", "Status", "Actions"]);
        $editable_table->setRows([                                        
            array("id" => "1", "cover" => ["type" => "__img", "value" => __FILE_ROOT__."/src/assets/demo/images/product.jpg"], "name" => "Product 01", "price" => 1000, "status" => true),
            array("id" => "2", "cover" => ["type" => "__img", "value" => __FILE_ROOT__."/src/assets/demo/images/product.jpg"], "name" => "Product 02", "price" => 1200, "status" => false),
            array("id" => "3", "cover" => ["type" => "__img", "value" => __FILE_ROOT__."/src/assets/demo/images/product.jpg"], "name" => "Product 03", "price" => 1300, "status" => true),
            array("id" => "4", "cover" => ["type" => "__img", "value" => __FILE_ROOT__."/src/assets/demo/images/product.jpg"], "name" => "Product 04", "price" => 1400, "status" => true),
        ]);            
        echo $editable_table->renderTable();            
        ?>
    </div>    
</div>