<?php
$table_columns = ['info', 'categories', 'tags', 'status'];
foreach ($table_columns as $column_name) {
    if(!isset($_REQUEST['sortby']) && $column_name==='info'){
        $carets['info'] = 'sort-down';
    }else if($column_name === $_REQUEST['sortby']){
        if(!isset($_REQUEST['sort_dir']) || $_REQUEST['sort_dir']==='down')
            $carets[$column_name] = 'sort-down';
        else
            $carets[$column_name] = 'sort-up';
    }else
        $carets[$column_name] = 'sort-down';
}
?>
<div class="table-responsive">
    <table class="table border-left border-right products-table shadow-sm rounded">
        <thead>
            <tr class="bg-white text-muted">
                <th class="border-bottom-0">
                    <div class="form-check">
                        <input class="form-check-input position-static" type="checkbox" id="blankCheckbox" value="option1" aria-label="...">
                    </div>
                </th>
                <th class="border-bottom-0">Product Info <i class="fas fa-<?=$carets['info']?>"></i></th>
                <th class="border-bottom-0">Categories <i class="fas fa-<?=$carets['categories']?>"></i></th>
                <th class="border-bottom-0">Tags <i class="fas fa-<?=$carets['tags']?>"></i></th>
                <th class="border-bottom-0">Status <i class="fas fa-<?=$carets['status']?>"></i></th>
                <th class="border-bottom-0">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if($products->num_rows>0){
                while($product = $products->fetch_object()){
                    $categories = $Db->select('products_categories pc',
                        'c.category',
                        'pc.id_product='.$product->id,
                        'c.category ASC',
                        null,
                        'LEFT JOIN '.$Db->getTablePrefix().'categories c ON c.id=pc.id_category'
                    );                    
                    ?>
                    <tr class="bg-white border-bottom product-row">
                        <td width="5%">
                            <div class="form-check">
                                <input class="form-check-input position-static" type="checkbox" id="blankCheckbox" value="option1" aria-label="...">
                            </div>
                        </td>
                        <td width="30%">
                            <div class="d-flex flex-row justify-content-start align-items-start">
                                <img src="<?=__FILE_ROOT__."/{$product->image}"?>" class="mr-3 rounded" height="50" alt="demo product">
                                <div>
                                    <p class="p-0 m-0"><?=$product->name?></p>
                                    <small class="text-muted p-0 m-0">€ <?=$product->price?></small>
                                </div>
                                
                            </div>                            
                        </td>
                        <td width="20%">
                            <?php if($categories->num_rows>0){
                                while($category = $categories->fetch_object()){
                                    $categories_list[] = $category->category;
                                }     
                                $Builder->generateChips($categories_list);
                            }else{
                                echo 'n.d.';
                            }?>
                        </td>
                        <td width="15%">
                            <?php
                            if($product->tags){
                                $tags = explode(",",$product->tags);                                
                                $Builder->generateChips($tags);
                            }else{
                                echo 'n.d.';
                            }
                            ?>
                        </td>
                        <td width="10%" align="center">
                            <div class="table-switch text-left mx-auto">
                                <input id="table-switch-<?=$product->id?>" name="table-switch" type="checkbox" <?=$product->active ? 'checked' : ''?>/>
                                <label for="table-switch-<?=$product->id?>"></label>
                            </div>
                        </td>
                        <td width="20%" align="center">
                            <a href="<?="{$Router->getBasePath()}admin/products/edit/{$product->id}"?>" class="text-decoration-none">
                                <span class="edit-button mr-2" data-toggle="tooltip" data-title="Edit" data-placement="top">
                                    <i class="fas fa-pen"></i>
                                </span>
                            </a>
                            <form class="d-inline" action="">
                                <buttom type="submit" class="delete-button mr-2" data-toggle="tooltip" data-title="Delete" data-placement="top">
                                    <i class="fas fa-trash"></i>
                                </buttom>
                            </form>                            
                            <!-- <div class="dropdown d-inline mt-2">
                                <a class="text-decoration-none text-secondary dropdown-toggle" href="#" role="button" id="<?="dropdown-".$product->id?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-ellipsis-h"></i>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="<?="dropdown-".$product->id?>">                                    
                                    <a class="dropdown-item text-secondary" href="<?=$Router->getBasePath()."admin/product/{$product->uuid}"?>"><i class="fas fa-chart-line"></i> View Stats</a>
                                </div>
                            </div> -->
                        </td>
                    </tr>
                    <?php
                }
            }else{
                ?>
                <tr class="border-bottom">
                    <td colspan="6" class="bg-white text-muted text-center p-4">
                        No products available
                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
</div>