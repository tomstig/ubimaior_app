<div class="pt-3 px-3 pb-5" style="background-color: #F8F7FC;">
    
    <?php include('src/components/forms/admin/product/images.form.php'); ?> 

    <div class="card shadow-sm mb-3">        
        <div class="card-body">
            <h5 class="card-title">Product Images</h5>
            <div class="table-responsive">
                <?php include('src/components/ui/tables/product/images.table.inc.php'); ?> 
            </div>
        </div>
    </div>
                              
    
</div>