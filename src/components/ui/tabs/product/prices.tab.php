<?php 
$prices = $Db->select('products_prices','*',"id_product={$product_id}");
$colors = $Db->select('products_colors','*',"id_product={$product_id}");
$sizes = $Db->select('products_sizes','*',"id_product={$product_id}");
?>
<div class="pt-3 px-3 pb-5 border border-top-0" style="background-color: #F8F7FC;">
    <h3>Manage Prices</h3>
    <?php
    if($colors->num_rows > 0 || $sizes->num_rows > 0){
        ?>
        <p>Please choose price rule:</p>
        <div class="form-row">
            <div class="col">
                <div class="form-check form-check-inline">
                    <input class="form-check-input price-rule" type="radio" name="price-rule" id="price-rule-pick" value="one-price" checked="checked" required>
                    <label class="form-check-label" for="price-rule-pick">One price only</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input price-rule" type="radio" name="price-rule" id="price-rule-image" value="price-each" required>
                    <label class="form-check-label" for="price-rule-image">Price for each variations</label>
                </div>
            </div>
            <div class="spacer20"></div>
        </div>
        <?php
    }else{
        ?>
        <div class="form-row">
            <div class="col">
                <div class="form-group">
                    <label for="name">Specify Product Price</label>
                    <input type="text" name="price" class="form-control" id="price" placeholder="e.g. 100,00" required />
                </div>
            </div>                        
        </div>
        <?php
    }
    ?>
</div>