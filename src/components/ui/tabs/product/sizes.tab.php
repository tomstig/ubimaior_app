<div class="pt-3 px-3 pb-5 border-top-0" style="background-color: #F8F7FC;">    
    <div class="row">
        <div class="col-md-8">                    
            <div class="card shadow-sm mb-3">
                <div class="card-body">
                    <h5 class="card-title">Manage Product Sizes</h5>                    
                    <?php include('src/components/ui/tables/product/sizes.table.inc.php'); ?>                     
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="sticky">                      
                <?php include('src/components/forms/admin/product/sizes.form.php'); ?> 
            </div>
        </div>
    </div>    
</div>