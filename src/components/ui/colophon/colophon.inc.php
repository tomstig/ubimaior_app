<footer class="text-black mt-auto py-3 text-center">
    <div class="font-weight-light">
        <p>Ubimaior Italia - powered by <a class="text-danger" href="https://meetico.ltd">Meetico LTD</a>.</p>
    </div>
</footer>