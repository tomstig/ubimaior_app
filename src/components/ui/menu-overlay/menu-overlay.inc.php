<!-- The overlay -->
<div id="nav" class="overlay">
    <div class="overlay-inner-cont">
        <!-- Overlay content -->
        <div class="overlay-content row">
            <div class="col-5 col-sm-6 col-md-3 first-menu-col">
                <div class="row">
                    <ul class="menu-col col-xs-12">
                        <li class="menu-col-tit"><?=L::menu_pages?></li>
                        <li><a href="index.php">Home</a></li>
                        <li><a href="/about"><?=L::menu_about?></a></li>                            
                        <li><a href="index.php?p=catalog&idl=4"><?=L::menu_catalogue?></a></li>
                        <li><a href="pdf/UbiMaiorItalia_Catalog_eng.pdf" target="_blank"><?=L::menu_pdf_catalogue?></a></li>
                        <li><a href="/resellers"><?=L::menu_dealers?></a></li>
                        <li><a href="/wishlist">Wishlist</a></li>
                        <li><a href="/contacts"><?=L::menu_contacts?></a></li>
                        <li style="height:40px;"></li>
                        <?php if($_SESSION[$_ENV['KRO_COOKIE_PREFIX'].'lang']=='it'): ?>
                            <li><a href="<?="?{$_ENV['KRO_COOKIE_PREFIX']}lang=en"?>"><img src="<?=__FILE_ROOT__?>/src/assets/images/en-flag.png" alt="english flag" width="25" /></a></li>
                        <?php else: ?>
                            <li><a href="<?="?{$_ENV['KRO_COOKIE_PREFIX']}lang=it"?>"><img src="<?=__FILE_ROOT__?>/src/assets/images/it-flag.png" alt="italian flag" width="25" /></a></li>
                        <?php endif; ?>  
                        <li>
                            <a href="pdf/WLG-<?=$_SESSION[$_ENV['KRO_COOKIE_PREFIX'].'lang']?>.pdf" target="_blank">
                                <span class="fa fa-file-pdf" style="font-size:13px;"></span> <i style="font-size:15px;"><?=L::menu_warranty?></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <ul class="col-6 col-sm-4 col-md-4 menu-col">
                <li class="menu-col-tit"><?=L::menu_products_title?></li>
                <li><a href="/lines/yatch-club">YC Yacht Club</a></li>
                <li><a href="/lines/regata">RT Regata</a></li>
                <li><a href="/lines/x3m-flight">X3M Flight</a></li>
                <li><a href="/lines/jiber">JB Jiber</a></li>
                <li><a href="/lines/furlers">FR Avvolgitori</a></li>
                <li><a href="/lines/accessories"><?=L::menu_accessories?></a></li>
                <li><a href="/lines/sailmakers">Sailmakers</a></li>
                <li><a href="/lines/custom">Custom</a></li>
                <li><a href="/lines/alphalock">Alphalock</a></li>
                <li><a href="index.php?p=apparel">Apparel</a></li>
                <li><a href="/lines/other-brands"><?=L::menu_others?></a></li>
            </ul>
        </div>
    </div>
</div>