<div class="form-check <?=$inline && "form-check-inline"?>">
    <input type="checkbox" class="form-check-input <?=!$label && "position-static"?> <?=$custom_classes?>" id="<?=$this->e($id)?>" <?=$attributes?>>
    <?php if($label): ?>
        <label class="form-check-label" for="<?=$this->e($id)?>"><?=$this->e($label)?></label>
    <?php endif; ?>
    <?php if($helper_text): ?>
        <small id="<?=$this->e($id)?>" class="form-text text-muted"><?=$this->e($helper_text)?></small>
    <?php endif; ?>
    <?php if($valid_feedback): ?>
        <div class="valid-feedback"><?=$this->e($valid_feedback)?></div>
    <?php endif; ?>
    <?php if($invalid_feedback): ?>
        <div class="invalid-feedback"><?=$this->e($invalid_feedback)?></div>
    <?php endif; ?>
</div>