<div class="custom-control custom-switch">
  <input type="checkbox" class="custom-control-input" id="<?=$this->e($id)?>"  <?=$attributes?>>
    <?php if($label): ?>
        <label class="custom-control-label" for="<?=$this->e($id)?>"><?=$this->e($label)?></label>
    <?php endif; ?>
</div>