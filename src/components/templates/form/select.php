<div class="form-group">
    <?php if($label): ?>
    <label for="<?=$this->e($id)?>"><?=$this->e($label)?></label>
    <?php endif; ?>
    <select id="<?=$this->e($id)?>" class="form-control <?=$custom_classes?>" <?=$attributes?>>
        <option><?=$this->e($select_placeholder)?></option>
        <?php foreach($options as $option): ?>
            <option value="<?=$this->e($option["value"])?>">
                <?=$this->e($option["label"])?>
            </option>
        <?php endforeach ?>
    </select>
    <?php if($helper_text): ?>
        <small id="<?=$this->e($id)?>" class="form-text text-muted"><?=$this->e($helper_text)?></small>
    <?php endif; ?>
    <?php if($valid_feedback): ?>
        <div class="valid-feedback"><?=$this->e($valid_feedback)?></div>
    <?php endif; ?>
    <?php if($invalid_feedback): ?>
        <div class="invalid-feedback"><?=$this->e($invalid_feedback)?></div>
    <?php endif; ?>
</div>