            <div id="footer">
                <div class="container-fluid newsletter-container pt-5 pb-5 text-center">
                    <div class="row">
                        <div class="col col-sm-12 text-center">
                            <a href="http://eepurl.com/cBk9df" target="_blank">
                                <h3><i class="fa fa-envelope"></i> <?=L::footer_newsletter?></h3>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="container pt-5 pb-5 text-center">
                    <div class="row justify-content-center">
                        <div class="col-sm-4 col-sm-push-4 ">
                            <img src="<?=__FILE_ROOT__?>/src/assets/images/ubi-footer.png" class="img-responsive" alt="">
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <a class="fancybox" href="<?=__FILE_ROOT__?>/src/assets/images/regione-toscana-big.jpg">
                            <ul>
                                <li><img src="<?=__FILE_ROOT__?>/src/assets/images/regione-toscana-POR-CreO14_20.png" width="100" style="margin:50px 0 0;" alt="regione toscana fondo"></li>
                                <li><p>Progetto co-finanziato dal <br> POR FESR Toscana 2014-2020</p></li>
                            </ul>
                        </a>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-sm-12 mt-5">
                            <a href="https://www.google.it/maps/place/UBIMAIOR+ITALIA/@43.8210758,11.3903583,17z/data=!3m1!4b1!4m5!3m4!1s0x132baa7cc10779d1:0x9e7a47a1c54439e7!8m2!3d43.821072!4d11.392547" target="_blank">UBI MAIOR ITALIA <sup>&reg;</sup> by DINI s.r.l., Meccanica di precisione <br> via di Serravalle 35 / 37 / 39 - 50065 - Molino del Piano - Pontassieve - Firenze &nbsp; - &nbsp;<span class="fa fa-map-o"></span></a><br> Tel. 055 8364421 - Fax 055 8364614 - info@ubimaioritalia.com
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="spacer20"></div>
                        <p><a href="index.php?p=privacy-policy">Read our <span class="text-red">Privacy Policy</span></a></p>
                    </div>
                </div>
            </div>


            <?php $Builder::getAppColophon(); ?>

        </div>        

        <?php $Builder::loadFrameworkJs(); ?>
        <?php $Builder::loadDefaultJs(); ?>
        <?php $Builder::loadPageJs($page->js); ?>
        

        <?php //$tracker->run(); ?>        

    </body>
</html>