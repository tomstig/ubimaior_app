<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

		<title>Karoo Framework - Admin Panel</title>		

		<meta name="viewport" content="width=device-width; initial-scale=1.0" />		

		<base href="/">

		<?php $Builder::loadFrameworkCss(); ?>
		<?php $Builder::loadDefaultCss(); ?>
		<?php $Builder::loadPageCss($page->css); ?>

	</head>
	<body class="vh-100 m-0">		
		<div class="container-fluid m-0 p-0">
			<div class="row m-0">
				<div class="col-sm-12 m-0 p-0">
					<div class="topbar-container sticky-top bg-black w-100 m-0 py-2 px-4 d-flex flex-row justify-content-between align-items-center">
						<h5 class="w-75 m-0 p-0 text-white"><img src="src/assets/images/logo_ubi_white.png" height="35" alt="ubimaior italia logo" /></h5>
						<?php $Builder->includeTopbar(); ?>						
					</div>
					<div class="drawer hide">						
						<div class="text-left d-flex justify-content-center align-items-start sidebar-menu-container" aria-hidden="true">
							<?php $Builder->includeSidebar($page->slug); ?>
						</div>
					</div>
					<div class="admin-content bg-light">			
                            