<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

		<title>Karoo Framework</title>		

		<meta name="viewport" content="width=device-width; initial-scale=1.0" />		

		<base href="/">

		<?php $Builder::loadFrameworkCss(); ?>
		<?php $Builder::loadDefaultCss(); ?>
		<?php $Builder::loadPageCss($page->css); ?>		

	</head>
	<body class="vh-100">		
		<div class="h-100 d-flex flex-column">
			<?php if(!include('src/components/menus/'.$page->menu_path)) throw new \Exception('Cannot load page menu'); ?>
			<div class="spacer30"></div>