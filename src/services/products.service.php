<?php
/**
* Karoo App - Products Web Service
*
* @category   Framework Service
* @package    Karoo
* @author     Meetico LTD <hello@meetico.ltd>
* @author     Simone Landi <s.landi@meetico.ltd>
* @copyright  2021 Meetico LTD
* @license    http://www.php.net/license/3_01.txt  PHP License 3.01
* @link       https://meetico.ltd
*/

switch (strtolower($_SERVER['REQUEST_METHOD'])) {
    
    case 'post':
            switch ($_POST['action']) {
                case 'create':
                    if(!$Auth::isAuthenticated() || !$Auth::isAdmin()){
                        throw new \Exception('Invalid Request Method: '.$_SERVER['REQUEST_METHOD']);
                    }

                    $Product = new \Karoo\Controllers\Product($Db);
                    $Image = new \Karoo\Builder\ImageHandler(); 
                    $payload = $Db->cleanDataForDb($_POST);

                    $destination = 'src/images/products';
                    $filename = $Image->uploadImage($_FILES['image'],$destination);				

                    if(is_object($filename)) throw new Exception('Error while uploading image: '.$filename->error);

                    $res = $Product->create($payload);
                    if($res->status == 500)  throw new Exception($res->errno." - ".$res->error." - <br />".$res->query);
                    if($res->status == 400)  throw new Exception($res->error);

                    $product_id = $res->product_id;

                    $res = $Product->setCategories($payload['categories'], );
                    if($res->status == 500)  throw new Exception($res->errno." - ".$res->error." - <br />".$res->query);
                    
                    $Router->redirect($_ENV['KRO_APP_BASEURL']."/admin/products/edit/{$product_id}&cb=ok");                    
                    break;                                
            }
        break;

    default:
        throw new \Exception('Invalid Request Method: '.$_SERVER['REQUEST_METHOD']);
        break;

}