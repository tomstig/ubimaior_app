<?php
/**
* Karoo App - Autocomplete Web Service
*
* @category   Framework Service
* @package    Karoo
* @author     Meetico LTD <hello@meetico.ltd>
* @author     Simone Landi <s.landi@meetico.ltd>
* @copyright  2021 Meetico LTD
* @license    http://www.php.net/license/3_01.txt  PHP License 3.01
* @link       https://meetico.ltd
*/

switch (strtolower($_SERVER['REQUEST_METHOD'])) {
    
    case 'post':
            switch ($_POST['action']) {
                case 'get-items':
                    if(!$Auth::isAuthenticated() || !$Auth::isAdmin()){
                        throw new \Exception('Invalid Request Method: '.$_SERVER['REQUEST_METHOD']);
                    }

                    $payload = $Db->cleanDataForDb($_POST);

                    $res = $Db->select($payload['table'],$payload['fields'],null,$payload['orderby']);                
                    while($item = $res->fetch_object()){
                        $items[] = $item;
                    }
                    echo json_encode($items);
                 
                    break;                                
            }
        break;

    default:
        throw new \Exception('Invalid Request Method: '.$_SERVER['REQUEST_METHOD']);
        break;

}