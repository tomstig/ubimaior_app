<?php
/**
* Karoo App - Authentication Web Service
*
* @category   Framework Service
* @package    Karoo
* @author     Meetico LTD <hello@meetico.ltd>
* @author     Simone Landi <s.landi@meetico.ltd>
* @copyright  2021 Meetico LTD
* @license    http://www.php.net/license/3_01.txt  PHP License 3.01
* @link       https://meetico.ltd
*/

switch (strtolower($_SERVER['REQUEST_METHOD'])) {
    
    case 'post':
            switch ($_POST['action']) {
                case 'login':                    
                    $User = new \Karoo\Controllers\User($Db);
                    $result = $User->login($Db->escapeString($_POST['email']),hash('sha512',$Db->escapeString($_POST['password'])));                                        
                    if($result->status === 200){                        
                        if($_POST['redirect_to']) $Router->redirect($_ENV['KRO_APP_BASEURL'].$_POST['redirect_to']);
                        // else if($result->user->role==='user') $Router->redirect($_ENV['KRO_APP_BASEURL']);
                        else $Router->redirect('/admin');
                    }else{
                        $Router->redirect('/login?cb=auth-ko&redirect_to='.$_POST['redirect_to']);
                    }
                    break;                                
            }
        break;

    default:
        throw new \Exception('Invalid Request Method: '.$_SERVER['REQUEST_METHOD']);
        break;

}