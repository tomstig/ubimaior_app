<?php
/**
* Karoo App - Authentication Web Service
*
* @category   Framework Service
* @package    Karoo
* @author     Meetico LTD <hello@meetico.ltd>
* @author     Simone Landi <s.landi@meetico.ltd>
* @copyright  2021 Meetico LTD
* @license    http://www.php.net/license/3_01.txt  PHP License 3.01
* @link       https://meetico.ltd
*/

switch (strtolower($_SERVER['REQUEST_METHOD'])) {
    
    case 'post':
            switch ($_POST['action']) {
                case 'process-payment':
                    if (isset($_POST['stripeToken']) && !empty($_POST['stripeToken'])) {
                        
                        $token = $_POST['stripeToken'];
                        
                        $response = $gateway->purchase([
                            'amount' => $_POST['amount'],
                            'currency' => 'EUR',
                            'token' => $token,
                        ])->send();
                        
                        if ($response->isSuccessful()) {
                            // payment was successful: update database
                            $arr_payment_data = $response->getData();
                                                            
                            $payment_id = $arr_payment_data['id'];
                            $amount = $_POST['amount'];
                            echo "<pre>";
                            print_r($arr_payment_data);
                            echo "</pre>";
                    
                            // // Insert transaction data into the database
                            // $isPaymentExist = $Db->query("SELECT * FROM payments WHERE payment_id = '".$payment_id."'");
                        
                            // if($isPaymentExist->num_rows == 0) { 
                            //     $insert = $Db->query("INSERT INTO payments(payment_id, amount, currency, payment_status) VALUES('$payment_id', '$amount', 'USD', 'Captured')");
                            // }
                    
                            echo "Payment is successful. Your payment id is: ". $payment_id;
                        } else {
                            // payment failed: display message to customer
                            echo $response->getMessage();
                        }                        
                    }           

                    break;                                
            }
        break;

    default:
        throw new \Exception('Invalid Request Method: '.$_SERVER['REQUEST_METHOD']);
        break;

}