<?php
return (object) [
    'database' => (object) [
        'db_connection' => 'mysql',
        'db_host' => 'localhost',
        'db_name' => 'ubimaior',
        'db_user' => 'root',
        'db_pass' => 'root',
        'db_prefix' => 'ubi_',
        'db_charset' => 'utf8'
    ],
    'frameworks' => (object) [
        'bootstrap' => (object) [
            'css' => [
                'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css',
                'https://cdn.quilljs.com/1.3.6/quill.snow.css',
                'https://cdn.jsdelivr.net/jquery.slick/1.5.7/slick.css'
            ],
            'js' => [
                'https://code.jquery.com/jquery-3.4.1.min.js',
                'https://code.jquery.com/ui/1.11.4/jquery-ui.min.js',
                'https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js',
                'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js',
                'https://cdn.quilljs.com/1.3.6/quill.js'
                // 'https://js.stripe.com/v3/',
            ]
        ]
    ],
    'licensorWS' => 'http://localhost/licensor/webservice.php'
];