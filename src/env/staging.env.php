<?php
return (object) [
    'database' => [
        'db_connection' => 'mysql',
        'db_host' => 'localhost',
        'db_name' => 'zululand_karoo',
        'db_user' => 'zululand_karoo',
        'db_pass' => 'JzeNt%Y_51**5VZ7M!',
        'db_prefix' => 'kro_',
        'db_charset' => 'utf8'
    ],
    'frameworks' => [
        'bootstrap' => [
            'css' => [
                'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css'
            ],
            'js' => [
                'https://code.jquery.com/jquery-3.4.1.min.js',
                'https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js',
                'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js',
                'https://js.stripe.com/v3/'
            ]
        ]
    ]
];