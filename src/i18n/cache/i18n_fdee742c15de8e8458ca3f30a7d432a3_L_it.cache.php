<?php class L {
const menu_home = 'Home';
const menu_catalogue = 'Catalogo';
const menu_404 = '404';
const menu_profile = 'Profilo';
const menu_admin = 'Admin';
const menu_languages = 'Lingue';
const menu_currencies = 'Valute';
const menu_login = 'Login';
const menu_back_to = 'Torna al';
const home_welcome = 'Benvenuti in Karoo.';
const home_payoff = 'La piattaforma facile da configurare perfetta per gestiore il tuo catalogo prodotti.';
const home_learn_cta = 'Vedi Catalogo';
const catalogue_filter = 'Filtri';
const catalogue_product_category = 'Categorie Prodotti';
const catalogue_sortby_prefix = 'Ordina per: ';
const catalogue_sortby_name = 'Nome';
const catalogue_sortby_price_hl = 'Prezzo (da alto a basso)';
const catalogue_sortby_price_lh = 'Prezzo (da basso a alto)';
const catalogue_sortby_newest = 'Nuovi';
public static function __callStatic($string, $args) {
    return vsprintf(constant("self::" . $string), $args);
}
}
function L($string, $args=NULL) {
    $return = constant("L::".$string);
    return $args ? vsprintf($return,$args) : $return;
}