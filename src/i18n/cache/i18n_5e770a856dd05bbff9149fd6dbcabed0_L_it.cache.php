<?php class L {
const shared_read_more = 'leggi di più';
const shared_products = 'Prodotti';
const menu_pages = 'PAGINE';
const menu_products_title = 'LINEE PRODOTTI';
const menu_home = 'Home';
const menu_about = 'Su di noi';
const menu_catalogue = 'Catalogo Online';
const menu_pdf_catalogue = 'Catalogo PDF';
const menu_dealers = 'Distributori';
const menu_contacts = 'Contatti';
const menu_warranty = 'Garanzia internazionale';
const menu_accessories = 'Accessori';
const menu_others = 'Altri Marchi';
const menu_404 = '404';
const menu_profile = 'Profilo';
const menu_admin = 'Admin';
const menu_languages = 'Lingue';
const menu_currencies = 'Valute';
const menu_login = 'Login';
const menu_back_to = 'Torna al';
const home_claim1 = 'SAIL BLOCKS – DECK HARDWARE – CUSTOM PROJECTS';
const home_claim2 = 'We equip your passion';
const home_alex_thomson1 = 'We are proud to have UBI MAIOR ITALIA® as our Preferred Supplier. Like us, the team at Ubi Maior prioritise quality, performance and reliability, and this is evidenced in the products which they supply to us';
const home_alex_thomson2 = 'Their attention to detail and high standards of operation make them the perfect fit for our team';
const home_about1 = '<span class="bold">50 years</span> of experience';
const home_about2 = 'Functional and constructive <span class="bold">quality</span>';
const home_about3 = '<span class="bold">Craft</span> knowledge, <span class="bold">modern</span> techniques';
const home_about4 = '<span class="bold">Performance, reliability and durability</span>';
const home_yc = 'Designed for the new generation of increasingly performant cruisers, the blocks in the YACHT CLUB series follow a new lay-out, which foresees the use of shackles to be fixed, and are guaranteed to meet the highest strength, versatility and design criteria';
const home_rt = 'The RT line turns 30 years of evolution in this field into a truly ALL ROUND range of components designed for any kind of boat. RT blocks can bear very heavy weights and ensure the smoothest running mechanism thanks to TORLON (HIGH LOAD line) or DELRIN (LOW LOAD line) roller cage';
const home_x3m = 'X3M FLIGHT blocks are designed to support extremely heavy weights, and boast a highly beneficial load /dimension/ weight ratio. These blocks combine technology, functionality and design, and are designed to be used exclusively with textile fixings (included)';
const home_jiber = 'JIBER is a structural jib or genoa furler that works without the usual aluminum extruded foils. The rod forestay is linked to the drum and swivel, and rotates to transmit the torque and furl the sail';
const home_furlers = 'FR FURLERS are extremely reliable and functional for managing bow sails, both code zero and gennaker. This complete line ranges from products suited to dinghy and smaller vessels, right up to larger units and custom designed products';
const home_accessories = 'UBI MAIOR ITALIA&#174; offers a complete range of rigging equipment and accessories';
const home_sailmakers = 'UBI MAIOR ITALIA&#174; produces a series of technical accessories for sailmakers';
const home_custom = 'Thanks to the company&rsquo;s in house production facilities, UBI MAIOR ITALIA&#174; is able to realize custom projects of all types, easily and quickly';
const home_alphalock = 'Introducing Alphalock new Modular Lock Series, an all new automatic (no tripline) halyard lock';
const home_apparel = 'Explore the UBI MAIOR ITALIA&#174; technical range of clothing and accessories';
const home_others = 'UBI MAIOR ITALIA&#174; is distributor and dealer for other brands of sailing hardware';
const home_warranty = 'SEE OUR INTERNATIONAL WARRANTY';
const home_follow_us = 'Follow us';
const catalogue_filter = 'Filtri';
const catalogue_product_category = 'Categorie Prodotti';
const catalogue_sortby_prefix = 'Ordina per: ';
const catalogue_sortby_name = 'Nome';
const catalogue_sortby_price_hl = 'Prezzo (da alto a basso)';
const catalogue_sortby_price_lh = 'Prezzo (da basso a alto)';
const catalogue_sortby_newest = 'Nuovi';
const footer_newsletter = 'Iscriviti alla <span class="text-red">Newsletter Ubi Maior Italia</span> per ricevere aggiornamenti e promozioni sui nostri prodotti';
public static function __callStatic($string, $args) {
    return vsprintf(constant("self::" . $string), $args);
}
}
function L($string, $args=NULL) {
    $return = constant("L::".$string);
    return $args ? vsprintf($return,$args) : $return;
}