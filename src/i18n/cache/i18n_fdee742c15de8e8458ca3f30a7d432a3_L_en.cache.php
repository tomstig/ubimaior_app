<?php class L {
const menu_home = 'Home';
const menu_catalogue = 'Catalogue';
const menu_404 = '404';
const menu_profile = 'Profile';
const menu_admin = 'Admin';
const menu_languages = 'Languages';
const menu_currencies = 'Currencies';
const menu_login = 'Login';
const menu_back_to = 'Back to';
const home_welcome = 'Welcome to Karoo.';
const home_payoff = 'The dead-simple, lightweight, highly customizable catalogue handler for your business.';
const home_learn_cta = 'View Catalogue';
const catalogue_filter = 'Filter';
const catalogue_product_category = 'Product Category';
const catalogue_sortby_prefix = 'Sort By: ';
const catalogue_sortby_name = 'Name';
const catalogue_sortby_price_hl = 'Price (high to low)';
const catalogue_sortby_price_lh = 'Price (low to high)';
const catalogue_sortby_newest = 'Newest';
public static function __callStatic($string, $args) {
    return vsprintf(constant("self::" . $string), $args);
}
}
function L($string, $args=NULL) {
    $return = constant("L::".$string);
    return $args ? vsprintf($return,$args) : $return;
}