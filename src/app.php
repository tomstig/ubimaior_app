<?php
if($_ENV['DEBUG']==='1'){
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ERROR);
}
$uri =  $Router->getCurrentUri();
$page = $Router->match($__routes,$uri);

define("__FILE_ROOT__",'');

switch ($page->file_path) {
    
    case 404:
        include('src/pages/__defaults/404/404.page.php');
        break;
    
    case 403:
        include('src/pages/__defaults/403/403.page.php');
        break;    
    
    default:        
        if(!include('src/components/layout/headers/'.$page->header_path)) throw new \Exception('Cannot load page header: '.'./components/layout/headers/'.$page->header_path);        
        if(!include('src/pages/'.$page->file_path)) throw new \Exception('Cannot load page');
        if(!include('src/components/layout/footers/'.$page->footer_path)) throw new \Exception('Cannot load page footer');
        break;
}