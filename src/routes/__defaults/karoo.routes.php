<?php
$__karoo_routes = [
    '/' => [
        'slug' => 'home',
        'file_path' => '__defaults/home/home.page.php',        
        'auth_required' => false,
        'css' => ['home.css','categories.css','slick-theme.css'],
        'js' => ['home.js','carousel.js']
    ],
    '/login/' =>[
        'slug' => 'login',
        'file_path' => '__defaults/login/login.page.php',
        'auth_required' => false,
    ],
    '/logout/' =>[
        'file_path' => '__defaults/logout/logout.page.php',        
        'auth_required' => false,
    ],    
    '/profile/(.*)' =>[        
        'url_prefix' => '/profile',
        'auth_required' => true,
        'grant_types' => ['user','admin'],
        'routes' => [
            '/' => [
                'slug' => 'profile',
                'file_path' => '__defaults/profile/profile.page.php',                
            ]
        ]
    ],    
];