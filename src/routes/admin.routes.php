<?php
$__admin_routes = [
    '/admin/(.*)' => [
        'url_prefix' => '/admin',
        'auth_required' => true,
        'grant_types' => [1],
        'routes' => [
            '/' => [
                'slug' => 'dashboard',
                'file_path' => 'admin/dashboard/dashboard.page.php',
                'menu_path' => '__defaults/admin/menu.inc.php',
                'header_path' => '__defaults/admin/header.inc.php',
                'footer_path' => '__defaults/admin/footer.inc.php',
                'css' => ['__defaults/admin.karoo.css', 'admin/transfer-lists.css','admin/modal.css','admin/table.css']
            ],            
            '/categories/(.*)' => [
                'slug' => 'categories',
                'url_prefix' => '/admin/categories',
                'auth_required' => true,
                'grant_types' => [1],
                'routes' => [
                    '/' => [
                        'slug' => 'categories',
                        'file_path' => 'admin/categories/categories.page.php',
                        'menu_path' => '__defaults/admin/menu.inc.php',
                        'header_path' => '__defaults/admin/header.inc.php',
                        'footer_path' => '__defaults/admin/footer.inc.php',
                        'css' => ['__defaults/admin.karoo.css','admin/categories.css']
                    ],
                    '/add/' => [
                        'slug' => 'categories',
                        'file_path' => 'admin/categories_form/categories_form.page.php',
                        'menu_path' => '__defaults/admin/menu.inc.php',
                        'header_path' => '__defaults/admin/header.inc.php',
                        'footer_path' => '__defaults/admin/footer.inc.php',
                        'css' => ['__defaults/admin.karoo.css','admin/categories.css']
                    ],
                    '/edit/(.*)' => [
                        'slug' => 'categories',
                        'file_path' => 'admin/categories_form/categories_form.page.php',
                        'menu_path' => '__defaults/admin/menu.inc.php',
                        'header_path' => '__defaults/admin/header.inc.php',
                        'footer_path' => '__defaults/admin/footer.inc.php',
                        'css' => ['__defaults/admin.karoo.css','admin/categories.css']
                    ]
                ]                
            ],
            '/products/(.*)' => [
                'slug' => 'products',
                'url_prefix' => '/admin/products',
                'auth_required' => true,
                'grant_types' => [1],
                'routes' => [
                    '/' => [
                        'slug' => 'products',
                        'file_path' => 'admin/products/products.page.php',
                        'menu_path' => '__defaults/admin/menu.inc.php',
                        'header_path' => '__defaults/admin/header.inc.php',
                        'footer_path' => '__defaults/admin/footer.inc.php',
                        'css' => ['__defaults/admin.karoo.css','admin/products.css']
                    ],
                    '/add/' => [
                        'slug' => 'products',
                        'file_path' => 'admin/product_form/product_form.page.php',
                        'menu_path' => '__defaults/admin/menu.inc.php',
                        'header_path' => '__defaults/admin/header.inc.php',
                        'footer_path' => '__defaults/admin/footer.inc.php',
                        'css' => ['__defaults/admin.karoo.css','admin/products.css'],
                        'js' => ['admin/product.js']
                    ],
                    '/edit(.*)' => [
                        'slug' => 'products',
                        'file_path' => 'admin/product_form/product_form.page.php',
                        'menu_path' => '__defaults/admin/menu.inc.php',
                        'header_path' => '__defaults/admin/header.inc.php',
                        'footer_path' => '__defaults/admin/footer.inc.php',
                        'css' => ['__defaults/admin.karoo.css','admin/products.css','admin/spectrum.css','admin/colors.css'],
                        'js' => ['admin/product.js','admin/spectrum.js','admin/colors.js']
                    ],
                ]                
            ],
            '/sendbox/(.*)' => [
                'slug' => 'sendbox',
                'file_path' => 'admin/sendbox/sendbox.page.php',
                'menu_path' => '__defaults/admin/menu.inc.php',
                'header_path' => '__defaults/admin/header.inc.php',
                'footer_path' => '__defaults/admin/footer.inc.php',
                'css' => ['__defaults/admin.karoo.css']
            ],
            '/settings/(.*)' => [
                'slug' => 'settings',
                'file_path' => 'admin/settings/settings.page.php',
                'menu_path' => '__defaults/admin/menu.inc.php',
                'header_path' => '__defaults/admin/header.inc.php',
                'footer_path' => '__defaults/admin/footer.inc.php',
                'css' => ['__defaults/admin.karoo.css']
            ]
        ]
    ]
];