<?php
if(!include_once(__DIR__.'/__defaults/karoo.routes.php')) throw new Exception('cannot include Karoo Default routes');    
if(!include_once(__DIR__.'/app.routes.php')) throw new Exception('cannot include App routes');    
if(!include_once(__DIR__.'/admin.routes.php')) throw new Exception('cannot include Admin routes');        
$__routes = (object) array_merge($__karoo_routes, $__app_routes, $__admin_routes);