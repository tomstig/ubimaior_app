<?php
$__app_routes = [
    '/about/' => [
        'slug' => 'about',        
        'file_path' => 'about/about.page.php',        
        'auth_required' => false,
        'css' => ['about.css']
    ],
    '/resellers/' => [
        'slug' => 'resellers',
        'file_path' => 'resellers/resellers.page.php',
        'auth_required' => false,
        'css' => ['resellers.css']
    ],
    '/contacts/' => [
        'slug' => 'contacts',
        'file_path' => 'contacts/contacts.page.php',
        'auth_required' => false,
        'css' => ['contacts.css']
    ],
    '/lines/(.*)' =>[        
        'url_prefix' => '/lines',
        'auth_required' => false,
        'routes' => [
            '/yatch-club/' => [
                'slug' => 'line-yatch-club',
                'file_path' => 'lines/yatch-club.page.php',
                'css' => ['lines.css','categories.css','slick-theme.css'],
                'js' => ['carousel.js']
            ],
            '/regata/' => [
                'slug' => 'line-regata',
                'file_path' => 'lines/regata.page.php',
                'css' => ['lines.css','categories.css','slick-theme.css'],
                'js' => ['carousel.js']
            ],
            '/x3m-flight/' => [
                'slug' => 'x3m-flight',
                'file_path' => 'lines/x3m-flight.page.php',
                'css' => ['lines.css','categories.css','slick-theme.css'],
                'js' => ['carousel.js']
            ],
            '/jiber/' => [
                'slug' => 'jiber',
                'file_path' => 'lines/jiber.page.php',
                'css' => ['lines.css','categories.css','slick-theme.css'],
                'js' => ['carousel.js']
            ],
            '/furlers/' => [
                'slug' => 'furlers',
                'file_path' => 'lines/furlers.page.php',
                'css' => ['lines.css','categories.css','slick-theme.css'],
                'js' => ['carousel.js']
            ],
            '/accessories/' => [
                'slug' => 'accessories',
                'file_path' => 'lines/accessories.page.php',
                'css' => ['lines.css','categories.css','slick-theme.css'],
                'js' => ['carousel.js']
            ],
            '/sailmakers/' => [
                'slug' => 'sailmakers',
                'file_path' => 'lines/sailmakers.page.php',
                'css' => ['lines.css','categories.css','slick-theme.css'],
                'js' => ['carousel.js']
            ],
            '/custom/' => [
                'slug' => 'custom',
                'file_path' => 'lines/custom.page.php',
                'css' => ['lines.css','categories.css','slick-theme.css'],
                'js' => ['carousel.js']
            ],
            '/alphalock/' => [
                'slug' => 'alphalock',
                'file_path' => 'lines/alphalock.page.php',
                'css' => ['lines.css','categories.css','slick-theme.css'],
                'js' => ['carousel.js']
            ],
            '/other-brands/' => [
                'slug' => 'other-brands',
                'file_path' => 'lines/other-brands.page.php',
                'css' => ['lines.css','categories.css','slick-theme.css'],
                'js' => ['carousel.js']
            ]
        ]
    ], 
    '/wishlist/' => [
        'slug' => 'wishlist',
        'file_path' => 'wishlist/wishlist.page.php',
        'auth_required' => false,
    ]
];