$(document).ready(function () {
    $(".first-image").animate(
        {
            marginRight: "0%",
            opacity: 1,
        },
        2000,
        function () {
            $(".subtitle h2").animate(
                {
                    opacity: 0.5,
                },
                500,
                function () {
                    $(".mouse-wrapper").fadeIn(1500);
                }
            );
        }
    );
    $(".second-image").animate(
        {
            marginLeft: "0%",
            opacity: 1,
        },
        2000
    );
});
