/**
 * 
 * @param {string} email 
 */
function validateEmail(email) {
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}

/**
 * 
 * @param {Object} input 
 * @param {string} item 
 */
function addOnEnter(input, item){

    if(input.attr('type') == 'email' && item!=='' && !validateEmail(item)){
        input.addClass('is-invalid')
        $('.autocomplete-container').empty().remove()
        return false;
    }else if(input.attr('type') !== 'email' && item!==''){
        input.addClass('is-invalid')
        $('.autocomplete-container').empty().remove()
        return false;
    }

    var autocomplete_input_values = input.attr('data-value-container')
    var selected_items_container = input.attr('data-selected-container')
    var badge = '<span class="badge badge-secondary mr-2">'
        +item
        +'<button type="button" data-value-container="'+autocomplete_input_values+'" data-value="'+item+'" class="close ml-1" aria-label="Close">x</button>'
    +'</span>'        
    
    var item_values
    if($(autocomplete_input_values).val()!=='') item_values = $(autocomplete_input_values).val()+','+item        
    else item_values = item

    $(selected_items_container).append(badge)     
    $(autocomplete_input_values).val(item_values)   
    input.val('')
    $('.autocomplete-container').empty().remove()        
    input.removeClass('is-invalid')    
}

/**
 * Collect all autocomplete templates in the page and
 * set the ready to be filled with items when their
 * trigger is fired
 */
function getAutocompleteTemplates(){
    var templates = [];
    $('.autocomplete-template').each(function(){
        templates.push({id: $(this).attr('id'), html: $(this).html().trim() })        
    });
    return templates;
}

/**
 * 
 * @param {Object} template  
 * @param {string} searchby
 * @param {Object[]} items 
 * @param {string[]} selected 
 * @param {string} keyword 
 * @param {string} selected_items_container
 */
async function renderTemplateItem(template, searchby, fields, items, selected, keyword){    
    var items_list = '';   
    var fields_array = fields.split(',') 
    items.forEach(item => {
        var item_index = _.findIndex(selected,function(iv) { return iv === item['id'] });        
        if(item_index=== -1 && item[searchby].toLowerCase().indexOf(keyword.toLowerCase()) !== -1){
            var item_html = template;
            fields_array.forEach(field => {
                item_html = _.replace(item_html,new RegExp('{{'+field+'}}',"g"),item[field])                 
            });                        
            items_list += item_html
        }
    });    
    return items_list
}

/**
 * 
 * @param {Object[]} items 
 * @param {string} searchby
 * @param {string[]} selected 
 * @param {string} keyword 
 * @param {string} id
 */
function renderDefaultItem(items, searchby, selected, keyword, id){
    var items_list = '';
    items.forEach(item => {                        
        var item_index = _.findIndex(selected,function(iv) { return iv === item['id'] });                        
        if(item_index=== -1 && item[searchby].indexOf(keyword) !== -1){
            items_list += '<div class="autocomplete-item" data-id="'+id+'" data-label="'+item[searchby]+'" data-value="'+item['id']+'">'+item[searchby]+'</div>'            
        }
    });
    return items_list
}

/**
 * 
 * @param {string} keyword
 * @param {string} searchby
 * @param {Object[]} templates
 * @param {string} fields - comma separated field name to be replaced in template
 * @param {Object[]} items 
 * @param {string} autocomplete_id 
 */
async function handleAutocompleteValues(keyword,searchby,templates,fields,items,autocomplete_id){    
    var item_values = $('.autocomplete-values#'+autocomplete_id).val().trim().split(',');
    var template = _.find(templates, { 'id': autocomplete_id });    
    var items_list = ''
    if(template){
        items_list = await renderTemplateItem(template.html,searchby,fields,items,item_values,keyword,autocomplete_id)        
        return items_list;
    }else{
        items_list = await renderDefaultItem(items,searchby,item_values,keyword,autocomplete_id)        
        return items_list;
    }        
}

$(document).ready(function(){

    var templates = getAutocompleteTemplates();

    $(document).on('keyup','[data-toggle="autocomplete"]',function(e){

        if(e.key === "Escape") {
            $('[data-toggle="autocomplete"]').blur().val('')
            $('.autocomplete-container').empty().addClass('d-none').hide() 			
            e.stopPropagation();
            e.preventDefault();
            return false;
       }

        var input = $(this);
        var keyword = input.val().trim()
        var canAddOnEnter = $(this).attr('data-add-on-enter') ? true : false        

        // Triggers Enter key; if so, stop everything
		if (e.keyCode === 13 && canAddOnEnter) {
            return addOnEnter(input, keyword);
        }			
        
        var autocomplete_id = input.attr('id')        
        var table = input.attr('data-entity').trim()
        var fields = input.attr('data-fields').trim()
        var orderby = input.attr('data-order-by').trim()
        var searchby = input.attr('data-search-by').trim()
                
        var dropdown_offset = input.outerHeight()        
        var autocomplete_container = $('.autocomplete-container#'+autocomplete_id)
        autocomplete_container.css('top',dropdown_offset);

        if(keyword.length>0){
            $.ajax({
                method: 'post',
                url: '/karoo/webservice.php',
                dataType: 'json',
                data: { 
                    'action': 'get-items',
                    'controller': 'autocomplete',
                    'table': table,
                    'fields': fields,
                    'orderby': orderby
                }
            }).done(async function(items){
                autocomplete_container.empty()
                var autocomplete_items = await handleAutocompleteValues(keyword,searchby,templates,fields,items,autocomplete_id)                
                if(autocomplete_items.trim()=='') autocomplete_items = '<p class="mt-2 ml-2">No Items found for the keyword prompted</p>'
                autocomplete_container.html(autocomplete_items).removeClass('d-none').show()
            })
        }else{
            input.removeClass('is-invalid')
            if(autocomplete_container.length){
                autocomplete_container.empty().addClass('d-none').hide()
            }
        }
				
	})

	$(document).on('click','.autocomplete-item',function(){        
        var value = $(this).attr('data-value')
        var label = $(this).attr('data-label')        
        var id = $(this).attr('id')
		var badge = '<span class="item mr-2 text-uppercase">'
			+ label
			+'<button type="button" id="'+id+'" data-value="'+value+'" class="close ml-1" aria-label="Close">x</button>'
		+'</span>'        
        
		var item_values
		if($('.autocomplete-values#'+id).val()!=='') item_values = $('.autocomplete-values#'+id).val()+','+value        
		else item_values = value

		$('.autocomplete-selected#'+id).append(badge)     
		$('.autocomplete-values#'+id).val(item_values)   
		$('[data-toggle="autocomplete"]#'+id).val('')
		$('.autocomplete-container#'+id).empty().addClass('d-none').hide()

	})

	$(document).on('click','.autocomplete-selected .item .close',function(){
        var value = $(this).attr('data-value')
        var label = $(this).attr('data-label')
		var id = $(this).attr('id')
		if(confirm('Delete '+label+'?')){
            var item_values = $('.autocomplete-values#'+id).val()
            if(item_values.indexOf(',') !== -1){
                item_values = item_values.split(',');                
                item_values = _.filter(item_values,function(e){ return parseInt(e)!==parseInt(value) });
                $('.autocomplete-values#'+id).val(item_values.join(','))			    
            }else{
                $('.autocomplete-values#'+id).val('')			    
            }			
            $(this).parent().remove();
		} 		
	})

	$(document).on("click", function (event) {           
		if ($(event.target).closest(".autocomplete-container").length === 0 && $(event.target).closest('[data-toggle="autocomplete"]').length === 0) {
			if($('.autocomplete-container').length){
				$('[data-toggle="autocomplete"]').val('')
				$('.autocomplete-container').empty().addClass('d-none').hide() 
			}
		}
    });
});