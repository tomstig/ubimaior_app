$(document).ready(function(){
    
    $(document).on('click','.modal-trigger',function(){
        var type = $(this).data('type');
        var custom_text = $(this).data('text');
        var ws = $(this).data('url');
        var action = $(this).data('action');
        var controller = $(this).data('controller');
        var id = $(this).data('id');

        const url = `${ws}?action=${action}&controller=${controller}&id=${id}`;

        $('#'+type+'Modal .modal-submit').attr('href',url)
        $('#'+type+'Modal .modal-body-text').html(custom_text);        
        $('#'+type+'Modal').modal('show');
    });

    $('#confirmModal').on('hidden.bs.modal', function (event) {
        $('#confirmModal .modal-body-text').empty();        
    })

});