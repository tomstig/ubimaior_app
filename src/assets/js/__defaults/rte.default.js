$(document).ready(function(){
    if($('[data-toggle="rte"]').length>0){
        var toolbarOptions = [['bold', 'italic', 'underline', 'strike'],['link'],[{ 'list': 'ordered'}, { 'list': 'bullet' }],['clean']];
        var editor = new Quill('[data-toggle="rte"]',{
            theme: 'snow',
            modules: {
                toolbar: toolbarOptions
            }
        });
    
        editor.on('text-change', function() {    
            $('[data-target="rte"]').val(editor.root.innerHTML);
        });
    }
})
