$(document).ready(function () {
    $(".menu").on("click", function () {
        $(this).toggleClass("open");
        $("#searchOverlay").fadeOut(500);
        $("#nav").fadeToggle(500);
        $("body").toggleClass("disableScroll"); //disabilita lo scroll sotto l'overlay. Applicato al body
    });

    $(window).on('scroll touchmove', function() {
        $('nav').toggleClass('navScrolled', $(document).scrollTop() > 0);
    });
    
});
