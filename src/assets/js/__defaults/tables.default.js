$(document).ready(function(){

    $(document).on('change','.table .table-select-all',function(){
        var tableId = $(this).attr('id');
        var isChecked = $(this).is(':checked');        
        $("#"+tableId+" input[type='checkbox'].row-item").prop("checked" , isChecked);        
    })    

    $(document).on('click','.table-switch-trigger',function(){
        var id = $(this).attr("id");
        var tableId = $(this).data('table');
        var checkbox = $('#'+tableId+' #table-switch-'+id);     
        
        // console.log({ tableId, id, checked: checkbox.is(':checked') ? 'false' : 'true'})
        //TODO: ajax call to ws updating status
        console.log(`Ajax call to WS. Set stastus to: ${checkbox.is(':checked') ? 'false' : 'true'}`);
        checkbox.prop('checked',checkbox.is(':checked') ? false : true)
    })

    $(".table.sortable tbody").sortable({
        handle: ".handle",
        update: function() {
            var order = []
            $("#"+$(this).data('table')+".sortable tbody tr.ui-state-default").each(function(){
                order.push($(this).attr("id"))
            })
            //TODO: ajax call to ws updating items order
            console.log(`New order for table #${$(this).data('table')} updated: ${JSON.stringify(order)}`)
        }
    });

    $(document).on('click','.edit-table-item',function(){
        var itemId = $(this).data("id")
        var payload = { id: itemId };
        $('.row-item-input[data-id="'+itemId+'"]').each(function(){
            payload[$(this).data("field")] = $(this).val();
        })
        $('.row-item-checkbox[data-id="'+itemId+'"]').each(function(){
            payload[$(this).data("field")] = $(this).is(":checked");
        })

        //TODO: ajax call to ws updating item
        console.log('Edit payload', payload);

    })

    /** 
     * 
     * Currency, Qty and Total Amount handlers 
     * 
    */

    /** Currency formatter on page load */
    $('.currency-formatter').each(function(){
        // var itemId = $(this).data("id")
        var amount = $(this).data("amount")
        var currency = $(this).data("currency")
        var formattedAmount = new Intl.NumberFormat('it-IT', { style: 'currency', currency }).format(amount)
        $(this).empty().html(formattedAmount)
    })

    $(document).on('click','.qty-trigger',function(){
        var itemId = $(this).data("id")
        var type = $(this).data("type")        
        var qty = parseInt($('.qty[data-id="'+itemId+'"]').text())
        if(type==='sub' && qty===1){
            return false;        
        }
        
        var newQty
        if(type==='sub'){
            newQty = qty-1
            $('.qty[data-id="'+itemId+'"]').empty().html(newQty)
        }else{
            newQty = qty+1
            $('.qty[data-id="'+itemId+'"]').empty().html(newQty)
        }

        var discountValue = $('.discount-value[data-id="'+itemId+'"]').val()
        var discountType = $('.discount-type[data-id="'+itemId+'"]').val()
        var initialPriceItem = $('.currency-formatter[data-name="rrp"][data-id="'+itemId+'"]');
        var amountItem = $('.currency-formatter[data-name="amount"][data-id="'+itemId+'"]');
        var amount = parseFloat(initialPriceItem.data("amount"))*newQty;
        
        if(discountValue>0){            
            if(discountType==='%'){
                amount -= (amount*(discountValue/100))
            }else{
                amount -= discountValue;
            }            
        }
        
        var currency = amountItem.data("currency")
        var formattedAmount = new Intl.NumberFormat('it-IT', { style: 'currency', currency }).format(amount)
        amountItem.empty().html(formattedAmount)
    })

    $(document).on('change','.discount-value',function(){
        var itemId = $(this).data("id")
        
        var discountValue = $(this).val()
        var discountType = $('.discount-type[data-id="'+itemId+'"]').val()
        var initialPriceItem = $('.currency-formatter[data-name="rrp"][data-id="'+itemId+'"]');
        var amountItem = $('.currency-formatter[data-name="amount"][data-id="'+itemId+'"]');
        var qty = parseInt($('.qty[data-id="'+itemId+'"]').text())
        var amount = parseFloat(initialPriceItem.data("amount"))*qty;
        
        if(discountValue>0){            
            if(discountType==='%'){
                amount -= (amount*(discountValue/100))
            }else{
                amount -= discountValue;
            }            
        }
        
        var currency = amountItem.data("currency")
        var formattedAmount = new Intl.NumberFormat('it-IT', { style: 'currency', currency }).format(amount)
        amountItem.empty().html(formattedAmount)
    })

    $(document).on('change','.discount-type',function(){
        var itemId = $(this).data("id")
        
        var discountValue = $('.discount-value[data-id="'+itemId+'"]').val()
        var discountType = $(this).val()
        var initialPriceItem = $('.currency-formatter[data-name="rrp"][data-id="'+itemId+'"]');
        var amountItem = $('.currency-formatter[data-name="amount"][data-id="'+itemId+'"]');
        var qty = parseInt($('.qty[data-id="'+itemId+'"]').text())
        var amount = parseFloat(initialPriceItem.data("amount"))*qty;
        
        if(discountValue>0){            
            if(discountType==='%'){
                amount -= (amount*(discountValue/100))
            }else{
                amount -= discountValue;
            }            
        }
        
        var currency = amountItem.data("currency")
        var formattedAmount = new Intl.NumberFormat('it-IT', { style: 'currency', currency }).format(amount)
        amountItem.empty().html(formattedAmount)
    })



});