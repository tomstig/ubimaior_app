$(document).ready(function(){
    $(document).on('click','.transfer-list .add-selected', function(){
        var tlId = $(this).data('tl-id');
        var selectedItems = $(".transfer-list#"+tlId+" .items-container .accordion .row-item:checked")
        var selectedIds = [];
        selectedItems.each(function(){
            selectedIds.push($(this).attr('id'))
        })  
        var urlParams = selectedIds.join(';')
        window.location.href= `${window.location.pathname}?tl=${tlId}&t=a&ids=${urlParams}`;
    })

    $(document).on('click','.transfer-list .remove-selected', function(){
        var tlId = $(this).data('tl-id');
        var selectedItems = $(".transfer-list#"+tlId+" .selected-container .accordion .row-item:checked")
        var selectedIds = [];
        selectedItems.each(function(){
            selectedIds.push($(this).attr('id'))
        })  
        var urlParams = selectedIds.join(';')
        window.location.href= `${window.location.pathname}?tl=${tlId}&t=r&ids=${urlParams}`;
    })

    $(document).on('click','.transfer-list .add-all', function(){
        var tlId = $(this).data('tl-id');
        window.location.href= `${window.location.pathname}?tl=${tlId}&t=a&ids=all`;
    })

    $(document).on('click','.transfer-list .remove-all', function(){
        var tlId = $(this).data('tl-id');
        window.location.href= `${window.location.pathname}?tl=${tlId}&t=r&ids=all`;
    })

})