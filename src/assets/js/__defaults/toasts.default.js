$(document).ready(function(){
    
    $('.toast').toast()

    $(document).on('click',"[data-dismiss='toast']",function(){
        $('.toast').stop().animate({'right':'-500px'},500, function() { $('.toast').remove(); });        
    })

    setTimeout(function(){
        if($('.toast').length) $('.toast').stop().animate({'right':'-500px'},500, function() { $('.toast').remove(); });
    },5000)
})