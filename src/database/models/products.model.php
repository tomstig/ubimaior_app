<?php
return (object) [
    "name" => "products",
    "columns" => [
        "id_line" => [
            "type" => "INT",
            "nullable" => "NULL"
        ],
        "id_typology" => [
            "type" => "INT",
            "nullable" => "NULL"
        ],
        "id_currency" => [
            "type" => "INT",
            "nullable" => "NOT NULL"            
        ],
        "code" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",            
        ],
        "price" => [
            "type" => "FLOAT",
            "nullable" => "NULL",            
        ],
        "tech_specs" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL"  
        ],
        "manual" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL"  
        ],        
        "priority" => [
            "type" => "INT",
            "nullable" => "NULL",
        ],
        "active" => [
            "type" => "BOOLEAN",
            "nullable" => "NOT NULL",
            "default" => 1
        ],
        "custom" => [
            "type" => "BOOLEAN",
            "nullable" => "NOT NULL",
            "default" => 1
        ],
        "show_price" => [
            "type" => "BOOLEAN",
            "nullable" => "NOT NULL",
            "default" => 1
        ],                
    ]
];