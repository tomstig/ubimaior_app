<?php
return (object) [
    "name" => "bundles_products",
    "columns" => [       
        "id_product" => [
            "type" => "INT",
            "nullable" => "NOT NULL"           
        ],
        "id_bundle" => [
            "type" => "INT",
            "nullable" => "NOT NULL"         
        ]        
    ]
];