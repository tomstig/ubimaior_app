<?php
return (object) [
    "name" => "products_attributes",
    "columns" => [
        "id_product" => [
            "type" => "INT",
            "nullable" => "NOT NULL"
        ],
        "id_typology_attribute" => [
            "type" => "INT",
            "nullable" => "NOT NULL"
        ],
        "id_language" => [
            "type" => "INT",
            "nullable" => "NOT NULL"            
        ],
        "name" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",            
        ],
        "value" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",            
        ],        
    ]
];