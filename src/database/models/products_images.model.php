<?php
return (object) [
    "name" => "products_images",
    "columns" => [
        "id_product" => [
            "type" => "INT",
            "nullable" => "NULL"
        ],
        "image" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",            
        ],            
        "priority" => [
            "type" => "INT",
            "nullable" => "NULL",
        ],                
    ]
];