<?php
return (object) [
    "name" => "discounts",
    "columns" => [       
        "id_user" => [
            "type" => "INT",
            "nullable" => "NULL"           
        ],
        "id_catalog" => [
            "type" => "INT",
            "nullable" => "NULL"           
        ],
        "id_line" => [
            "type" => "INT",
            "nullable" => "NULL"           
        ],
        "id_typology" => [
            "type" => "INT",
            "nullable" => "NULL"           
        ],
        "id_product" => [
            "type" => "INT",
            "nullable" => "NULL"           
        ],
        "id_bundle" => [
            "type" => "INT",
            "nullable" => "NULL"           
        ],
        "percentageDiscount" => [
            "type" => "FLOAT",
            "nullable" => "NULL"  
        ],
        "percentageDiscount" => [
            "type" => "FLOAT",
            "nullable" => "NULL"  
        ],
        "is_template" => [
            "type" => "BOOLEAN",
            "nullable" => "NOT NULL",
            "default" => 1
        ],
    ]
];