<?php
return (object) [
    "name" => "typologies_attributes",
    "columns" => [       
        "id_typology" => [
            "type" => "INT",
            "nullable" => "NOT NULL"           
        ],
        "id_language" => [
            "type" => "INT",
            "nullable" => "NOT NULL"         
        ],
        "name" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",            
        ],
    ]
];