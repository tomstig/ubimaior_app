<?php
return (object) [
    "name" => "users",
    "columns" => [
        "id_user_type" => [
            "type" => "INT",
            "nullable" => "NOT NULL"
        ],
        "id_currency" => [
            "type" => "INT",
            "nullable" => "NULL"            
        ],
        "name" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",            
        ],
        "surname" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",            
        ],
        "email" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",            
            "unique" => true,
        ],
        "username" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",
            "unique" => true,
        ],
        "password" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",
        ],
        "mobile" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",
        ],
        "business_name" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",
        ],
        "fiscal_code" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",
        ],
        "vat" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",
        ],
        "active" => [
            "type" => "BOOLEAN",
            "nullable" => "NOT NULL",
            "default" => 1
        ],
        "can_create" => [
            "type" => "BOOLEAN",
            "nullable" => "NOT NULL",
            "default" => 1
        ],
        "reply_to" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",
        ],
        "sender_email" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",
        ],
        "logo" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",
        ],
        "token" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL"  
        ],
    ]
];