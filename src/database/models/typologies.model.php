<?php
return (object) [
    "name" => "typologies",
    "columns" => [
        "name" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",            
        ],
        "code" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",            
        ],        
        "logo" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",
        ],
        "tech_specs" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL"  
        ],        
    ]
];