<?php
return (object) [
    "name" => "catalogs",
    "columns" => [
        "id_user" => [
            "type" => "INT",
            "nullable" => "NOT NULL"
        ],
        "id_currency" => [
            "type" => "INT",
            "nullable" => "NOT NULL"            
        ],
        "id_language" => [
            "type" => "INT",
            "nullable" => "NOT NULL"            
        ],
        "name" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",            
        ],
        "active" => [
            "type" => "BOOLEAN",
            "nullable" => "NOT NULL",
            "default" => 1
        ],        
        "token" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL"  
        ],
    ]
];