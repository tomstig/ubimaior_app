<?php
return (object) [
    "name" => "ig_tokens",
    "columns" => [
        "type" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",            
        ],
        "value" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",            
        ],
        "expiring" => [
            "type" => "DATETIME",
            "nullable" => "NULL",            
            "unique" => true,
        ]        
    ]
];