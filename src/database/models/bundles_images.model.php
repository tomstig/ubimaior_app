<?php
return (object) [
    "name" => "bundles_images",
    "columns" => [
        "id_bundle" => [
            "type" => "INT",
            "nullable" => "NULL"
        ],
        "image" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",            
        ],            
        "priority" => [
            "type" => "INT",
            "nullable" => "NULL",
        ],                
    ]
];