<?php
return (object) [
    "name" => "users_parenting",
    "columns" => [       
        "id_parent" => [
            "type" => "INT",
            "nullable" => "NOT NULL"           
        ],
        "id_child" => [
            "type" => "INT",
            "nullable" => "NOT NULL"         
        ]        
    ]
];