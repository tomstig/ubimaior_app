<?php
return (object) [
    "name" => "localizations",
    "columns" => [       
        "id_language" => [
            "type" => "INT",
            "nullable" => "NOT NULL"           
        ],
        "id_line" => [
            "type" => "INT",
            "nullable" => "NULL"           
        ],
        "id_typology" => [
            "type" => "INT",
            "nullable" => "NULL"           
        ],
        "id_product" => [
            "type" => "INT",
            "nullable" => "NULL"           
        ],
        "id_bundle" => [
            "type" => "INT",
            "nullable" => "NULL"           
        ],
        "content" => [
            "type" => "TEXT",
            "nullable" => "NOT NULL"  
        ],
    ]
];