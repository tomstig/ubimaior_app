<?php
return (object) [
    "name" => "languages",
    "columns" => [
        "language" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NOT NULL"            
        ],
        "code" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NOT NULL"            
        ],
        "active" => [
            "type" => "BOOLEAN",
            "nullable" => "NOT NULL",
            "default" => 0
        ]
    ]
];