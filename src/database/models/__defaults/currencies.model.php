<?php
return (object) [
    "name" => "currencies",
    "columns" => [
        "name" => [
            "type" => "VARCHAR(20)",
            "nullable" => "NOT NULL"            
        ],
        "code" => [
            "type" => "VARCHAR(3)",
            "nullable" => "NOT NULL"            
        ],
        "symbol" => [
            "type" => "VARCHAR(5)",
            "nullable" => "NOT NULL"            
        ],
        "active" => [
            "type" => "BOOLEAN",
            "nullable" => "NOT NULL",
            "default" => 1
        ]
    ]
];