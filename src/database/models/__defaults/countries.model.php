<?php
return (object) [
    "name" => "countries",
    "columns" => [
        "code" => [
            "type" => "VARCHAR(3)",
            "nullable" => "NOT NULL"            
        ],
        "name" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NOT NULL"            
        ],        
    ]
];