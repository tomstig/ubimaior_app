<?php
return (object) [
    "name" => "users_products",
    "columns" => [       
        "id_user" => [
            "type" => "INT",
            "nullable" => "NOT NULL"           
        ],
        "id_product" => [
            "type" => "INT",
            "nullable" => "NOT NULL"         
        ]        
    ]
];