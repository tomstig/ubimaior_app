<?php
return (object) [
    "name" => "users_type",
    "columns" => [       
        "type" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",            
        ],
        "slug" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",            
        ]        
    ]
];