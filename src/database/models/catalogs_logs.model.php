<?php
return (object) [
    "name" => "catalogs_logs",
    "columns" => [
        "id_user" => [
            "type" => "INT",
            "nullable" => "NOT NULL"
        ],
        "id_catalog" => [
            "type" => "INT",
            "nullable" => "NOT NULL"            
        ]
    ]
];