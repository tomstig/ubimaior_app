<?php
return (object) [
    "name" => "users_addresses",
    "columns" => [
        "id_user" => [
            "type" => "INT",
            "nullable" => "NOT NULL"
        ],
        "id_country" => [
            "type" => "INT",
            "nullable" => "NOT NULL"
        ],
        "type" => [
            "type" => "ENUM('shipping','billing','legal')",
            "nullable" => "NOT NULL",
            "default" => "'legal'"
        ],
        "name" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",            
        ],
        "address" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",            
        ],
        "zip" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",            
            "unique" => true,
        ],
        "city" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",
            "unique" => true,
        ]        
    ]
];