<?php
return (object) [
    "name" => "orders",
    "columns" => [
        "id_catalog_user" => [
            "type" => "INT",
            "nullable" => "NULL"
        ],
        "id_catalog" => [
            "type" => "INT",
            "nullable" => "NULL"
        ],
        "id_reseller" => [
            "type" => "INT",
            "nullable" => "NULL"
        ],
        "id_client" => [
            "type" => "INT",
            "nullable" => "NULL"
        ],
        "id_currency" => [
            "type" => "INT",
            "nullable" => "NULL"
        ],
        "message" => [
            "type" => "TEXT",
            "nullable" => "NULL",            
        ],
        "note" => [
            "type" => "TEXT",
            "nullable" => "NULL",            
        ],
        "total" => [
            "type" => "FLOAT",
            "nullable" => "NULL",            
        ],
        "client_name" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",            
        ],
        "client_surname" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",            
        ],
        "client_email" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",            
        ],
        "client_mobile" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",            
        ],
        "client_business_name" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",            
        ],
    ]
];