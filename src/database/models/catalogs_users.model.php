<?php
return (object) [
    "name" => "catalogs_users",
    "columns" => [
        "id_user" => [
            "type" => "INT",
            "nullable" => "NOT NULL"
        ],
        "id_catalog" => [
            "type" => "INT",
            "nullable" => "NOT NULL"            
        ],
        "active" => [
            "type" => "BOOLEAN",
            "nullable" => "NOT NULL",
            "default" => 1
        ],                
    ]
];