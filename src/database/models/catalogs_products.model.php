<?php
return (object) [
    "name" => "catalogs_products",
    "columns" => [
        "id_catalog" => [
            "type" => "INT",
            "nullable" => "NOT NULL"
        ],
        "id_product" => [
            "type" => "INT",
            "nullable" => "NOT NULL"            
        ],
        "active" => [
            "type" => "BOOLEAN",
            "nullable" => "NOT NULL",
            "default" => 1
        ],        
        "priority" => [
            "type" => "INT",
            "nullable" => "NULL"  
        ],
    ]
];