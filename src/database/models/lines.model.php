<?php
return (object) [
    "name" => "lines",
    "columns" => [
        "name" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",            
        ],
        "code" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",            
        ],        
        "logo" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL",
        ],
        "tech_specs" => [
            "type" => "VARCHAR(255)",
            "nullable" => "NULL"  
        ],
    ]
];