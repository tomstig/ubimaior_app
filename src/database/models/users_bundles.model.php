<?php
return (object) [
    "name" => "users_bundles",
    "columns" => [       
        "id_user" => [
            "type" => "INT",
            "nullable" => "NOT NULL"           
        ],
        "id_bundle" => [
            "type" => "INT",
            "nullable" => "NOT NULL"         
        ]        
    ]
];