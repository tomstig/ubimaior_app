<?php
return (object) [
    "name" => "orders_items",
    "columns" => [
        "id_order" => [
            "type" => "INT",
            "nullable" => "NOT NULL"
        ],
        "id_product" => [
            "type" => "INT",
            "nullable" => "NOT NULL"            
        ],
        "id_bundle" => [
            "type" => "INT",
            "nullable" => "NOT NULL"            
        ],        
        "quantity" => [
            "type" => "INT",
            "nullable" => "NULL"  
        ],
    ]
];