<?php
return (object) [
    "queries" => [
        ["id_user_type" => 1, "name" => "Admin", "surname" => "Ubimaior", "email" => "admin@ubimaioritalia.com", "username" => "admin.ubimaior", "password" => hash("sha512","Admin12341234!!")],
        ["id_user_type" => 1, "name" => "Simone", "surname" => "Landi", "email" => "s.landi@meetico.ltd", "username" => "simone.landi", "password" => hash("sha512","12341234!!")],
        ["id_user_type" => 1, "name" => "Tommaso", "surname" => "Stigler", "email" => "t.stigler@meetico.ltd", "username" => "tommaso.stigler", "password" => hash("sha512","12341234!!")],
        ["id_user_type" => 1, "name" => "Gherardo", "surname" => "Masini", "email" => "g.masini@meetico.ltd", "username" => "gherardo.masini", "password" => hash("sha512","12341234!!")],
    ]
];