<?php
return (object) [
    "queries" => [                        
        [ "id" => 1, "code" => 'ca', "language" => 'Catalan', "active" => 0],               
        [ "id" => 2, "code" => 'nl', "language" => 'Dutch', "active" => 0],
        [ "id" => 3, "code" => 'en', "language" => 'English', "active" => 1],
        [ "id" => 4, "code" => 'fr', "language" => 'French', "active" => 0],
        [ "id" => 5, "code" => 'de', "language" => 'German', "active" => 0],
        [ "id" => 6, "code" => 'pt', "language" => 'Portuguese', "active" => 0],
        [ "id" => 7, "code" => 'es', "language" => 'Spanish', "active" => 0],        
        [ "id" => 8, "code" => 'it', "language" => 'Italian', "active" => 1],
    ]
];        