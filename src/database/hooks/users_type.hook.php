<?php
return (object) [
    "queries" => [
        ["type" => "Admin", "slug" => "admin"],
        ["type" => "Reseller", "slug" => "reseller"],
        ["type" => "Agent", "slug" => "agent"],
        ["type" => "Customer", "slug" => "customer"],
    ]
];